Welcome to
=================================================
  _______ _            ______ _     _
 |__   __| |          |  ____| |   | |
    | |  | |__   ___  | |__  | | __| | ___ _ __
    | |  | '_ \ / _ \ |  __| | |/ _` |/ _ \ '__|
    | |  | | | |  __/ | |____| | (_| |  __/ |
    |_|  |_| |_|\___| |______|_|\__,_|\___|_|
=================================================
PROJECT PATH: 
VFS_Depot/Projects/GD54PG15/TheElder

TEAM: Mei, Allie, Zion, Lisa, Marina, Daniel


INTRODUCTION:
The Elder is a 3rd person Action/Adventure game where you accompany a sapient tree named Chi to regain the order of the forest.
Using Chi’s abilities, the player must overcome the challenges of the degenerating environment and defeat the tainted elder of the forest.

CONTROLS: (Joystick controller)
Left Stick      -   Move Character
Right Stick     -   Aim Camera
X/Square        -   Interact/Climb
A/Cross         -   Jump
B/Circle        -   Stop Climbing
Start/Options   -   Pause
Left Shoulder   -   Enter Aim Sap View
Right Trigger   -   Throw Sap
Right Shoulder  -   Initiate Catapult
(1st press)
Right Stick     -   Aim Catapult
(Catapult Mode)
Right Shoulder	-   Launch Catapult
(2nd press)
B/Circle        -   Cancel Catapult
DPAD Up         -   Enter Debug Menu


WHAT'S IN THE GAME:
The game includes a full blockout of our entire level. All major mechanics are fully 
implemented and some placeholder art assets have been replaced with actual art assets.

KNOWN ISSUES:
In the MainMenu, do not press QA Play as it does not work at this time.
If you die in catapult mode in the boss area, the game will crash.




