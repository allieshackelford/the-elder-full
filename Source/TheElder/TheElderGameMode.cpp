// Fill out your copyright notice in the Description page of Project Settings.


#include "TheElderGameMode.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/PostProcessComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Player/Mokosh.h"
#include "AI/ChiAI/Chi.h"
#include "World/ElderCheckpoint.h"
#include "Engine/Engine.h"
#include "Systems/TheElderGameInstance.h"
#include "Engine/LevelStreaming.h"
#include "ContentStreaming.h"
#include "MasterIncludes.h"
#include "EngineUtils.h"
#include "Engine/PlayerStartPIE.h"
#include "Systems/CheckpointResettable.h"
#include "Systems/ElderSystemsDataAsset.h"
#include "UI/LoadingScreen.h"

ATheElderGameMode::ATheElderGameMode() {
	PostProcess = CreateDefaultSubobject<UPostProcessComponent>(TEXT("PostProcess"));

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Characters/BP_Mokosh"));
	static ConstructorHelpers::FClassFinder<APlayerController> ControllerPawnBPClass(TEXT("/Game/Blueprints/Characters/BP_PlayerController"));
	
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	if (ControllerPawnBPClass.Class != NULL)
	{
		PlayerControllerClass = ControllerPawnBPClass.Class;
	}

	// Setup loading screen UI
	static ConstructorHelpers::FClassFinder<ULoadingScreen> LoadingScreenBPClass(TEXT("/Game/Blueprints/UI/BPW_LoadingScreen"));
	if (LoadingScreenBPClass.Class != nullptr)
	{
		LoadingScreenClass = LoadingScreenBPClass.Class;
	}

	static ConstructorHelpers::FObjectFinder<UElderSystemsDataAsset> TheElderSystemsData(
		TEXT("/Game/Data/DataAssets/DA_ElderSystems.DA_ElderSystems"));
	if (TheElderSystemsData.Object != nullptr)
	{
		ElderSystemsData = TheElderSystemsData.Object;
	}
}

AActor* ATheElderGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	auto World = GetWorld();
	if (World == nullptr)
		return nullptr;

	auto Levels = World->GetStreamingLevels();
	
	auto GameInstance = Cast<UTheElderGameInstance>(GetGameInstance());
#if WITH_EDITOR
	// Let's check if we have a play in editor checkpoint
	// We only want to play from camera location if it's our first checkpoint
	if (GameInstance->bFirstRun)
	{
		for (TActorIterator<APlayerStartPIE> It(World); It; ++It)
		{
			APlayerStart* PlayerStart = *It;

			if (PlayerStart->IsA<APlayerStartPIE>())
			{
				SetupSublevels();
				return PlayerStart;
			}
		}
	}
#endif

	auto Checkpoint = GameInstance->GetCheckpoint(this);
	SetupSublevels();
	if (Checkpoint != nullptr)
	{
		return Checkpoint;
	}
	// Backup if we don't have data for some reason
	GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, TEXT("ERROR: Custom ChoosePlayerStart failed, falling back to Super:: impl."));
	return Super::ChoosePlayerStart_Implementation(Player);
}

void ATheElderGameMode::SetBrightness(float GammaVal) {
	FVector4 SetGamma(GammaVal, GammaVal, GammaVal, GammaVal);
	PostProcess->Settings.bOverride_ColorGamma = true;
	PostProcess->Settings.ColorGamma = SetGamma;
}

void ATheElderGameMode::ClimbingDisable(int IsOn)
{
	auto Character = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (Character != nullptr)
	{
		auto OurCharacter = Cast<AMokosh>(Character);
		if (OurCharacter != nullptr)
		{
			OurCharacter->ClimbingComponent->bIsDebugging = static_cast<bool>(IsOn);
		}
	}
}

int ATheElderGameMode::RegisterCheckpoint(AElderCheckpoint* Checkpoint)
{
	return Checkpoints.AddUnique(Checkpoint);
}

AElderCheckpoint* ATheElderGameMode::GetCheckpointByIndex(int Index)
{
	if (Index >= Checkpoints.Num() || Index < 0)
	{
		return nullptr;
	}
	return Checkpoints[Index];
}

void ATheElderGameMode::PausePlayerMovement()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;

	// Ugly hack because I don't want to refactor checkpoint system code right now
	auto WorldName = UGameplayStatics::GetCurrentLevelName(World);
	if (WorldName != TEXT("GD54_Lisa_TheElder_Whitebox"))
	{
		return;
	}

	auto PC = UGameplayStatics::GetPlayerController(World, 0);
	if (PC == nullptr)
		return;
	
	// Set the player to cinematic mode while loading sublevels
	// 
	auto Char = PC->GetCharacter();
	if (Char != nullptr)
	{
		PC->GetCharacter()->GetCharacterMovement()->GravityScale = 0;
		DisableInput(PC);
	}
	//PC->SetCinematicMode(true, false, true, true, true);
}

void ATheElderGameMode::UnpausePlayerMovement()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;

	auto PC = UGameplayStatics::GetPlayerController(World, 0);
	auto Character = UGameplayStatics::GetPlayerCharacter(World, 0);
	if (PC == nullptr || Character == nullptr)
		return;

	// Set the player to cinematic mode while loading sublevels
	Character->GetCharacterMovement()->GravityScale = 1;
	EnableInput(PC);
}

void ATheElderGameMode::BeginPlay()
{
	Super::BeginPlay();
	auto World = GetWorld();
	if (World == nullptr)
		return;

	auto GameInstance = Cast<UTheElderGameInstance>(GetGameInstance());


	// GET MOKOSH REFERENCE
	for (TActorIterator<AMokosh> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		MokoshCharacter = *ActorItr;
	}

	// GET CHI REFERENCE

	for (TActorIterator<AChi> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ChiCharacter = *ActorItr;
	}

	
	// Load Main Menu UI
#if WITH_EDITOR
	if (GameInstance->CheckpointData->bShowMainMenu)
#else
	if (GameInstance->bFirstRun)
#endif
	{
		auto Widget = CreateWidget<UMainMenu>(GetWorld(), GameInstance->MainMenuClass, FName("Main Menu"));
		if (Widget)
		{
			Widget->AddToViewport();
			MainMenu = Widget;
		}
	}

	GameInstance->bFirstRun = false;

	PausePlayerMovement();

	// Some ugly hack to attempt to remove texture streaming pop-in
	GEngine->Exec(World, TEXT("r.Streaming.PoolSize 5000"));
}

void ATheElderGameMode::SetupSublevels()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
	auto GameInstance = Cast<UTheElderGameInstance>(GetGameInstance());
	
	// Now load checkpoints
	auto LevelName = UGameplayStatics::GetCurrentLevelName(this);
	auto Checkpoints = GameInstance->CheckpointData->FindCheckpoint(LevelName);
	if (Checkpoints == nullptr)
	{
		return;
	}
	auto CheckpointIndex = GameInstance->CurrentCheckpointNum;

	auto CheckpointArray = GameInstance->CheckpointData->GetArray(LevelName);

	if (CheckpointIndex >= CheckpointArray.Num() && CheckpointArray.Num() > 0)
	{
		CheckpointIndex = CheckpointArray.Num() - 1;
	}
	auto SubLevels = CheckpointArray[GameInstance->CurrentCheckpointNum].SublevelsToLoad;
	SubLevels += GameInstance->CheckpointData->PersistentSublevels;
	if (GameInstance->bFirstRun)
	{
		CachedSublevelsToLoad = SubLevels;
	}
	else
	{
		auto Levels = World->GetStreamingLevels();
		TArray<FString> LevelsToUnload;
		for (auto Level : Levels)
		{
			if (Level->IsLevelLoaded())
			{
				auto WorldName = FPackageName::GetShortName(Level->GetWorldAssetPackageName());
				WorldName.RemoveFromStart(World->StreamingLevelsPrefix, ESearchCase::IgnoreCase);
				int index = SubLevels.Remove(WorldName);
				// If we removed nothing, then we're not supposed to have this sublevel loaded, so we need to unload it
				if (index == 0)
				{
					LevelsToUnload.Add(WorldName);
				}
			}
		}
		CachedSublevelsToUnload = LevelsToUnload;
		UnloadNextSubLevel();
	}

	for (auto ResettablePair : TrackedResettables)
	{
		if (IsValid(ResettablePair.Key))
		{
			if (ResettablePair.Key->GetClass()->ImplementsInterface(UCheckpointResettable::StaticClass()))
			{
				ICheckpointResettable::Execute_ResetActor(ResettablePair.Key, ResettablePair.Value);
			}
		}
	}
	
	LoadNextSubLevel();
	SetupLoadingScreen();
}

void ATheElderGameMode::UpdateResettable(TScriptInterface<class ICheckpointResettable> Resettable, uint8 StateToUpdate)
{
	AActor* Actor = Cast<AActor>(Resettable.GetObject());
	if (Actor == nullptr)
		return;
	TrackedResettables.Emplace(Actor, StateToUpdate);
}

void ATheElderGameMode::LoadNextSubLevel()
{
	if (CachedSublevelsToLoad.Num() > 0)
	{
		FLatentActionInfo ActionInfo;
		ActionInfo.CallbackTarget = this;
		ActionInfo.ExecutionFunction = GET_FUNCTION_NAME_CHECKED(ATheElderGameMode, LoadNextSubLevel);
		ActionInfo.Linkage = 0;
		UGameplayStatics::LoadStreamLevel(this, FName(*CachedSublevelsToLoad[0]), true, true, ActionInfo);
		CachedSublevelsToLoad.RemoveAt(0);
	}
	else
	{
		UnpausePlayerMovement();
		auto GameInstance = Cast<UTheElderGameInstance>(GetGameInstance());

#if !WITH_EDITOR
		// If it's the first run we need to pad out the load time to allow textures to stream in
		if (bInitialSubLevelsLoaded == false)
		{
			FTimerDelegate Delegate;
			FTimerHandle TimerHandle;
			Delegate.BindLambda([&]
			{
				if (!IsValid(this))
					return;
				LoadingScreen->FadeOut();
			});
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, Delegate, ElderSystemsData->TexturingStreamingBufferTime, false);
			bInitialSubLevelsLoaded = true;
		}
		else
#endif
		{
			LoadingScreen->FadeOut();
		}

	}
}
void ATheElderGameMode::UnloadNextSubLevel()
{
	if (CachedSublevelsToUnload.Num() > 0)
	{
		FLatentActionInfo ActionInfo;
		ActionInfo.CallbackTarget = this;
		ActionInfo.ExecutionFunction = GET_FUNCTION_NAME_CHECKED(ATheElderGameMode, UnloadNextSubLevel);
		ActionInfo.Linkage = 0;
		UGameplayStatics::UnloadStreamLevel(this, FName(*CachedSublevelsToUnload[0]), ActionInfo, true);
		CachedSublevelsToUnload.RemoveAt(0);
	}
}

AMokosh* ATheElderGameMode::GetMokoshCharacter() const
{
	return MokoshCharacter;
}

AChi* ATheElderGameMode::GetChiCharacter() const
{
	return ChiCharacter;
}

void ATheElderGameMode::SetupLoadingScreen()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
	
	// If we already have a loading screen, we don't need to set it up again
	if (IsValid(LoadingScreen))
		return;
	
	LoadingScreen = CreateWidget<ULoadingScreen>(World, LoadingScreenClass);
	if (LoadingScreen != nullptr)
	{
		LoadingScreen->AddToViewport(3);
	}
}

void ATheElderGameMode::PlayerDied()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
	
	if (ElderSystemsData == nullptr)
		return;

	// If we're already respawning we don't need to activate again
	if (bIsRespawning)
		return;

	bIsRespawning = true;

	SetupLoadingScreen();

	LoadingScreen->DeathFadeIn(ElderSystemsData->TimeToFadeIn);

	FTimerHandle handle;
	World->GetTimerManager().SetTimer(handle, this, &ATheElderGameMode::DeathFadeInDone, ElderSystemsData->TimeToFadeIn);
}

void ATheElderGameMode::DeathFadeInDone()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;

	PausePlayerMovement();
	
	FTimerHandle handle;
	World->GetTimerManager().SetTimer(handle, this, &ATheElderGameMode::FadeBackToGameplay, ElderSystemsData->TimeStayBlack);
}

void ATheElderGameMode::FadeBackToGameplay()
{
	if (bIsRespawning == false)
		return;
	GetMokoshCharacter()->ResetLocation();
	UnpausePlayerMovement();
	LoadingScreen->DeathFadeOut(ElderSystemsData->TimeToFadeOut);
	
	bIsRespawning = false;
}