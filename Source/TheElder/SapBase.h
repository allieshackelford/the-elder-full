// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "World/Climbable.h"
#include "World/SappableZone.h"
#include "SapBase.generated.h"

UCLASS()
class THEELDER_API ASapBase : public AClimbable
{
	GENERATED_BODY()
	
public:	
	ASapBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh;

	/* This is set if the object is placed at edit time in the world, rather than thrown by the player at runtime*/
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	bool bIsStaticallyPlaced = true;

	/* This is set to true when the collision check on creation passes successfully*/
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	bool bApproved = false;

	
	UPROPERTY(VisibleAnywhere)
	TArray<ASapBase*> ConnectedSaps;

	UFUNCTION(BlueprintImplementableEvent)
	void ActivateHighlight();

	UFUNCTION(BlueprintImplementableEvent)
	void DeactivateHighlight();

protected:
	virtual void BeginPlay() override;
	
	void PostInitializeComponents() override;
	void CheckForConnectedLedge();

	/* This is set if this sap is close to a ASplineLedge*/
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	bool bCloseToLedge = false;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Lasers")
	UStaticMesh* LaserBeamMesh;
	/*The thickness of the laser itself*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Lasers")
	float LaserScaleXZ = 1.0f;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Lasers")
	FVector LaserOffset;

	/* This is set on BeginPlay if it already has laser connection to all of it's connected nodes*/
	bool bAlreadyConnected = false;

	virtual UStaticMeshComponent* GenerateLaser(const ASapBase* SapToConnectTo);
	void GenerateConnectedSaps();

public:	
	virtual void Tick(float DeltaTime) override;
	TArray<ASapBase*> GetConnectedSaps() const;

	bool IsCloseToLedge() const;

	bool IsAlreadyConnected() const
	{
		return bAlreadyConnected;
	}

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateStatus(bool bIsApproved, bool bHasConnectedSap);
};
