// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SapViewActor.generated.h"

UCLASS()
class THEELDER_API ASapViewActor : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASapViewActor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USpringArmComponent* SapBoom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UCameraComponent* SapCamera;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	class AMokosh* PlayerRef;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// TODO: check if this works
	void MoveHorizontal(float Value);
	void MoveVertical(float Value);

	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
};
