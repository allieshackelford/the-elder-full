// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "ElderCameraShake.h"
#include "ElderSpringArmComponent.generated.h"

UENUM(BlueprintType)
enum ECameraTypes {
	Normal UMETA(DisplayName = "Normal"),
	Climbing UMETA(DisplayName = "Climbing"),
	SapAim UMETA(DisplayName = "SapAim"),
	Cinematic UMETA(DisplayName = "Cinematic"),
	Respawn UMETA(DisplayName = "Respawn"),
	Death UMETA(DisplayName = "Death"),
	BossSmash UMETA(DisplayName = "BossSmash")
};

struct FCameraChangeParams {
	float ArmLength;
	FVector Offset;
	FRotator RelRotation;
	bool ChangeRotation;
	float LoadTime;

	FCameraChangeParams() {}

	FCameraChangeParams(float len, FVector off, FRotator rot, bool change, float load) {
		ArmLength = len;
		Offset = off;
		RelRotation = rot;
		ChangeRotation = change;
		LoadTime = load;
	}
};


/**
 * 
 */
UCLASS()
class THEELDER_API UElderSpringArmComponent : public USpringArmComponent
{
	GENERATED_BODY()

public:

	UPROPERTY()
		FTimerHandle CinematicTimer;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UElderCameraShake> CinematicCamShake;
	
	ECameraTypes CurrentCam;
	

protected:
	// ======= CAMERA VIEW VARIABLES ===========
	float startTime;
	float loadTime = .5f;

	float startLength;
	float endLength;
	FVector startOffset;
	FVector endOffset;
	FRotator startRot;
	FRotator endRot;
	FRotator startControlRot;
	FRotator endControlRot;

	bool ChangeRot;
	bool ChangeToView;
	TMap<ECameraTypes, FCameraChangeParams> CameraChanges;

	class APlayerController* Controller;
	class AMokosh* Ref;

	TArray<AActor*> HiddenActors;

public:
	UElderSpringArmComponent();

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	// adjust camera pitch distance NORMAL
	void UpdateCameraPitchCurve(float currPitch);
	// adjust camera pitch distance AIMING
	void UpdateAimingPitchCurve(float currPitch);
	
	float Remap(float val, FVector2D xy1, FVector2D xy2);

	UFUNCTION(BlueprintCallable)
	void ChangeCamera(ECameraTypes cameraType);
	void LerpToView();

	// ==== HIDE CHI ======
	void HideChi();

	// ==== IMPROVE VISIBILITY =====
	void CheckToHideObjects();
	void UnHideObjects();

	// ====== CINEMATIC CAMERA =====
	UFUNCTION(BlueprintCallable)
	void CinematicLookAtTarget(FVector TargetLoc);
	void ResetCamera();
};
