// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "SapViewActor.h"
#include "Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Components/InputComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Player/Mokosh.h"
#include "Player/SapThrowComponent.h"

// Sets default values
ASapViewActor::ASapViewActor()
{
	// player rotation not affected by the camera
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom (pulls in towards the player if there is a collision)
	SapBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("SapBoom"));
	SapBoom->SetupAttachment(RootComponent);
	SapBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	SapCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("SapCamera"));
	SapCamera->SetupAttachment(SapBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	SapCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

// Called when the game starts or when spawned
void ASapViewActor::BeginPlay()
{
	Super::BeginPlay();
	PlayerRef = Cast<AMokosh>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

// Called every frame
void ASapViewActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASapViewActor::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// movement inputs
	// NOT WORKING -- LOOK INTO LATER
	PlayerInputComponent->BindAxis("Horizontal", this, &ASapViewActor::MoveHorizontal);
	PlayerInputComponent->BindAxis("Vertical", this, &ASapViewActor::MoveVertical);

	// aiming controls
	PlayerInputComponent->BindAxis("RHorizontal", this, &ASapViewActor::TurnAtRate);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASapViewActor::TurnAtRate);
	PlayerInputComponent->BindAxis("RVertical", this, &ASapViewActor::LookUpAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASapViewActor::LookUpAtRate);

	// give control to sap view, call sapthrowcomponent
	PlayerInputComponent->BindAction("SapAim", IE_Released, PlayerRef->SapThrowComponent, &USapThrowComponent::ExitSapAim);
	PlayerInputComponent->BindAction("SapThrow", IE_Pressed, PlayerRef->SapThrowComponent, &USapThrowComponent::SapThrow);
}

void ASapViewActor::MoveHorizontal(float Value) {
	// find out which way is forward
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	// get forward vector
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void ASapViewActor::MoveVertical(float Value) {
	// find out which way is right
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	// get right vector 
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	// add movement in that direction
	AddMovementInput(Direction, Value);
}


void ASapViewActor::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * 0.4f);
}

void ASapViewActor::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * 33.f * GetWorld()->GetDeltaSeconds());
}
