// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Waypoint.generated.h"

enum class FWaypointSide{
	Left,
	Right,
	Behind
};

UCLASS()
class THEELDER_API AWaypoint : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* DebugCheck;
	
protected:
	class AMokosh* PlayerRef;
	
	// ======= LERP VARIABLES ===========
	float startTime;
	float lerpTime = 0.4f;

	FVector startPos;
	FVector endPos;
	
	FWaypointSide CurrentSide;
	
	TMap<FWaypointSide, FVector> Positions;

	bool bShouldChangePos;

protected:
	AWaypoint();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// reposition waypoint
	void CalculateWaypointPos(FVector CameraPos);

	void LerpToPos();
};
