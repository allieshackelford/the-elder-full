// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BTT_MoveWayPoints.h"
#include "AI/ChiAI/ChiAIController.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTT_MoveWayPoints::UBTT_MoveWayPoints(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	bMoving = false;
}


EBTNodeResult::Type UBTT_MoveWayPoints::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) {
	if(AIComp == nullptr)
	{
		AIComp = Cast<AChiAIController>(OwnerComp.GetAIOwner());
	}

	return EBTNodeResult::Succeeded;
}