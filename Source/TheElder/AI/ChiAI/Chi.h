// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/BaseAI/AICharacter.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "CollisionQueryParams.h"
#include "Player/Mokosh.h"
#include "Chi.generated.h"

enum class ChiStates : uint8;

/**
 * 
 */
UCLASS()
class THEELDER_API AChi : public AAICharacter
{
	GENERATED_BODY()

protected:
	class AMokosh* PlayerRef;
	class AChiAIController* ChiAIControllerRef;
	class APlayerController* ChiPlayerController;

	// ==== TRANSFORM LERP VARIABLES =====
	bool IsTransforming;
	bool IsToStaff;
	FVector StartPos;
	FVector EndPos;
	float StartTime;
	float LoadTime = .5f;

	
	// raycast variables
	const float RotDistance = 180.0f / 8;
	FCollisionQueryParams params;
public:

	// ================ CATAPULT COMPONENTS ===================
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USceneComponent* CatapultLoadPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UElderSpringArmComponent* CameraBoom;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UCameraComponent* FollowCamera;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Respawn")
		bool IsRespawning = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walk Speed")
		float BaseWalkSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walk Speed")
		float CatapultWalkSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UChiAnimInstance* AnimInstance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Transformation")
		TSubclassOf<AActor> TransformActorToSpawn;

	// ===== TIMERS =======
	UPROPERTY()
		FTimerHandle CatapultTimer;
	UPROPERTY()
		FTimerHandle RespawnTimer;


	// ============ CATAPULT SPLINE =============
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* SceneComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USplineComponent* Spline;
	UPROPERTY(EditDefaultsOnly)
		class UStaticMesh* MeshForSpline;
	UPROPERTY(EditDefaultsOnly)
		class UMaterialInterface* SplineMeshMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<class USplineMeshComponent*> SplineMeshes;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* ChiTransformActor;


	// ======= TRANSFORM PARTICLE =======
	class UParticleSystem* CatapultParticle;
	FVector LastAimPos;
	class UParticleSystemComponent* LastParticle;
	
	bool CheckSpeed;
	bool CatapultReady;
	float SpeedMult;

	float CatapultPitchValue;

	bool GenerateCatapultSpline;

public:
	AChi();
	void OnConstruction(const FTransform& Transform) override;
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	float Remap(float val, FVector2D xy1, FVector2D xy2);
	
	//  ====== MOVEMENT =========
	void MoveHorizontal(float Value);
	void MoveVertical(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);
	// ===== SET SPAWN AND LOCATION ======
	void SetWalkSpeed();
	void SpawnBehindPlayer(FVector playerPos); 
	void SetTargetLoc(AActor* player);
	
	// ====== CATAPULT =========
	UFUNCTION(BlueprintCallable)
	void ActivateCatapult();
	
	void InitCatapult();
	void PlayerLaunched();
	void CancelCatapult();

	FVector GetLoadSocketLocation();

	UFUNCTION(BlueprintCallable)
	void CatapultFailed();

	// ======= SAP THROW =========
	void AimRotation(FVector lookDirection);

	void ChangeState(ChiStates NewState);
	
	UFUNCTION(BlueprintCallable)
	uint8 GetChiState();

	// ========= STAFF ===========
	UFUNCTION(BlueprintCallable)
	void ChangeToStaff();
	void ChangeFromStaff(AActor* player);

	UFUNCTION(BlueprintImplementableEvent)
	void PlayTransformationSound(bool TransformTo);

	UFUNCTION(BlueprintImplementableEvent)
	void ToggleAimPivot(bool ToggleOn);

	UFUNCTION(BlueprintImplementableEvent)
	void CalculateAimPosition(FVector AimScale);

	UFUNCTION(BlueprintImplementableEvent)
	void ToggleOccludeMaterial(float ScalarValue);

	UFUNCTION(BlueprintImplementableEvent)
	void SetCatapultSpline(FVector EndLocation);

	UFUNCTION(BlueprintImplementableEvent)
	void ToggleChristmasElements(bool ToggleOn);

	void LerpParticle();
	
	void RePossess();

	UFUNCTION(BlueprintCallable)
	void RespawnChi();

	// Catapult spline and particle
	
	void DrawCatapultSpline();

	UFUNCTION(BlueprintCallable)
	void FindParticleHitLocation(FVector StartLocation);
};
