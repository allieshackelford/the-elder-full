// Fill out your copyright notice in the Description page of Project Settings.


#include "ChiAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Components/ChildActorComponent.h"
#include "Waypoint.h"
#include "Engine/Engine.h"


AChiAIController::AChiAIController(const FObjectInitializer& ObjectInitializer) : ABaseAIController(ObjectInitializer){
}


void AChiAIController::BeginPlay() {
	Super::BeginPlay();

	PlayerRef = Cast<AMokosh>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	ResetBT();

	ChiRef = Cast<AChi>(GetPawn());
	if (ChiRef == nullptr) return;
	State = ChiStates::Neutral;
}

void AChiAIController::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);	
}

void AChiAIController::OnPossess(APawn* InPawn) {
	Super::OnPossess(InPawn);
	ResetBT();
}

void AChiAIController::ResetBT() {
	// set blackboard chi state to neutral initially
	BlackboardComponent->SetValueAsEnum("ChiState", (uint8)ChiStates::Neutral);
	if (PlayerRef == nullptr) return;
	BlackboardComponent->SetValueAsVector("TargetLoc", PlayerRef->GetActorLocation());
	BlackboardComponent->SetValueAsObject("PlayerObj", PlayerRef);
	BlackboardComponent->SetValueAsBool("CatapultReady", false);
	BlackboardComponent->SetValueAsObject("Waypoint", Cast<AWaypoint>(PlayerRef->ChiFollowPoint->GetChildActor()));
}

void AChiAIController::ChangeChiState(ChiStates newState) {
	if (newState == ChiStates::Neutral) PlayerRef->CheckDistance = true;
	else PlayerRef->CheckDistance = false;
	State = newState;
	BlackboardComponent->SetValueAsEnum("ChiState", (uint8)newState);
}

ChiStates AChiAIController::GetState()
{
	return State;
}

void AChiAIController::UpdateTargetLoc(FVector newLocation) {
	BlackboardComponent->SetValueAsVector("TargetLoc", newLocation);
}

void AChiAIController::ResetCatapult() {
	BlackboardComponent->SetValueAsBool("CatapultReady", false);
}


void AChiAIController::CancelCatapult() {
	BlackboardComponent->SetValueAsEnum("ChiState", (uint8)ChiStates::Neutral);
	ResetCatapult();
}

bool AChiAIController::CheckChiBehindPlayer()
{
	if (PlayerRef == nullptr) return false;
	FVector ChiToPlayer = PlayerRef->GetActorLocation() - ChiRef->GetActorLocation();
	FVector ForwardVec = PlayerRef->GetActorForwardVector();
	float DotProd = FVector::DotProduct(ChiToPlayer, ForwardVec);
	// get angle between vectors
	float Angle = FMath::RadiansToDegrees(acosf(DotProd / (ChiToPlayer.Size() * ForwardVec.Size()))); 
	
	if (Angle < 60) {
		//GEngine->AddOnScreenDebugMessage(-1, 0.001f, FColor::Green, TEXT("dot prod angle true"));
		return true;
	}

	else if (PlayerRef->CheckChiDistance() > 650) return true;

	return false;
}

bool AChiAIController::CheckWaypointDistance()
{
	if (PlayerRef == nullptr) return false;
	if (PlayerRef->CheckChiDistance() < 100) {
		//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("waypoint distance false"));
		return false;
	}

	return true;
}
