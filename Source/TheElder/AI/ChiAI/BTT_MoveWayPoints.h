// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_MoveWayPoints.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UBTT_MoveWayPoints : public UBTTaskNode
{
	GENERATED_BODY()

protected:
	class AChiAIController* AIComp;
	bool bMoving;

public:
	UBTT_MoveWayPoints(const FObjectInitializer & ObjectInitializer);

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) override;
};
