// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "BoidLeaf.generated.h"

UCLASS()
class THEELDER_API ABoidLeaf : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UShapeComponent* CollisionBody;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UProjectileMovementComponent* Movement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flocking")
		bool WatchMe;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flocking")
		float alignmentWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flocking")
		float cohesionWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flocking")
		float separationWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flocking")
		float containmentWeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flocking")
		float BaseMoveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flocking")
		float MaxMoveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Separation")
		float AvoidanceRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Containment")
		FVector Center;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Containment")
		float Radius;

	UPROPERTY()
		FTimerHandle FlyTimer;

	TArray<AActor*> neighbors;
protected:


	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	ABoidLeaf();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void UpdateAcceleration();
	
	void UpdateNeighbors();

	// FLOCKING BEHAVIOURS
	FVector Alignment();
	FVector Cohesion();
	FVector Separation();

	// QUEUEING BEHAVIORS
	FVector Containment();

	// COMBINING BEHAVIORS
	FVector Composite(TArray<FVector> behaviors, TArray<float> weights);

	UFUNCTION(BlueprintCallable)
	class UProjectileMovementComponent* GetProjMovementComp() const;
};
