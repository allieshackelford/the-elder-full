// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"

ABaseAIController::ABaseAIController(const FObjectInitializer& ObjectInitializer) : AAIController(ObjectInitializer) {
	BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComponent"));
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));
}

void ABaseAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	// start the behaviour tree
	if (BehaviorComponent != nullptr && BehaviorTree != nullptr)
	{
		if (BehaviorTree->BlackboardAsset != nullptr)
		{
			BlackboardComponent->InitializeBlackboard(*BehaviorTree->BlackboardAsset);
		}

		BehaviorComponent->StartTree(*BehaviorTree);
	}
}

UBlackboardComponent* ABaseAIController::GetBlackboardComponent() {
	return BlackboardComponent;
}