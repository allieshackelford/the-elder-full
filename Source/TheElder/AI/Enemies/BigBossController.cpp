// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BigBossController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BigBossCharacter.h"
#include "Kismet/GameplayStatics.h"

ABigBossController::ABigBossController(const FObjectInitializer& ObjectInitializer) : ABaseAIController(ObjectInitializer)
{
	
}

void ABigBossController::BeginPlay()
{
	Super::BeginPlay();
}

void ABigBossController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
}

void ABigBossController::SetPlayer(AActor* Player)
{
	auto World = GetWorld();
	if (BlackboardComponent != nullptr)
	{
		//Just a backup, we can lazily get the player
		if (Player == nullptr)
		{
			Player = UGameplayStatics::GetPlayerCharacter(World, 0);
		}
		BlackboardComponent->SetValueAsObject(PlayerKey, Player);
	}
}

EBossPhaseEnum ABigBossController::GetPhase() const
{
	if (BlackboardComponent != nullptr)
	{
		// Tell the blackboard to enter phase 1
		return static_cast<EBossPhaseEnum>(BlackboardComponent->GetValueAsEnum(CurrentPhaseKey));
	}
	return EBossPhaseEnum::BP_Intro;
}

void ABigBossController::SetPhase(EBossPhaseEnum NewPhase)
{
	if (GetCharacter() == nullptr)
		return;
	auto BossCharacter = Cast<ABigBossCharacter>(GetCharacter());
	
	switch(NewPhase)
	{
	case EBossPhaseEnum::BP_Intro:
		if (BossCharacter != nullptr)
		{
			BossCharacter->IntroPhaseActivated();
		}
		break;
	case EBossPhaseEnum::BP_Phase1Intro:

		if (BossCharacter != nullptr)
		{
			BossCharacter->Phase1IntroActivated();
		}
		break;
	case EBossPhaseEnum::BP_Phase1:

		if (BossCharacter != nullptr)
		{
			BossCharacter->Phase1Activated();
		}
		break;
	case EBossPhaseEnum::BP_ClosingPhase:

		if (BossCharacter != nullptr)
		{
			BossCharacter->ClosingPhaseActivated();
		}
		break;
	default:
		break;
	}
	BlackboardComponent->SetValueAsEnum(CurrentPhaseKey, static_cast<uint8>(NewPhase));
}