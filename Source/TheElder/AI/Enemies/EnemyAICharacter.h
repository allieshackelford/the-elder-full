// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AI/BaseAI/AICharacter.h"
#include "EnemyAICharacter.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API AEnemyAICharacter : public AAICharacter
{
	GENERATED_BODY()
public:
	AEnemyAICharacter();
};
