// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UserWidget.h"
#include "Components/BoxComponent.h"
#include "Systems/CheckpointResettable.h"
#include "BossVisibilityZone.generated.h"

UCLASS()
class THEELDER_API ABossVisibilityZone : public AActor, public ICheckpointResettable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABossVisibilityZone();
	
	UFUNCTION(BlueprintNativeEvent, Category = "Checkpoints")
	void ResetActor(uint8 StateToResetTo);
	virtual void ResetActor_Implementation(uint8 StateToResetTo) override;


	UFUNCTION(BlueprintNativeEvent)
	void BossSmashedDown();
	void BossSmashedDown_Implementation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(BlueprintReadWrite)
	UUserWidget* BossUIWidget;

	UPROPERTY()
	float ElapsedVisibleTime;
	UPROPERTY()
	bool bIssuedWarning;
	UPROPERTY()
	bool bIssuedAttack;
	UPROPERTY()
	bool bLookAtPlayer;
public:
	UFUNCTION(BlueprintNativeEvent, Category = "Effects")
	void FadeOutOverlay();
	void FadeOutOverlay_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "Effects")
	void TickUIEffect(float Value);
	void TickUIEffect_Implementation(float Value);

	UFUNCTION(BlueprintNativeEvent, Category = "Effects")
	void IssueWarning();
	void IssueWarning_Implementation();
	
	UFUNCTION(BlueprintNativeEvent, Category = "Effects")
	void LookAtPlayer();
	void LookAtPlayer_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "Effects")
	void AttackPlayer();
	void AttackPlayer_Implementation();
	
	void ResetZone();
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	UBoxComponent* DetectorBox;
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SecondsUntilWarning = 10;
	/* The number of seconds after the warning until Okean attacks*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SecondsUntilAttack = 10;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SecondsUntilLook = 10;
public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void PlayerOverlappedDetectorBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	                                 UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	                                 const FHitResult& SweepResult);
	UFUNCTION()
	void PlayerEndOverlappedDetectorBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	                                    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION(BlueprintCallable)
	bool IsInBossZone();
	UFUNCTION(BlueprintCallable)
	void SetInSubZone(int Priority);
	UFUNCTION(BlueprintCallable)
	void SetOutOfSubZone(int Priority);

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> BossSeenPlayerWidget;

	// Sub zone stuff
private:
	int CurrentPriority = -1;
public:
	UPROPERTY(BlueprintReadWrite)
	bool bInHiddenArea = false;
};
