// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BigBossCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Animations/BigBossAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "BigBossController.h"
#include "Components/SphereComponent.h"
#include "Player/Mokosh.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/Engine.h"
#include "Components/SplineComponent.h"
#include "MasterIncludes.h"

ABigBossCharacter::ABigBossCharacter()
{
	PlayerDetector = CreateDefaultSubobject<USphereComponent>(TEXT("PlayerDetector"));
	PlayerDetector->SetupAttachment(RootComponent);

	RightHandSapDetector = CreateDefaultSubobject<UBoxComponent>(TEXT("Right Hand sap detector"));
	if (GetMesh() != nullptr)
	{
		RightHandSapDetector->SetupAttachment(GetMesh(), TEXT("wrist_right"));
	}

	PlayerLaunchSpline = CreateDefaultSubobject<USplineComponent>(TEXT("Player Launch Spline"));
	PlayerLaunchSpline->SetupAttachment(RootComponent);

	RightShoulderLaunchSpline = CreateDefaultSubobject<USplineComponent>(TEXT("Right Shoulder Launch Spline"));
	RightShoulderLaunchSpline->SetupAttachment(RootComponent);
	PlayerGrabbedLaunchSpline = CreateDefaultSubobject<USplineComponent>(TEXT("Player GrabbedLaunch Spline"));
	PlayerGrabbedLaunchSpline->SetupAttachment(RootComponent);
	
	BoxRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Box Root"));
	BoxRoot->SetupAttachment(RootComponent);
}

void ABigBossCharacter::BeginPlay()
{
	Super::BeginPlay();

	PlayerDetector->OnComponentBeginOverlap.AddDynamic(this, &ABigBossCharacter::OnPlayerDetectorOverlapBegin);
	
	auto World = GetWorld();
	
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_None);
	auto CurrentAnimInstance = Cast<UBigBossAnimInstance>(GetMesh()->GetAnimInstance());
	if (CurrentAnimInstance != nullptr)
	{
		AnimInstance = CurrentAnimInstance;
	}


	BossController = Cast<ABigBossController>(GetController());
	BossController->SetPhase(CurrentTestPhase);

	if (RightShoulderStabPoint != nullptr)
	{
		RightShoulderStabPoint->OnStabbed.AddDynamic(this, &ABigBossCharacter::OnStabbedRightShoulder);
	}
	if (OtherShoulderStabPoint != nullptr)
	{
		OtherShoulderStabPoint->OnStabbed.AddDynamic(this, &ABigBossCharacter::OnStabbedOtherShoulder);
	}
}

void ABigBossCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	UpdatePlayerLocation();
	/*auto Zone = GetFloorPlayerZone(UGameplayStatics::GetPlayerCharacter(this, 0));
	if (Zone.TheBox != nullptr)
	{
		auto EnumName = GetEnumValueAsString<EFloorAnims>(TEXT("EFloorAnims"), Zone.FloorAnim);
		LOG(EnumName, 1)
	}*/
	//auto SmashOffset = UKismetMathLibrary::TransformDirection(GetActorTransform(), PlayerSmashOffset);

}



void ABigBossCharacter::OnPlayerDetectorOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto Mokosh = Cast<AMokosh>(OtherActor);
	if (Mokosh != nullptr)
	{
		BossController->SetPlayer(OtherActor);
		PlayerRef = Mokosh;
	}
}

// Dead Code
/*void ABigBossCharacter::RightHandSapDetectorAnimEvent()
{
	AnimInstance->bIsStuckInSap = true;
	
	//GetWorld()->GetTimerManager().SetTimer(TimerHandle_SeperateOtherHand, this, &ABigBossCharacter::SeperateOtherHand, HoldSapTime);
}

void ABigBossCharacter::SeperateOtherHand()
{
	AnimInstance->bSeperateLeftHand = true;
	TurnOffBoolAnim(&AnimInstance->bSeperateLeftHand);
}*/

FFloorArea ABigBossCharacter::GetFloorPlayerZone(AActor* Player)
{
	if (Player == nullptr)
		return FFloorArea();

	auto PossibleIndex = -1;
	for (int i = 0; i < FloorAreas.Num(); ++i)
	{
		if (FloorAreas[i].TheBox == nullptr)
			continue;
		auto TheBox = FloorAreas[i].TheBox->Bounds.GetBox();
		if (TheBox.IsInsideXY(Player->GetActorLocation()))
		{
			if (PossibleIndex == -1 || FloorAreas[PossibleIndex].Priority < FloorAreas[i].Priority)
			{
				PossibleIndex = i;
			}
		}
		
	}
	if (PossibleIndex == -1)
	{
		return FFloorArea();
	}
	return FloorAreas[PossibleIndex];
}

void ABigBossCharacter::OnConstruction(const FTransform& Transform)
{
	FAttachmentTransformRules AttachmentRules = { EAttachmentRule::KeepWorld, true};
	// Gotta attach all child components
	for (auto& Elem : ChildObjectsMap)
	{
		for (int i = 0; i < Elem.Value.Arr.Num(); i++)
		{
			if (Elem.Value.Arr[i] != nullptr)
			{
				if (Elem.Value.Arr[i]->IsRootComponentMovable())
				{
					Elem.Value.Arr[i]->AttachToComponent(GetMesh(), AttachmentRules, Elem.Key);
				}
				else
				{
					GEngine->AddOnScreenDebugMessage(	-1, 5.f, FColor::Red, 
						FString::Printf(
							TEXT("ERROR: The object %s, is not Moveable, attaching to bone %s won't work properly!!!"),
							*Elem.Value.Arr[i]->GetHumanReadableName(), *Elem.Key.ToString())
					);
				}
			}
		}
	}

	// Just setting up Floor Areas
	for (int i = 0; i < FloorAreas.Num(); i++)
	{
		if (FloorAreas[i].TheBox == nullptr)
		{
			auto Box = NewObject<UBoxComponent>(this, UBoxComponent::StaticClass(), FName(*FString::Printf(TEXT("Box %d"), i)));
			Box->RegisterComponent();
			Box->AttachToComponent(BoxRoot, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
			this->AddInstanceComponent(Box);
			Box->SetCanEverAffectNavigation(false);
			Box->SetCollisionResponseToAllChannels(ECR_Ignore);
			FloorAreas[i].TheBox = Box;
		}
		if (FloorAreas[i].TextDisplay == nullptr)
		{
			FloorAreas[i].TextDisplay = NewObject<UTextRenderComponent>(this, UTextRenderComponent::StaticClass(), FName("Text"));
			FloorAreas[i].TextDisplay->RegisterComponent();
			FloorAreas[i].TextDisplay->AttachToComponent(FloorAreas[i].TheBox, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false));
			FloorAreas[i].TextDisplay->SetHiddenInGame(true);
			FloorAreas[i].TextDisplay->bAbsoluteScale = true;

			FloorAreas[i].TextDisplay->SetRelativeScale3D(FVector::OneVector * 5.0f);
		}
		FText EnumName = FText::FromString(GetEnumValueAsString<EFloorAnims>(TEXT("EFloorAnims"), FloorAreas[i].FloorAnim));
		FloorAreas[i].TextDisplay->SetText(EnumName);
	}

	// Now to attach custom stuffs
	if (RightShoulderStabPoint != nullptr)
	{
		FAttachmentTransformRules Rules = {EAttachmentRule::KeepWorld, true};
		RightShoulderStabPoint->AttachToComponent(GetMesh(), Rules, RightShoulderStabPointBoneName);
	}

	// Now to attach custom stuffs
	if (OtherShoulderStabPoint != nullptr)
	{
		FAttachmentTransformRules Rules = { EAttachmentRule::KeepWorld, true };
		OtherShoulderStabPoint->AttachToComponent(GetMesh(), Rules, OtherShoulderStabPointName);
	}
}

void ABigBossCharacter::SetAlphaFade(float Value)
{
	if (BossController == nullptr)
		return;
	if (BossController->GetPhase() == EBossPhaseEnum::BP_Phase1)
	{
		AnimInstance->SmashDownAlpha = Value;
	}
}

///////////////////////////////////
//           Intro Phase		 //
///////////////////////////////////

void ABigBossCharacter::IntroPhaseActivated_Implementation()
{

}

///////////////////////////////////
//      Phase 1 Intro			 //
///////////////////////////////////

void ABigBossCharacter::OnStabbedRightShoulder_Implementation()
{
}

void ABigBossCharacter::Phase1IntroActivated_Implementation()
{
	//AnimInstance->bWakeUpRightShoulder = true;
}

void ABigBossCharacter::ThrowPlayerOffRightShoulder_Implementation()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
	auto Player = GetPlayerRef();
	if (Player == nullptr)
		return;
	
	Player->TriggerStaff(false);
	Player->UpdateState(EPlayerStates::BossThrown);
}

void ABigBossCharacter::RightShoulderTossFinished()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
	auto Player = Cast<AMokosh>(UGameplayStatics::GetPlayerCharacter(World, 0));
	if (Player == nullptr)
		return;

	Player->TriggerChi();
	Player->UpdateState(EPlayerStates::Neutral);
	BossController->SetPhase(EBossPhaseEnum::BP_Phase1);
}

///////////////////////////////////
//           Phase 1             //
///////////////////////////////////

void ABigBossCharacter::Phase1Activated_Implementation()
{

}

void ABigBossCharacter::PlaySmashEffects_Implementation()
{
	
}

void ABigBossCharacter::GroundFistSmash_Implementation(EFloorAnims FloorAnim)
{
		//SetSmashType(ESmashTargetType::STT_FollowPlayer);
	AnimInstance->FloorAnim = FloorAnim;
	AnimInstance->bFloorSmashAttack = true;
	TurnOffBoolAnim(&AnimInstance->bFloorSmashAttack);

}

void ABigBossCharacter::SetSmashType(ESmashTargetType NewType)
{
	SmashTargetType = NewType;
}

void ABigBossCharacter::OnStabbedOtherShoulder_Implementation()
{
}

/*void ABigBossCharacter::OnStabbedOtherShoulder()
{
	BossController->SetPhase(EBossPhaseEnum::BP_ClosingPhase);
}*/

///////////////////////////////////
//      Closing Phase            //
///////////////////////////////////

void ABigBossCharacter::ClosingPhaseActivated_Implementation()
{
}

void ABigBossCharacter::ThrowPlayerOffOtherShoulder_Implementation()
{
	
}

///////////////////////////////////
//   End Boss Phases Section	 //
///////////////////////////////////

AMokosh* ABigBossCharacter::GetPlayerRef()
{
	if (PlayerRef == nullptr)
	{
		auto Player = UGameplayStatics::GetPlayerCharacter(this, 0);
		PlayerRef = Cast<AMokosh>(Player);
	}
	return PlayerRef;
}


void ABigBossCharacter::UpdatePlayerLocation()
{
	auto World = GetWorld();

	PlayerRef = GetPlayerRef();
	
	if (AnimInstance != nullptr && PlayerRef != nullptr)
	{
		AnimInstance->HoverLocation = PlayerRef->GetActorLocation() + PlayerHoverOffset;
		
		switch (SmashTargetType)
		{
		case ESmashTargetType::STT_FollowPlayer:
			{
				auto Loc = GetPlayerRef()->GetActorLocation() + PlayerSmashOffset;
				AnimInstance->SmashLocation = Loc;
				break;
			}
			case ESmashTargetType::STT_SmashOnTarget:
			{
				auto Loc = NewCustomSmashTarget + PlayerSmashOffset;
				AnimInstance->SmashLocation = Loc;
				break;
			}
			case ESmashTargetType::STT_Wait:
			{
				break;
			}
		}
	}
}

//////////////////////////////////////////////////////////
//         Silly timer delays for anim graph section	//
//////////////////////////////////////////////////////////

// ReSharper disable once CppMemberFunctionMayBeStatic
void ABigBossCharacter::RemoveBool(bool* BoolValue) 
{
	*BoolValue = false;
}

void ABigBossCharacter::TurnOffBoolAnim(bool* value)
{
	FTimerDelegate InDelegate;
	InDelegate.BindUObject(this, &ABigBossCharacter::RemoveBool, value);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_TurnOfBoolAnim, InDelegate, 0.1f, false);
}