// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AI/Enemies/EnemyAICharacter.h"
#include "Animations/BigBossAnimInstance.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "World/BossStabPoint.h"
#include "Player/Mokosh.h"
#include "Components/SplineComponent.h"
#include "AI/Enemies/BossEnums.h"
#include "Components/TextRenderComponent.h"
#include "BigBossCharacter.generated.h"

USTRUCT(BlueprintType)
struct FFloorArea
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadOnly, meta = (DisplayName = "Actor"))
	UBoxComponent* TheBox;
	UPROPERTY(BlueprintReadOnly, meta = (DisplayName = "Actor"))
	UTextRenderComponent* TextDisplay;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (DisplayName = "Animation"))
	EFloorAnims FloorAnim;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Priority = 1;
};

/*
 * This struct is needed for a stupid workaround because we cannot put a TArray inside a TMap,
 * but we can put a TArray in a struct in a TMap
 */
USTRUCT(BlueprintType)
struct FAttachedActors
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = ( DisplayName = "Actors" ))
	TArray<AActor*> Arr;
};



/**
 * Snaaaaaaaake Eaaateeeeer
 * https://www.youtube.com/watch?v=ygguAS_kARQ
 */
UCLASS()
class THEELDER_API ABigBossCharacter : public AEnemyAICharacter
{
	GENERATED_BODY()
public:
	ABigBossCharacter();
	
	void BeginPlay() override;
	void Tick(float DeltaSeconds) override;
	
	UFUNCTION()
	void OnPlayerDetectorOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	                                  const FHitResult& SweepResult);
	
	void OnConstruction(const FTransform& Transform) override;
	UFUNCTION(BlueprintCallable)
	FFloorArea GetFloorPlayerZone(AActor* Player);

	// Anim bool turn off
	void RemoveBool(bool* BoolValue);
	void TurnOffBoolAnim(bool* value);

	// Anim IK alpha magic 
	void SetAlphaFade(float Value);

	void UpdatePlayerLocation();
	

	// Components
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	USplineComponent* PlayerLaunchSpline;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	USplineComponent* RightShoulderLaunchSpline;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	USplineComponent* PlayerGrabbedLaunchSpline;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	USphereComponent* PlayerDetector;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	UBoxComponent* RightHandSapDetector;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	USceneComponent* BoxRoot;
	
	UPROPERTY(EditAnywhere)
	EBossPhaseEnum CurrentTestPhase = EBossPhaseEnum::BP_Intro;
	
	/*
	 * Intro Phase
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Boss")
	void IntroPhaseActivated();
	void IntroPhaseActivated_Implementation();

	// Variables for stabbing the right shoulder
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!Boss")
	ABossStabPoint* RightShoulderStabPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!Boss")
	FName RightShoulderStabPointBoneName = TEXT("upperarm_r");

	UFUNCTION(BlueprintNativeEvent)
	void OnStabbedRightShoulder();
	
	/*
	 * Phase 1 Intro
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Boss")
	void Phase1IntroActivated();
	void Phase1IntroActivated_Implementation();

	/* This function should be called when the player finishes getting tossed by the boss*/
	UFUNCTION(BlueprintCallable)
	void RightShoulderTossFinished();

	/* This is called from an Anim event*/
	UFUNCTION(BlueprintNativeEvent, Category = "Boss")
	void ThrowPlayerOffRightShoulder();
	void ThrowPlayerOffRightShoulder_Implementation();

	/*
	 * Phase 1
	*/
	UFUNCTION(BlueprintNativeEvent, Category = "Boss")
	void Phase1Activated();
	void Phase1Activated_Implementation();
	
	UFUNCTION(BlueprintNativeEvent, Category = "Boss")
	void PlaySmashEffects();
	void PlaySmashEffects_Implementation();

	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void GroundFistSmash(EFloorAnims FloorAnim);
	void GroundFistSmash_Implementation(EFloorAnims FloorAnim);
	UFUNCTION(BlueprintCallable)
	void SetSmashType(ESmashTargetType NewType);
	
	UFUNCTION(BlueprintNativeEvent, Category = "Boss")
	void OnStabbedOtherShoulder();
	void OnStabbedOtherShoulder_Implementation();

	UPROPERTY(EditAnywhere, meta = (MakeEditWidget = true), Category = "!Boss")
	FVector GroundLocation;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (MakeEditWidget = true), Category = "!Boss")
	FVector WaitingFistLocation;
	UPROPERTY(EditAnywhere, meta = (MakeEditWidget = true), Category = "!Boss")
	FVector GroundBuildingSmashLocation;
	UPROPERTY(EditAnywhere, Category = "!Boss")
	FVector PlayerSmashOffset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "!Boss")
	FVector PlayerHoverOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!Boss")
	ESmashTargetType SmashTargetType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!Boss")
	FVector NewCustomSmashTarget;


	// Floor Animation stuffs
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!Boss")
	TArray<FFloorArea> FloorAreas;

	/*
	 * Closing Phase
	*/

	
	UFUNCTION(BlueprintNativeEvent, Category = "Boss")
	void ClosingPhaseActivated();
	void ClosingPhaseActivated_Implementation();
	
	UFUNCTION(BlueprintNativeEvent, Category = "Boss")
	void ThrowPlayerOffOtherShoulder();
	void ThrowPlayerOffOtherShoulder_Implementation();

	// Variables for stabbing the right shoulder
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!Boss")
	ABossStabPoint* OtherShoulderStabPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "!Boss")
	FName OtherShoulderStabPointName = TEXT("RightArm_Result");

	/*
	 * Phase section ends
	*/
	
	/* A map of the actors that need to be stuck to each of the bones of Okean*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "!Boss")
	TMap<FName, FAttachedActors> ChildObjectsMap;


	
	UPROPERTY(EditDefaultsOnly, Category = "Animations")
	UAnimMontage* HandSmashMontage;
	UPROPERTY(EditDefaultsOnly, Category = "Animations")
	FName HandSmashSectionName = TEXT("SmashDown");


	FTimerHandle TimerHandle_TurnOfBoolAnim;

	// Dead code
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tunables")
	float HoldSapTime;

	// Just a cached player
	UPROPERTY()
	AMokosh* PlayerRef;

protected:
	UPROPERTY(BlueprintReadWrite)
	UBigBossAnimInstance* AnimInstance;
	UPROPERTY(BlueprintReadWrite)
	FVector GroundPlayerLocation;
private:
	UPROPERTY()
	class ABigBossController* BossController;

	/* Just a simple function to get the player so that we can get the player character*/
	UFUNCTION(BlueprintCallable)
	AMokosh* GetPlayerRef();
};