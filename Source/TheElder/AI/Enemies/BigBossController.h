// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AI/BaseAI/BaseAIController.h"
#include "BigBossCharacter.h"
#include "BigBossController.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API ABigBossController : public ABaseAIController
{
	GENERATED_BODY()
public:
	ABigBossController(const FObjectInitializer& ObjectInitializer);
	void BeginPlay() override;
	void OnPossess(APawn* InPawn) override;
	void SetPlayer(AActor* Player);
	UFUNCTION(BlueprintCallable)
	EBossPhaseEnum GetPhase() const;
	UFUNCTION(BlueprintCallable)
	void SetPhase(EBossPhaseEnum NewPhase);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Blackboard Keys")
	FName CurrentPhaseKey = TEXT("CurrentPhase");
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Blackboard Keys")
	FName PlayerKey = TEXT("Player");
};