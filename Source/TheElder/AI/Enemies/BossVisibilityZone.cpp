// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BossVisibilityZone.h"
#include "MasterIncludes.h"

// Sets default values
ABossVisibilityZone::ABossVisibilityZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	
	DetectorBox = CreateDefaultSubobject<UBoxComponent>(TEXT("DetectorBox"));
	DetectorBox->SetCollisionResponseToAllChannels(ECR_Ignore);
	DetectorBox->SetCollisionResponseToChannel(ECC_MokoshChannel, ECR_Overlap);
	RootComponent = DetectorBox;
	
}

// Called when the game starts or when spawned
void ABossVisibilityZone::BeginPlay()
{
	Super::BeginPlay();
	
	DetectorBox->OnComponentBeginOverlap.AddDynamic(this, &ABossVisibilityZone::PlayerOverlappedDetectorBox);
	DetectorBox->OnComponentEndOverlap.AddDynamic(this, &ABossVisibilityZone::PlayerEndOverlappedDetectorBox);
	auto GameMode = UElderFunctionLibrary::GetElderGameMode(this);
	GameMode->CurrentActiveZone = this;
}

void ABossVisibilityZone::ResetActor_Implementation(uint8 StateToResetTo)
{
	SetActorTickEnabled(true);
	ElapsedVisibleTime = 0;
	CurrentPriority = -1;
	bIssuedAttack = false;
	bIssuedWarning = false;
	bLookAtPlayer = false;
}

void ABossVisibilityZone::BossSmashedDown_Implementation()
{
	ResetZone();
}

// Called every frame
void ABossVisibilityZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsInBossZone() == false)
	{
		SetActorTickEnabled(false);
		ElapsedVisibleTime = 0;
		bIssuedWarning = false;
		bIssuedAttack = false;
		bLookAtPlayer = false;
		return;
	}
	if (bInHiddenArea)
	{
		CurrentPriority = -1;
		FadeOutOverlay();
		SetActorTickEnabled(false);
		ElapsedVisibleTime = 0;
		bIssuedWarning = false;
		bIssuedAttack = false;
		bLookAtPlayer = false;
		return;
	}
	ElapsedVisibleTime += DeltaTime;

	if (ElapsedVisibleTime >= SecondsUntilWarning && bIssuedWarning == false)
	{
		IssueWarning();
	}
	if (ElapsedVisibleTime >= SecondsUntilWarning + SecondsUntilAttack && bIssuedAttack == false)
	{
		AttackPlayer();
	}
	if (ElapsedVisibleTime >= SecondsUntilLook && bLookAtPlayer == false)
	{
		LookAtPlayer();
	}
	TickUIEffect(ElapsedVisibleTime/(SecondsUntilWarning + SecondsUntilAttack));
}

void ABossVisibilityZone::FadeOutOverlay_Implementation()
{
	
}

void ABossVisibilityZone::TickUIEffect_Implementation(float Value)
{
	
}

void ABossVisibilityZone::IssueWarning_Implementation()
{
	bIssuedWarning = true;
}

void ABossVisibilityZone::LookAtPlayer_Implementation()
{
	bLookAtPlayer = true;
}

void ABossVisibilityZone::AttackPlayer_Implementation()
{
	bIssuedAttack = true;
}

void ABossVisibilityZone::ResetZone()
{
	TickUIEffect(0);
	SetActorTickEnabled(false);
	ElapsedVisibleTime = 0;
	bIssuedWarning = false;
	bIssuedAttack = false;
	bLookAtPlayer = false;
}

void ABossVisibilityZone::PlayerOverlappedDetectorBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto Player = Cast<AMokosh>(OtherActor);
	if (Player == nullptr)
		return;

	// If we don't already have an active widget, add one
	if (BossUIWidget == nullptr)
	{
		BossUIWidget = CreateWidget(GetWorld(), BossSeenPlayerWidget);
		if (BossUIWidget != nullptr)
		{
			BossUIWidget->AddToViewport(-1);
		}
	}

	SetActorTickEnabled(true);
}

void ABossVisibilityZone::PlayerEndOverlappedDetectorBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	auto Player = Cast<AMokosh>(OtherActor);
	if (Player == nullptr)
		return;

	if (BossUIWidget != nullptr)
	{
		BossUIWidget->RemoveFromParent();
		BossUIWidget = nullptr;
	}

	ResetZone();
}

bool ABossVisibilityZone::IsInBossZone()
{
	return BossUIWidget != nullptr;
}

void ABossVisibilityZone::SetInSubZone(int Priority)
{

	bInHiddenArea = true;
	SetActorTickEnabled(false);
	bIssuedAttack = false;
	bIssuedWarning = false;
	bLookAtPlayer = false;
	FadeOutOverlay();
}

void ABossVisibilityZone::SetOutOfSubZone(int Priority)
{

	bInHiddenArea = false;
	ResetZone();
	SetActorTickEnabled(true);
}