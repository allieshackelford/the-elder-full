#pragma once
#include "CoreMinimal.h"
#include "BossEnums.generated.h"

USTRUCT() struct FDummyStruct { GENERATED_BODY() };

UENUM(BlueprintType)
enum class EFloorAnims : uint8
{
	FA_LongSmash	 	UMETA(DisplayName = "Long Smash"),
	FA_RegularSmash 	UMETA(DisplayName = "Regular Smash"),
	FA_NoSmash 			UMETA(DisplayName = "No Smash")
};

UENUM(BlueprintType)
enum class EBossPhaseEnum : uint8
{
	BP_Intro 		UMETA(DisplayName = "Intro Phase"),
	BP_Phase1Intro	UMETA(DisplayName = "Phase 1 Intro"),
	BP_Phase1 		UMETA(DisplayName = "Phase 1"),
	BP_Phase2		UMETA(DisplayName = "Phase 2"),
	BP_Phase3		UMETA(DisplayName = "Phase 3"),
	BP_ClosingPhase	UMETA(DisplayName = "Closing Phase")
};

UENUM(BlueprintType)
enum class ESmashTargetType : uint8
{
	STT_FollowPlayer 		UMETA(DisplayName = "Follow Player"),
	STT_Wait		 		UMETA(DisplayName = "Wait"),
	STT_SmashOnTarget		UMETA(DisplayName = "Smash Custom Target"),
};