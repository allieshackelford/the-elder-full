// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BTT_PlayHandSmashAnim.h"
#include "AIController.h"
#include "Enemies/BigBossCharacter.h"

EBTNodeResult::Type UBTT_PlayHandSmashAnim::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto BigBoss = Cast<ABigBossCharacter>(OwnerComp.GetAIOwner()->GetCharacter());
	if (BigBoss != nullptr)
	{
		//BigBoss->PlayGroundSmashAttack();
	}
	return EBTNodeResult::Succeeded;
}
