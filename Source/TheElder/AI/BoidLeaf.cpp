  // Fill out your copyright notice in the Description page of Project Settings.


#include "BoidLeaf.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
ABoidLeaf::ABoidLeaf()
{
	CollisionBody = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionBody"));
	RootComponent = CollisionBody;

	Movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Movement"));

	Movement->SetUpdatedComponent(CollisionBody);

	Movement->bShouldBounce = false;
	Movement->ProjectileGravityScale = 0.0f;
	Movement->InitialSpeed = 400.0f;
	Movement->MaxSpeed= 450.0f;

	Movement->bIsHomingProjectile = true;
	Movement->HomingAccelerationMagnitude = 1000;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABoidLeaf::BeginPlay()
{
	Super::BeginPlay();

	UpdateAcceleration();
}

// Called every frame
void ABoidLeaf::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//UpdateNeighbors();
}

void ABoidLeaf::UpdateAcceleration()
{
	Movement->MaxSpeed += FMath::RandRange(-50, 50);
	
	if(Movement->MaxSpeed < 200)
	{
		Movement->MaxSpeed += 75;
	}
	else if (Movement->MaxSpeed > 650)
	{
		Movement->MaxSpeed -= 75;
	}

	GetWorld()->GetTimerManager().SetTimer(FlyTimer, this, &ABoidLeaf::UpdateAcceleration, 3.0f);
}

void ABoidLeaf::UpdateNeighbors() {
	GetOverlappingActors(neighbors, ABoidLeaf::StaticClass());

	FVector alignment = Alignment();
	FVector cohesion = Cohesion();
	FVector separation = Separation();
	FVector containment = Containment();

	TArray<FVector> behaviors = { alignment, cohesion, separation, containment };
	TArray<float> weights = { alignmentWeight, cohesionWeight, separationWeight, containmentWeight };

	CollisionBody->AddForce(Composite(behaviors, weights) * BaseMoveSpeed);
}

FVector ABoidLeaf::Alignment() {
	int neighborCount = neighbors.Num();
	if (neighborCount == 0) {
		return GetActorUpVector();
	}

	FVector newLocation(0,0,0);
	// for each neighbor, add velocity and average it
	for (int i = 0; i < neighborCount; i++)
	{
		//ABoidLeaf* friendLeaf = Cast<ABoidLeaf>(neighbors[i]);
		newLocation += neighbors[i]->GetActorUpVector();
	}

	newLocation /= neighborCount;
	return newLocation;
}

FVector ABoidLeaf::Cohesion() {
	int neighborCount = neighbors.Num();

	if (neighborCount == 0) {
		return GetActorUpVector();
	}


	FVector newLocation(0,0,0);

	// for each neighbor, add velocity and average it
	for (int i = 0; i < neighborCount; i++)
	{
		newLocation += neighbors[i]->GetActorLocation();
	}

	newLocation /= neighborCount;
	newLocation -= GetActorLocation();

	return newLocation;
}

FVector ABoidLeaf::Separation() {
	int neighborCount = neighbors.Num();

	if (neighborCount == 0) {
		return GetActorUpVector();
	}

	FVector newLocation(0,0,0);
	int nAvoid = 0;

	// for each neighbor, add velocity and average it
	for(int i = 0; i < neighborCount; i++)
	{
		FVector CheckThis = neighbors[i]->GetActorLocation() - GetActorLocation();
		if (CheckThis.SizeSquared() < AvoidanceRadius) {
			nAvoid++;
			newLocation += (GetActorLocation() - neighbors[i]->GetActorLocation());
		}
	}

	if (nAvoid > 0) {
		newLocation /= nAvoid;
	}

	return newLocation;
}

FVector ABoidLeaf::Containment() {
	FVector centerOffset = Center - GetActorLocation();

	float t = centerOffset.Size() / Radius;

	if (t < 0.9f) {
		return FVector(0, 0, 0);
	}

	return centerOffset * t * t;
}


FVector ABoidLeaf::Composite(TArray<FVector> behaviors, TArray<float> weights) {
	FVector move(0, 0, 0);

	for (int i = 0; i < behaviors.Num(); i++) {
		FVector partialMove = behaviors[i] * weights[i];

		if (partialMove != FVector(0, 0, 0)) {
			if (partialMove.SizeSquared() > (weights[i] * weights[i])) {
				partialMove.Normalize();
				partialMove *= weights[i];
			}
			move += partialMove;
		}
	}

	return move;
}

UProjectileMovementComponent * ABoidLeaf::GetProjMovementComp() const
{
	return Movement;
}
