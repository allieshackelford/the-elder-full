// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BigBossAlphaFadeAnimNotify.h"
#include "MasterIncludes.h"
#include "Components/SkeletalMeshComponent.h"
#include "BigBossAnimInstance.h"
#include "AI/Enemies/BigBossCharacter.h"
#include "Animation/AnimSequence.h"

void UBigBossAlphaFadeAnimNotify::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
	float TotalDuration)
{
	auto BigBoss = Cast<ABigBossCharacter>(MeshComp->GetOwner());
	if (BigBoss != nullptr)
	{
		float AValue = 0.0f, BValue = 1.0f;
		if (bReverseFade)
		{
			AValue = 1.0f;
			BValue = 0.0f;
		}
		
		auto Fade = UKismetMathLibrary::Ease(AValue, BValue, /*Alpha: */0.0f, EaseType);
		if (bIsDebugging)
		{
			LOG(FString::Printf(TEXT("Begin: %f"), Fade), 10)
			
		}
		BigBoss->SetAlphaFade(Fade);
	}

	// We need to reset CurrentTime because the AnimNotify uobject is reused
	CurrentTime = 0.0f;

	StateDuration = TotalDuration;
}

void UBigBossAlphaFadeAnimNotify::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
	float FrameDeltaTime)
{
	CurrentTime += FrameDeltaTime;
	auto BigBoss = Cast<ABigBossCharacter>(MeshComp->GetOwner());
	if (BigBoss != nullptr)
	{
		float AValue = 0.0f, BValue = 1.0f;
		if (bReverseFade)
		{
			AValue = 1.0f;
			BValue = 0.0f;
		}
		auto Fade = FMath::Clamp(UKismetMathLibrary::Ease(AValue, BValue, CurrentTime/StateDuration, EaseType, 2.0f, 2), 0.0f, 1.0f);
		if (bIsDebugging)
		{
			UE_LOG(LogTemp, Warning, TEXT("Tick: %f(val), %f(CurrentTime)"), Fade, CurrentTime)
		}
		BigBoss->SetAlphaFade(Fade);
	}
}

void UBigBossAlphaFadeAnimNotify::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	auto BigBoss = Cast<ABigBossCharacter>(MeshComp->GetOwner());
	if (BigBoss != nullptr)
	{
		float AValue = 0.0f, BValue = 1.0f;
		if (bReverseFade)
		{
			AValue = 1.0f;
			BValue = 0.0f;
		}
		auto Fade = UKismetMathLibrary::Ease(AValue, BValue, /*Alpha: */1.0f, EaseType);
		if (bIsDebugging)
		{
			UE_LOG(LogTemp, Warning, TEXT("Final: %f"), Fade)
		}
		BigBoss->SetAlphaFade(Fade);
	}

	// We need to reset CurrentTime because the AnimNotify uobject is reused
	CurrentTime = 0.0f;
}