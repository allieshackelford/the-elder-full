// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "MokoshAnimInstance.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Engine/Engine.h"

void UMokoshAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
	MokoshOwner = Cast<AMokosh>(TryGetPawnOwner());
}

void UMokoshAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	// Check for pawn or reinitialize
	if (MokoshOwner == nullptr)
	{
		NativeInitializeAnimation();
		return;
	}

	PlayerState = MokoshOwner->GetState();
	JoystickDirectionX = MokoshOwner->InputDirectionX;
	JoystickDirectionY = MokoshOwner->InputDirectionY;
	ClimbJump = MokoshOwner->IsClimbJumping;
	DetachFromClimbing = MokoshOwner->IsDetachClimbing;
	MantleUpLedge = MokoshOwner->IsMantleLedge;
	// variable now set in bp_mokosh for cinematic
	//EnvironmentStab = MokoshOwner->IsStabbing;
	DistanceFromGround = MokoshOwner->CheckFloorDist();

	DownwardVelocity = MokoshOwner->GetVelocity().Z;
	MokoshVelocity = MokoshOwner->GetVelocity();

	CrystalAiming = MokoshOwner->IsAiming;

	ClimbingDirection = MokoshOwner->ClimbingDirection;


	// reset trigger variables if set
	if (ClimbJump) MokoshOwner->IsClimbJumping = false;
	if (DetachFromClimbing) MokoshOwner->IsDetachClimbing = false;
	if (MantleUpLedge) MokoshOwner->IsMantleLedge = false;
	if (EnvironmentStab) MokoshOwner->IsStabbing = false;
	//FRotator CharacterAimDirection = AnimOwner->GetAimDirection();
	//AimPitch = CharacterAimDirection.Pitch;
}
