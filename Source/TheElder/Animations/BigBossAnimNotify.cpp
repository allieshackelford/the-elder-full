// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BigBossAnimNotify.h"
#include "Engine/Engine.h"
#include "Components/SkeletalMeshComponent.h"
#include "AI/Enemies/BigBossCharacter.h"
#include "MasterIncludes.h"
#include "AI/Enemies/BossVisibilityZone.h"


void UBigBossAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	auto BigBoss = Cast<ABigBossCharacter>(MeshComp->GetOwner());
	if (BigBoss == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red,
			TEXT("BigBossAnimNotify was called on an object that is not ABigBoss!"));
		return;
	}
	
	switch (NotifyFunction)
	{
	case EAnimNotifyFunctionsEnum::ANF_FistSapCheck:
		{
			//BigBoss->RightHandSapDetectorAnimEvent();
			break;
		}
	case EAnimNotifyFunctionsEnum::ANF_ThrowPlayerOffRightShoulder:
		{
			BigBoss->ThrowPlayerOffRightShoulder();
			break;
		}
	case EAnimNotifyFunctionsEnum::ANF_ThrowPlayerOffLeftShoulder:
	{
		BigBoss->ThrowPlayerOffOtherShoulder();
		break;
	}
	case EAnimNotifyFunctionsEnum::ANF_KillPlayer:
		{
			auto GameMode = UElderFunctionLibrary::GetElderGameMode(MeshComp);
			BigBoss->PlaySmashEffects();
			if (BigBoss->SmashTargetType != ESmashTargetType::STT_SmashOnTarget)
			{
				GameMode->CurrentActiveZone->BossSmashedDown();
			}
			BigBoss->SetSmashType(ESmashTargetType::STT_Wait);
			
			break;
		}
	default:
		{
		auto EnumName = GetEnumValueAsString<EAnimNotifyFunctionsEnum>(TEXT("EAnimNotifyFunctionsEnum"), NotifyFunction);
		GEngine->AddOnScreenDebugMessage(-1, 20, FColor::Red, 
			FString::Printf(TEXT("ERROR: Anim Notify called with unimplemented Enum: %s"), *EnumName));
		}
	}
	Super::Notify(MeshComp, Animation);
}
