// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animations/BaseAnimInstance.h"
#include "ChiAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UChiAnimInstance : public UBaseAnimInstance
{
	GENERATED_BODY()

protected:
	class AChi* ChiOwner;
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		float AimOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SapThrow")
		bool bIsSapping = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SapThrow")
		bool bThrowSap = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Catapult")
		bool bIsCatapultStart = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Catapult")
		bool bIsCatapultIdle = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Catapult")
		bool bIsCatapultEnd = false;

protected:
	UFUNCTION()
	virtual void NativeInitializeAnimation() override;
	
	UFUNCTION()
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
};
