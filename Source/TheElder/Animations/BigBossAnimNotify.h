// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "BigBossAnimNotify.generated.h"

UENUM(BlueprintType)
enum class EAnimNotifyFunctionsEnum : uint8
{
	ANF_FistSapCheck 	UMETA(DisplayName = "FirstSapCheck"),
	ANF_ThrowPlayerOffRightShoulder 	UMETA(DisplayName = "Throw Player Off Right Shoulder"),
	ANF_ThrowPlayerOffLeftShoulder 	UMETA(DisplayName = "Throw Player Off Left Shoulder"),
	ANF_KillPlayer 	UMETA(DisplayName = "Kill Player"),
};

/**
 * 
 */
UCLASS()
class THEELDER_API UBigBossAnimNotify : public UAnimNotify
{
	GENERATED_BODY()
public:
	void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Notify")
	EAnimNotifyFunctionsEnum NotifyFunction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Notify")
	bool bIsDebugging = false;
};