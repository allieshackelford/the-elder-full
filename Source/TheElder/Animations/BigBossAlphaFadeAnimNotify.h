// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "Kismet/KismetMathLibrary.h"
#include "BigBossAlphaFadeAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UBigBossAlphaFadeAnimNotify : public UAnimNotifyState
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Category = Fade)
	TEnumAsByte<EEasingFunc::Type> EaseType;
	
	UPROPERTY(EditAnywhere, Category = Fade)
	bool bReverseFade = false;

	UPROPERTY(EditAnywhere, Category = Fade)
	bool bIsDebugging = false;

	// Used for the tween
	UPROPERTY()
	float CurrentTime = 0.0f;
	UPROPERTY()
	float StateDuration = 0.0f;
private:
	void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
	void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime) override;
	void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
