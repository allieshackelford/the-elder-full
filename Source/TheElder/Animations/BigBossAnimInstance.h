// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
//#include "Structs.h"
#include "AI/Enemies/BossEnums.h"
#include "BigBossAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UBigBossAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	/*All these properties should be EditAnywhere and BlueprintReadWrite so you can test out animations in the graph*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bFloorSmashAttack = false;
	/* This is when Okean is waiting to attack while the player is on the ground*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bWaitingForAttack = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bSeperateLeftHand = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bIsStuckInSap = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bFlickPlayer = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bSmashMud = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default", meta = (MakeEditWidget = true))
	FVector PlayerLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default", meta = (MakeEditWidget = true))
	FVector SmashLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default", meta = (MakeEditWidget = true))
	FVector HoverLocation;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bWakeUpRightShoulder = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bHoverHand = false;

	/* This is used in the ground smash anim to tell which animation to play*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	EFloorAnims FloorAnim;

	// Anim graph alphas

	// This is used to fade between the IKs
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	float SmashDownAlpha;
};
