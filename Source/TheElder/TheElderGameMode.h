// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "World/ElderCheckpoint.h"
#include "ArchiveReplaceOrClearExternalReferences.h"
#include "UI/LoadingScreen.h"
#include "Utility/CheckpointFunctionLibrary.h"
#include "Utility/CheckpointFunctionLibrary.h"
#include "TheElderGameMode.generated.h"

class AMokosh;
class AChi;

/**
 * 
 */
UCLASS()
class THEELDER_API ATheElderGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UPostProcessComponent* PostProcess;

	ATheElderGameMode();

	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

	UFUNCTION(BlueprintCallable)
	void SetBrightness(float GammaVal);

	UFUNCTION(Exec, Category = ExecFunctions)
	void ClimbingDisable(int IsOn);
	UFUNCTION(BlueprintCallable, Category = "Checkpoints")
	int RegisterCheckpoint(AElderCheckpoint* Checkpoint);
	UFUNCTION(BlueprintCallable, Category = "Checkpoints")
	AElderCheckpoint* GetCheckpointByIndex(int Index);
	/* Pauses player movement and gravity, used to stop the player from moving while loading sublevels*/
	void PausePlayerMovement();
	void UnpausePlayerMovement();
	virtual void BeginPlay() override;
	UFUNCTION()
	void LoadNextSubLevel();
	UFUNCTION()
	void UnloadNextSubLevel();

	UPROPERTY(BlueprintReadOnly)
	class UElderSystemsDataAsset* ElderSystemsData;

	/* Get AMokosh, which is cached in the Game Mode on startup*/
	UFUNCTION(BlueprintPure)
	AMokosh* GetMokoshCharacter() const;
	/* Get AChi, which is cached in the Game Mode on startup*/
	UFUNCTION(BlueprintPure)
	AChi* GetChiCharacter() const;
	void SetupLoadingScreen();

	UFUNCTION()
	void PlayerDied();
private:
	/* Called by PlayerDied when the Death wipe animation is done*/
	UFUNCTION()
	void DeathFadeInDone();
	UFUNCTION()
	void FadeBackToGameplay();

	/* If the player is currently in the middle of respawning*/
	bool bIsRespawning = false;

private:
	void SetupSublevels();
	UPROPERTY()
	TArray<AElderCheckpoint*> Checkpoints;

	UPROPERTY()
	TArray<FString> CachedSublevelsToLoad;
	UPROPERTY()
	TArray<FString> CachedSublevelsToUnload;

	AMokosh* MokoshCharacter;
	AChi* ChiCharacter;

	/* This is set when the sub levels are finished loading after loading the game mode*/
	bool bInitialSubLevelsLoaded = false;
	
	// Used for Loading Screen stuff
	UPROPERTY()
	TSubclassOf<ULoadingScreen> LoadingScreenClass;
	UPROPERTY()
	ULoadingScreen* LoadingScreen;

	// Some lazy singletons
public:
	UPROPERTY(BlueprintReadOnly)
	class ABossVisibilityZone* CurrentActiveZone;

	// UI Stuffs
	UPROPERTY(BlueprintReadWrite)
	class UMainMenu* MainMenu;
	UPROPERTY(BlueprintReadWrite)
	class UDebugMenu* DebugMenu;
	UPROPERTY(BlueprintReadWrite)
	class UOptionsMenu* OptionsMenu;
	UPROPERTY(BlueprintReadWrite)
	class UPauseMenu* PauseMenu;
	UPROPERTY(BlueprintReadWrite)
	class UQAMenu* QAMenu;
	
private:

	// Resettables

	/*TMap variables are Actor, and actor state respectively*/
	UPROPERTY()
	TMap<AActor*, uint8> TrackedResettables;
	
public:
	UFUNCTION(BlueprintCallable, Category = "Checkpoints")
	void UpdateResettable(TScriptInterface<class ICheckpointResettable> Resettable, uint8 StateToUpdate);
};
