// Fill out your copyright notice in the Description page of Project Settings.


#include "Mokosh.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Components/InputComponent.h"
#include "Components/ChildActorComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "AI/ChiAI/Chi.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/Engine.h"
#include "Camera/SapViewActor.h"
#include "Player/ClimbingComponent.h"
#include "Player/CatapultComponent.h"
#include "Player/SapThrowComponent.h"
#include "World/BouncyMushroom.h"
#include "Systems/DamageType_Mud.h"
#include "TheElderGameMode.h"
#include "AI/ChiAI/Waypoint.h"
#include "Player/ElderPlayerController.h"
#include "Kismet/KismetMathLibrary.h"
#include "AI/Enemies/BigBossCharacter.h"
#include "UI/ChiIndicator.h"
#include "Utility/ElderFunctionLibrary.h"
#include "Animations/ChiAnimInstance.h"
#include "AI/ChiAI/ChiAIController.h"
  
AMokosh::AMokosh() {
	// setting player walk and jump metrics
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->bApplyGravityWhileJumping = true;
	GetCharacterMovement()->JumpZVelocity = 625;
	GetCharacterMovement()->MaxWalkSpeed = 400;
	GetCharacterMovement()->MinAnalogWalkSpeed = 100;
	GetCharacterMovement()->AirControl = 0.4f;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 450.0f, 0.0f); // set how fast player rotates to face direction

	// player rotation not affected by the camera
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;


	// placeholder staff mesh
	StaffMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaffMesh"));
	StaffMesh->SetupAttachment(RootComponent);

	WingedStaffMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WingedStaffMesh"));
	WingedStaffMesh->SetupAttachment(RootComponent);
	WingedStaffMesh->SetVisibility(false);
	WingedStaffMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// follow point for chi
	ChiFollowPoint = CreateDefaultSubobject<UChildActorComponent>(TEXT("ChiFollowPoint"));
	ChiFollowPoint->SetupAttachment(RootComponent);

	StaffCrystalSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("StaffCrystalSpawnPoint"));
	StaffCrystalSpawnPoint->SetupAttachment(RootComponent);

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<UElderSpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	
	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	prevPitch = 0;
	State = EPlayerStates::Neutral;
	ChiCam = false;
	wasClimbing = false;
	doneClimbing = false;
	staffMode = false;
	IsSpawnForMechanic = false;
	ShowCatapultSpline = false;

	CheckForChiSpawn = false;
	CheckDistance = true;

	// REGISTER MECHANIC COMPONENTS
	ClimbingComponent = CreateDefaultSubobject<UClimbingComponent>(TEXT("ClimbingSystem"));

	CatapultComponent = CreateDefaultSubobject<UCatapultComponent>(TEXT("CatapultComponent"));

	SapThrowComponent = CreateDefaultSubobject<USapThrowComponent>(TEXT("SapThrowComponent"));
}

void AMokosh::BeginPlay() {
	Super::BeginPlay();
	
	MokoshPlayerController = Cast<AElderPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	
	CamManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);

	CamManager->ViewPitchMax = 43;
	CamManager->ViewPitchMin = -75;

	WaypointRef = Cast<AWaypoint>(ChiFollowPoint->GetChildActor());

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassToFind, FoundActors);
	// error checker
	if (FoundActors.Num() > 0) {
		ChiRef = Cast<AChi>(FoundActors[0]);
	}

	if (ChiRef == nullptr) {
		FVector location = GetActorLocation();
		location.X += 45;
		location.Z += 5;
		ChiRef = Cast<AChi>(GetWorld()->SpawnActor<AChi>(ClassToFind, location, GetActorRotation()));
	}
	
	ClimbingComponent->Init(GetCharacterMovement(), GetCapsuleComponent());

	OnTakeAnyDamage.AddDynamic(this, &AMokosh::OnTakenAnyDamage);
	OnTakePointDamage.AddDynamic(this, &AMokosh::OnTakenPointDamage);
}

void AMokosh::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	// ********* PRINTING STATE ****************
	//GEngine->AddOnScreenDebugMessage(-1, 0.001f, FColor::Yellow, FString::Printf(TEXT("STATE: %d"), (uint8)State));

	if (CheckDistance) {
		// check for chi distance
		if (CheckChiDistance() > 3000) {
			ChiRef->SpawnBehindPlayer(GetActorLocation());
		}
	}

	if (CheckForChiSpawn) {
		// check if mokosh is far enough away from ledge/walls
		TriggerChi();
	}


	if(staffMode && State != EPlayerStates::Cinematic)
	{
		CatapultComponent->CheckForGround();
	}

	if(GetCharacterMovement()->IsFalling() && CheckFloorDist() > 600)
	{
		TriggerStaff(true);
	}

	// =========================== STATE CHECKS ================================
	if (State == EPlayerStates::Neutral) { 
		if (wasClimbing) {
			// EXIT CLIMBING
			wasClimbing = false;
			doneClimbing = true;
		}

		// if done climbing or in staff mode
		if (doneClimbing) {
			CatapultComponent->CheckForGround();
		}
	}

	if (State == EPlayerStates::Climbing) {
		if (wasClimbing == false) {
			// change chi to staff mode
			TriggerStaff(false);

			// CLIMBING
			CameraBoom->ChangeCamera(ECameraTypes::Climbing);
			wasClimbing = true;
		}
	}

	if (State == EPlayerStates::Bouncing) {
		if (staffMode == false) {
			TriggerStaff(false);
		}
		CheckBounceStop();
	}
}

void AMokosh::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	// movement inputs
	PlayerInputComponent->BindAxis("Horizontal", this, &AMokosh::MoveHorizontal);
	PlayerInputComponent->BindAxis("Vertical", this, &AMokosh::MoveVertical);

	// action inputs
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);


	// camera inputs
	PlayerInputComponent->BindAxis("RHorizontal", this, &AMokosh::TurnAtRate);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMokosh::TurnAtRate);
	PlayerInputComponent->BindAxis("RVertical", this, &AMokosh::LookUpAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMokosh::LookUpAtRate);


	// mechanic inputs
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AMokosh::Interact);

	PlayerInputComponent->BindAction("Catapult", IE_Pressed, CatapultComponent, &UCatapultComponent::InitCatapult);
	PlayerInputComponent->BindAction("Cancel", IE_Pressed, this, &AMokosh::CancelPressed);

	PlayerInputComponent->BindAction("SapAim", IE_Pressed, SapThrowComponent, &USapThrowComponent::EnterSapAim);
	PlayerInputComponent->BindAction("SapAim", IE_Released, SapThrowComponent, &USapThrowComponent::ExitSapAim);
	PlayerInputComponent->BindAction("SapThrow", IE_Pressed, SapThrowComponent, &USapThrowComponent::SapThrow);
	PlayerInputComponent->BindAction("SapThrow", IE_Released, SapThrowComponent, &USapThrowComponent::SapThrowInputReleased);

	// summon chi
	PlayerInputComponent->BindAction("CallChi", IE_Pressed, this, &AMokosh::SummonChi);
}

AChi* AMokosh::GetChiRef() {
	return ChiRef;
}

EPlayerStates AMokosh::GetState() {
	return State;
}

void AMokosh::UpdateState(EPlayerStates newState) {
	if (newState == EPlayerStates::Bouncing) {
		if (CheckBounceStart() == false) {
			return;
		}
	}

	State = newState;

	//TODO: add delegate callback
}

bool AMokosh::IsStaffMode()
{
	return staffMode;
}

void AMokosh::SummonChi()
{
	if(TeleportChi() == false)
	{
		MokoshPlayerController->mChiIndicator->ToggleChiIndicator(false);
	}
}


void AMokosh::SetCollisionToIgnore() {
	// change chi collision to ignore
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
	// change mokosh collision to ignore
	ChiRef->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
}

void AMokosh::ResetCollisions() {
	// change chi collision to block
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Block);
	// change mokosh collision to block
	ChiRef->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Block);

}

FVector AMokosh::GetFollowRelativeLocation()
{
	return ChiFollowPoint->GetRelativeTransform().GetLocation();
}

void AMokosh::SetFollowRelativeLocation(FVector lerpLoc)
{
	ChiFollowPoint->SetRelativeLocation(lerpLoc);
}

// ============ INPUT ACTIONS ====================
void AMokosh::CancelPressed()
{
	if (GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Custom &&
		GetCharacterMovement()->CustomMovementMode == static_cast<uint8>(ECustomMovementModesEnum::MME_Climbing))
	{
		CameraBoom->bUsePawnControlRotation = true;
		ClimbingComponent->DetachFromClimbing();
		// change camera back to normal
		CameraBoom->ChangeCamera(ECameraTypes::Normal);
	}
	else
	{
		CatapultComponent->CancelCatapult();
	}
}

void AMokosh::Interact()
{
	if (State == EPlayerStates::Cinematic) return;
	// If we're already climbing, don't do anything. We only want this to be a force initial climb thing
	if (GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Custom &&
		GetCharacterMovement()->CustomMovementMode == static_cast<uint8>(ECustomMovementModesEnum::MME_Climbing))
	{
		return;
	}
	CancelSapAim();
	ClimbingComponent->ForceInitClimb();
}

// ======================= CHECKS FOR CHI ==========================
float AMokosh::CheckChiDistance() {
	// pretty aggressive right now, need to streamline
	float distance = FVector::Distance(GetActorLocation(), ChiRef->GetActorLocation());

	return distance;
}
   
// respawn chi after staff form
bool AMokosh::CheckRespawnChi() {
	bool Checker = false;
	// check if mokosh is away from ledge (edge whiskers)
	const FName TraceTag("EdgeTest");

	FHitResult hit;
	FCollisionQueryParams params(TraceTag, true, this);
	params.AddIgnoredActor(ChiRef);
	
	// start player position
	FVector Start = GetActorLocation();
	FVector End;
	FVector playerForward = GetActorForwardVector();

	End = Start - (playerForward * 200.0f);
	End.Z -= 300;
	// check edge behind short
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) == false && GetWorld()->LineTraceSingleByChannel(hit,Start, End, ECC_GameTraceChannel4, params) == false)
	{
		Checker = true;
	}


	End = Start - (playerForward * 400.0f);
	End.Z -= 300;
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) == false && GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params) == false)
	{
		Checker = true;
	}

	
	End = Start - (playerForward * 600.0f);
	End.Z -= 300;
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) == false && GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params) == false)
	{
		Checker = true;
	}

	
	// check area above mokosh
	End = Start - (playerForward * 300.0f);
	End.Z += 450.0f;
	if(GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) || GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params))
	{
		Checker = true;
	}


	// check wall collision
	End = Start - (playerForward * 500.0f);
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) || GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params))
	{
		Checker = true;
	}

	// check below chi spawn point and see if its hitting a mushroom
	Start = GetActorLocation() - (playerForward * 320.0f);
	End = Start;
	End.Z -= 650.0f;

	if (IsChiNavigable(Start, End)) Checker = true;
	
	if (GetWorld()->SweepSingleByChannel(hit, Start, End, FQuat::Identity, ECC_WorldStatic, FCollisionShape::MakeSphere(70.0f), params))
	{
		if (hit.Actor != nullptr)
		{
			ABouncyMushroom* IsMushroom = Cast<ABouncyMushroom>(hit.Actor);
			if (IsMushroom != nullptr) Checker = true;
		}
	}

	
	// trace complex against big boss, per poly collision
	FCollisionObjectQueryParams ObjectParams(ECC_Pawn);
	
	if (GetWorld()->LineTraceTestByObjectType(Start, End, ObjectParams, params))
	{
		Checker = true;
	}

	if (Checker || State == EPlayerStates::Cinematic) return false;

	return true;
}

bool AMokosh::TriggerChiOutOfCamera()
{
	bool Checker = false;
	// check if mokosh is away from ledge (edge whiskers)
	const FName TraceTag("EdgeTest");

	FHitResult hit;
	FCollisionQueryParams params(TraceTag, true, this);
	params.AddIgnoredActor(ChiRef);

	//GetWorld()->DebugDrawTraceTag = TraceTag;
	
	// CAMERA POSITION CHECKS
	FVector CamForward = CamManager->GetActorForwardVector();
	FVector SpawnPoint = CamManager->GetCameraLocation() - (CamForward * 120.0f);
	// spawn position for checks
	FVector Start = CamManager->GetCameraLocation() - (CamForward * 50.0f);
	FVector End;
	
	// check points along line between spawn point and player 
	for(int i = 1; i < 15; i++)
	{
		Start = SpawnPoint + (CamForward * (55.0f * i));
		End = Start;
		End.Z -= 2000;
		// if close enough to player, stop checking
		float DistanceToPlayer = FVector2D::Distance(FVector2D(GetActorLocation().X, GetActorLocation().Y), FVector2D(Start.X, Start.Y));
		if (DistanceToPlayer < 100) break;
		
		if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) || GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params))
		{
			float HeightDiff = FMath::Abs(hit.Location.Z - GetActorLocation().Z);
			if (HeightDiff > 200) Checker = true;
		}
		else
		{
			Checker = true;
		}
	}

	// check in radius around spawn point
	// behind
	Start = CamManager->GetCameraLocation() - (CamForward * 50.0f);
	End = Start - (CamForward * 350.0f);
	End.Z -= 2000;
	
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) || GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params))
	{
		float HeightDiff = FMath::Abs(hit.Location.Z - GetActorLocation().Z);
		if (HeightDiff > 200) Checker = true;
	}
	else
	{
		Checker = true;
	}

	// front
	End = Start + (CamForward * 350.0f);
	End.Z -= 2000;
	
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) || GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params))
	{
		float HeightDiff = FMath::Abs(hit.Location.Z - GetActorLocation().Z);
		if (HeightDiff > 200) Checker = true;
	}
	else
	{
		Checker = true;
	}

	// right
	End = Start + (CamManager->GetActorRightVector() * 350.0f);
	End.Z -= 2000;

	
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) || GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params))
	{
		float HeightDiff = FMath::Abs(hit.Location.Z - GetActorLocation().Z);
		if (HeightDiff > 200) Checker = true;
	}
	else
	{
		Checker = true;
	}

	// left
	End = Start - (CamManager->GetActorRightVector() * 350.0f);
	End.Z -= 2000;
	
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params) || GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params))
	{
		float HeightDiff = FMath::Abs(hit.Location.Z - GetActorLocation().Z);
		if (HeightDiff > 200) Checker = true;
	}
	else
	{
		Checker = true;
	}
	

	
	Start = CamManager->GetCameraLocation() - (CamForward * 50.0f);
	End = Start;
	End.Z -= 2000;

	if (IsChiNavigable(Start, End)) Checker = true;
	
	// check below chi spawn point and see if its hitting a mushroom
	if (GetWorld()->SweepSingleByChannel(hit, Start, End, FQuat::Identity, ECC_WorldStatic, FCollisionShape::MakeSphere(70.0f), params))
	{
		if (hit.Actor != nullptr)
		{
			ABouncyMushroom* IsMushroom = Cast<ABouncyMushroom>(hit.Actor);
			if (IsMushroom != nullptr) Checker = true;
		}
	}

	// trace complex against big boss, per poly collision
	FCollisionObjectQueryParams ObjectParams(ECC_Pawn);

	if (GetWorld()->LineTraceTestByObjectType(Start, End, ObjectParams, params))
	{
		Checker = true;
	}
	

	if (Checker || State == EPlayerStates::Cinematic) return false;

	return true;
}

void AMokosh::TriggerStaff(bool ShouldBeWings) {
	MokoshPlayerController->mChiIndicator->ToggleStaffIndicator();
	if (staffMode && !ShouldBeWings) return;
	// change chi to staff mode
	if (ShouldBeWings) {
		WingedStaffMesh->SetVisibility(true);
		PlayWingStaffMontage();
		ChangeStaffMaterial(true);
		//StaffMesh->SetVisibility(true);
		if (staffMode) return;
	}
	else
	{
		WingedStaffMesh->SetVisibility(false);
		//StaffMesh->SetVisibility(true);
		ChangeStaffMaterial(true);
		//StaffMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		if (staffMode) return;
	}
	
	staffMode = true;
	ChiRef->ChangeToStaff();
}

void AMokosh::TriggerChi() {
	// if mechanic, spawn chi closer to mokosh
	if(IsSpawnForMechanic)
	{
		if(CheckRespawnChi())
		{
			ChiRef->ChangeFromStaff(this);

			MokoshPlayerController->mChiIndicator->ResetIndicator();
			WingedStaffMesh->SetVisibility(false);
			ChangeStaffMaterial(false);
			CheckForChiSpawn = false;
			staffMode = false;

			IsSpawnForMechanic = false;
		}
	}
	// if not mechanic, spawn chi behind camera
	else
	{
		if(TriggerChiOutOfCamera()) {
			ChiRef->ChangeFromStaff(CamManager);
		
			MokoshPlayerController->mChiIndicator->ResetIndicator();
			WingedStaffMesh->SetVisibility(false);
			ChangeStaffMaterial(false);
			CheckForChiSpawn = false;
			staffMode = false;
		}
		/*
		 * 
		else
		{
			if (CheckRespawnChi())Fc
			{
				ChiRef->ChangeFromStaff(this);

				MokoshPlayerController->mChiIndicator->ResetIndicator();
				WingedStaffMesh->SetVisibility(false);
				ChangeStaffMaterial(false);
				CheckForChiSpawn = false;
				staffMode = false;

				IsSpawnForMechanic = false;
			}
		}
		 */
	}
}

bool AMokosh::TeleportChi() {
	if (CheckRespawnChi() == false) {
		return false;
	}

	TriggerStaff(false);
	IsSpawnForMechanic = true;
	
	return true;
}

// ================ MOVEMENT ==================

void AMokosh::MoveHorizontal(float Value) {
	if (State == EPlayerStates::CatInit || State == EPlayerStates::CatLoaded || State == EPlayerStates::CatLoading ||
		State == EPlayerStates::BossThrown || State == EPlayerStates::Mantling || State == EPlayerStates::Cinematic) return;
	if (Controller != nullptr && Value != 0.0f) {
		if (GetCharacterMovement()->MovementMode != MOVE_Custom)
		{
			// find out which way is forward
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			// get forward vector
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
			AddMovementInput(Direction, Value);
		}
		// Send information to climbing system
		ClimbingComponent->SendHorizontalInput(Value);

	}
	InputDirectionX = Value;
}

void AMokosh::MoveVertical(float Value) {
	if (State == EPlayerStates::CatInit || State == EPlayerStates::CatLoaded || State == EPlayerStates::CatLoading ||
		State == EPlayerStates::BossThrown || State == EPlayerStates::Mantling || State == EPlayerStates::Cinematic) return;
	if (Controller != nullptr && Value != 0.0f) {
		if (GetCharacterMovement()->MovementMode != MOVE_Custom)
		{
			// find out which way is right
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			// get right vector 
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
			// add movement in that direction
			AddMovementInput(Direction, Value);
		}

		// Send information to climbing system
		ClimbingComponent->SendVerticalInput(Value);
	}
	InputDirectionY = Value;
}

void AMokosh::TurnAtRate(float Rate)
{
	if (State == EPlayerStates::Climbing || State == EPlayerStates::Mantling || State == EPlayerStates::Cinematic) return;

	// calculate waypoint position based on camera location
	if (Rate != 0.0f)
	{
		FVector CameraToPlayer = GetActorLocation() - CamManager->GetCameraLocation();
		FVector ForwardVec = GetActorForwardVector();
		float DotProd = FVector::DotProduct(CameraToPlayer, ForwardVec);
		// get angle between vectors
		float Angle = FMath::RadiansToDegrees(acosf(DotProd / (CameraToPlayer.Size() * ForwardVec.Size())));
		if(Angle > 100)
		{
			WaypointRef->CalculateWaypointPos(CamManager->GetCameraLocation());
			
		}
	}
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * TurnRate * GetWorld()->GetDeltaSeconds());
}

void AMokosh::LookUpAtRate(float Rate)
{
	if (State == EPlayerStates::Climbing || State == EPlayerStates::Mantling || State == EPlayerStates::Cinematic) return;
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * LookUpRate * GetWorld()->GetDeltaSeconds());

	// adjust distance on pitch change 
	if (State == EPlayerStates::Neutral)
	{
		FRotator rotation = CamManager->GetCameraRotation();
		if (prevPitch != rotation.Pitch) {
			CameraBoom->UpdateCameraPitchCurve(/*currPitch:*/ rotation.Pitch); 
			prevPitch = rotation.Pitch;
		}
	}

	if(State == EPlayerStates::SapAiming)
	{
		FRotator rotation = CamManager->GetCameraRotation();
		if (prevPitch != rotation.Pitch) {
			CameraBoom->UpdateAimingPitchCurve(/*currPitch:*/ rotation.Pitch);
			prevPitch = rotation.Pitch;
		}
	}

}

void AMokosh::RotateToFaceCamera() {
	FRotator rotation = CamManager->GetCameraRotation();

	SetActorRotation(FRotator(0, rotation.Yaw, 0));
}


// ======================== MECHANICS ==============================

// ********************JUMP**********************
void AMokosh::Jump() {
	if (State != EPlayerStates::Neutral && State != EPlayerStates::Climbing) return;

	// Let's check if our movement mode is set to climbing
	if (GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Custom &&
		GetCharacterMovement()->CustomMovementMode == static_cast<uint8>(ECustomMovementModesEnum::MME_Climbing))
	{
		CancelSapAim();
		ClimbingComponent->ForceInitClimb();
		bWillFallSave = false;
	}
	else
	{
		Super::Jump();
	}
}


// ********************** BOUNCING **********************
bool AMokosh::CheckBounceStart() {
	FHitResult hit;
	FCollisionQueryParams params(TEXT("BounceStart"), true, this);
	if(ChiRef != nullptr) params.AddIgnoredActor(ChiRef);
	// ray cast from player straight down
	FVector Start = GetActorLocation();
	FVector End = Start;
	End.Z -= 500;

	
	// edge behind
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params))
	{
		ABouncyMushroom* IsMushroom = Cast<ABouncyMushroom>(hit.Actor);
		if(IsMushroom != nullptr) return true;
	}

	return false;
}

void AMokosh::CheckBounceStop() {
	const FName TraceTag("BounceStop");
	//GetWorld()->DebugDrawTraceTag = TraceTag;

	FHitResult hit;
	FCollisionQueryParams params(TraceTag, true, this);
	params.AddIgnoredActor(ChiRef);
	// ray cast from player straight down
	FVector Start = GetActorLocation();
	FVector End = Start;
	End.Z -= 100;
	
	// edge behind
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params))
	{
		ABouncyMushroom* IsMushroom = Cast<ABouncyMushroom>(hit.Actor);
		if (IsMushroom == nullptr) State = EPlayerStates::Neutral;
	}
}

// ================== FALL SAVE ==================

float AMokosh::CheckFloorDist()
{
	FVector Start = GetActorLocation();
	FVector End = Start;
	End.Z -= 8000;


	FHitResult hit;
	FCollisionQueryParams params(TEXT("Floor Distance"), true, this);

	//DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 0.0001f);
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params)) {
		//GEngine->AddOnScreenDebugMessage(-1, 0.01f, FColor::Yellow, FString::Printf(TEXT("Hit static: %s"), *hit.GetActor()->GetName()));
		return hit.Distance;
	}

	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_GameTraceChannel4, params)) {
		//GEngine->AddOnScreenDebugMessage(-1, 0.01f, FColor::Yellow, FString::Printf(TEXT("Hit landscape: %s"), *hit.GetActor()->GetName()));
		return hit.Distance;
	}


	return 0;
}

void AMokosh::CheckShouldFallSave()
{
	FVector Start = GetActorLocation();
	FVector End = Start;
	End.Z -= 8000;

	FHitResult hit;
	const FName TraceTag("Water Check");
	//GetWorld()->DebugDrawTraceTag = TraceTag;
	FCollisionQueryParams params(TraceTag, true, this);

	// check against BP_Water and BouncyMushroom
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params)) {
		if(hit.Actor != nullptr)
		{
			if(hit.Actor->ActorHasTag(TEXT("Respawn")))
			{
				bWillFallSave = false;
				return;
			}

			ABouncyMushroom* IsMushroom = Cast<ABouncyMushroom>(hit.Actor);
			if (IsMushroom != nullptr) bWillFallSave = false;
		}
	}

	// check against BP_MovingMud
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldDynamic, params)) {
		if (hit.Actor != nullptr)
		{
			if (hit.Actor->ActorHasTag(TEXT("Respawn")))
			{
				bWillFallSave = false;
			}
		}
	}
	
	float CurrentFallDist = CheckFloorDist();
	float diffFallDistance = CurrentFallDist - prevFallDist;
	if(CurrentFallDist < prevFallDist && (diffFallDistance >= 100))
	{
		bWillFallSave = false;
	}

	prevFallDist = CurrentFallDist;
}

void AMokosh::ActivateFallSave()
{
	/*
	 * 
	float floorDist = CheckFloorDist();
	if (floorDist < 150) {
		WingedStaffMesh->SetVisibility(true);
		//StaffMesh->SetVisibility(false);
		LaunchCharacter(FVector(0, 0, 350), true, true);
		bWillFallSave = false;
	}
	 */
}

// ================== CANCEL MECHANICS ================
void AMokosh::CancelSapAim() {

	ChiRef->AnimInstance->bIsSapping = false;

	// hide reticle
	if (MokoshPlayerController != nullptr) {
		MokoshPlayerController->ToggleReticleOff();
	}

	// reset animation and camera
	IsAiming = false;
	bUseControllerRotationYaw = false;
	ChiRef->ChangeState(ChiStates::Neutral);

	if (IsStaffMode() == false) MokoshPlayerController->mChiIndicator->ResetIndicator();
	//SapThrowComponent->ExitSapAim();
}

ECameraTypes AMokosh::GetCameraType()
{
	return CameraBoom->CurrentCam;
}

// ================== DEATH AND RESETTING ================

void AMokosh::KillAndReset()
{
	if(State == EPlayerStates::CatLoaded)
	{
		CatapultComponent->CancelCatapult();
		ChiRef->CancelCatapult();
	}
	
	// set chi visibility
	ChiRef->GetMesh()->SetVisibility(false);
	ChiRef->ToggleChristmasElements(false);
	
	CancelSapAim();
	ClimbingComponent->DetachFromClimbing();
	
	UpdateState(EPlayerStates::Neutral);
	
	CameraBoom->ChangeCamera(ECameraTypes::Normal);

	auto GameMode = UElderFunctionLibrary::GetElderGameMode(this);
	if (GameMode != nullptr)
	{
		GameMode->PlayerDied();
	}
}

/* This is called in the GameMode to get Mokosh to reset his position, this should be called just before the fade out*/
void AMokosh::ResetLocation()
{
	auto GameMode = UElderFunctionLibrary::GetElderGameMode(this);
	auto CheckpointLocation = GameMode->ChoosePlayerStart(Controller);

	auto NewRotation = FRotator(0, CheckpointLocation->GetActorRotation().Yaw, 0);
	TeleportTo(CheckpointLocation->GetActorLocation(), NewRotation);

	// if staff mode, start checking for chi respawn spot
	if (staffMode) {
		CheckForChiSpawn = true;
	}
	else
	{
		FTimerDelegate TimerDel;
		TimerDel.BindUFunction(this, FName("TriggerStaff"), false);
		GetWorld()->GetTimerManager().SetTimer(ResetTimer, TimerDel, 0.15f, false);
	}
	
}

void AMokosh::OnTakenAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if (Cast<UDamageType_Mud>(DamageType) != nullptr)
	{
		// TODO: Hook up mud dying animation
	}

	KillAndReset();
}

/* Will be called by the boss on smash attacks*/
void AMokosh::OnTakenPointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation, 
	UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser)
{
	// TODO: Hook up to AnimInstance to play damage knockback animation before causing death event
	KillAndReset();
}

// ================== STAB MECHANICS ================

void AMokosh::StabCamera(FVector TargetLoc)
{ 
	// face towards
	FRotator LookRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TargetLoc);
	SetActorRotation(FRotator(0, LookRotation.Yaw, 0));
	// update state to cinematic mode
	UpdateState(EPlayerStates::Cinematic);

	FRotator CamRotation = UKismetMathLibrary::FindLookAtRotation(FollowCamera->GetComponentLocation(), TargetLoc);

	FollowCamera->SetWorldRotation(CamRotation);
	
	CameraBoom->ChangeCamera(ECameraTypes::Cinematic);

	FTimerDelegate TimerDel;
	TimerDel.BindUFunction(CameraBoom, FName("CinematicLookAtTarget"), TargetLoc);
	GetWorld()->GetTimerManager().SetTimer(CinematicTimer, TimerDel, 0.6f, false);
}
