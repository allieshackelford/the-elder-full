// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "CatapultComponent.h"
#include "Player/Mokosh.h"
#include "AI/ChiAI/Chi.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Engine/Engine.h"
#include "Animations/ChiAnimInstance.h"
#include "Player/ElderPlayerController.h"
#include "UI/ChiIndicator.h"
#include "Components/SplineMeshComponent.h"
#include "Camera/CameraComponent.h"

UCatapultComponent::UCatapultComponent()
{
	// register aim particle 
	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/ArtAssets/Effects/Cascade/Emmiters/Mechanic/P_ChiCatapult"));
	CatapultParticle = Particle.Object;

	LastAimPos = FVector(0, 0, 0);
	bIsMenuThrow = false;

	PrimaryComponentTick.bCanEverTick = true;
}


void UCatapultComponent::BeginPlay()
{
	Super::BeginPlay();

	// set inital force and zchange values for aiming
	CatForce = MinForce;
	ZChange = ZChangeMin;
	
	// should change to chi camera
	bChangeCam = false;

	// cache references to player, camManager, playercontroller
	MOwner = Cast<AMokosh>(GetOwner());
	CamManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
	Controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	// spawn particle and set to invisible
	LastParticle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CatapultParticle, FVector(0, 0, 0));
	LastParticle->SetVisibility(false);
}


void UCatapultComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// state when chi traverses to player
	if (MOwner->GetState() == EPlayerStates::CatInit) {
		CheckForCatReady();
	}

	// state when player loads into chi
	if (MOwner->GetState() == EPlayerStates::CatLoading) {
		LoadingIntoChi();

		if (MOwner->ChiCam == false) {
			// blends the camera view to be chi's aiming camera
			CamStartPos = CamManager->GetCameraRotation();
			CamEndPos = MOwner->GetChiRef()->FollowCamera->GetComponentRotation();
			CamStartTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
			bChangeCam = true;
			
			Controller->SetViewTargetWithBlend(MOwner->GetChiRef(), 0.8f, EViewTargetBlendFunction::VTBlend_EaseIn, 2, true);
			
			MOwner->ChiCam = true;
			// calls toggle chi after camera blend finished
			GetWorld()->GetTimerManager().SetTimer(MOwner->PossessTimer, this, &UCatapultComponent::ToggleChi, 0.8f);
		}
	}

	if(bChangeCam)
	{
		LerpCamera();
	}

	if(MOwner->ShowCatapultSpline && bIsMenuThrow == false)
	{
		ShowCatapultPath();
	}

	// state when aiming loaded catapult
	if (MOwner->GetState() == EPlayerStates::CatLoaded) {
		CalculateCatValues();
	}

	// state when player has been launched
	if (MOwner->GetState() == EPlayerStates::CatLaunched) {
		MOwner->ChiCam = false;
		CheckForGround();
	}

}

float UCatapultComponent::Remap(float val, FVector2D xy1, FVector2D xy2)
{
	return (val - xy1.X) / (xy1.Y - xy1.X) * (xy2.Y - xy2.X) + xy2.X;
}

// rules to check if catapult can be activated
bool UCatapultComponent::CheckIfCanActivate() {
	// state must be neutral or sap aiming
	if (MOwner->GetState() != EPlayerStates::Neutral && MOwner->GetState() != EPlayerStates::SapAiming) {
		return false;
	}

	// is mokosh on the ground?
	if (MOwner->GetCharacterMovement()->IsMovingOnGround() == false) {
		return false;
	}

	// if SAPAIMING, need to exit sap aim mode!
	if (MOwner->GetState() == EPlayerStates::SapAiming) {
		MOwner->CancelSapAim();
	}

	// is chi in staff form?
	if (MOwner->staffMode) {
		if (MOwner->CheckRespawnChi() == false) {
			return false;
		}
	}

	// how far away is chi?
	if (MOwner->CheckChiDistance() > 1000) {
		// teleport chi to be behind the player at a smaller distance
		if (MOwner->TeleportChi() == false) {
			// return false if good chi spawn position cannot be found
			return false;
		}
	}


	return true;
}

void UCatapultComponent::InitCatapult() {
	if (CheckIfCanActivate() == false) {
		MOwner->MokoshPlayerController->mChiIndicator->ToggleChiIndicator(false);
		return;
	}

	MOwner->MokoshPlayerController->mChiIndicator->ResetIndicator();
	
	
	// set chi-mokosh collision to ignore
	MOwner->SetCollisionToIgnore();

	// rotate player to face camera direction for aiming
	MOwner->RotateToFaceCamera();
	MOwner->UpdateState(EPlayerStates::CatInit);

	// AUDIO WHISTLE_CALL_CATAPULT
	MOwner->PlayWhistle(WhistleType::CallCatapult);
	
	MOwner->GetChiRef()->InitCatapult();
	MOwner->GetChiRef()->SetTargetLoc(MOwner);
	 
} 

// checks if chi has reached proper loading distance
void UCatapultComponent::CheckForCatReady() {
	if (MOwner->GetChiRef()->CatapultReady) {
		MOwner->UpdateState(EPlayerStates::CatLoading); 
		SetCatapultLoad();
	}
}

// set lerping variables for loading player into catapult
void UCatapultComponent::SetCatapultLoad() {
	StartTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	FVector targetLoc = MOwner->GetActorLocation();
	CurvePos = (targetLoc + MOwner->GetChiRef()->CatapultLoadPoint->GetComponentTransform().GetLocation()) * .5f;
	CurvePos -= FVector(0, 2, 0);
	StartPos = targetLoc - CurvePos;
	EndPos = MOwner->GetChiRef()->CatapultLoadPoint->GetComponentTransform().GetLocation() - CurvePos;
	
	// set catapult animation
	MOwner->GetChiRef()->AnimInstance->bIsCatapultStart = true;
}

// lerp player into chi loading socket
void UCatapultComponent::LoadingIntoChi() {
	float fraction = (UGameplayStatics::GetRealTimeSeconds(GetWorld()) - StartTime) / LoadTime;
	FVector lerpLoc = FMath::Lerp(StartPos, EndPos, fraction) + CurvePos;
	//FVector lerpLoc = FMath::Lerp(StartPos, EndPos, fraction);
	//MOwner->SetActorLocation(lerpLoc);

	if (fraction >= 1.0f)
	{

		MOwner->UpdateState(EPlayerStates::CatLoaded);
		// set animation bool
		MOwner->GetChiRef()->AnimInstance->bIsCatapultIdle = true;
		MOwner->MokoshPlayerController->mChiIndicator->ResetIndicator();
		FVector lookrotation = MOwner->GetChiRef()->GetActorForwardVector();
		MOwner->SetActorRotation(lookrotation.Rotation());

		MOwner->GetCharacterMovement()->SetMovementMode(MOVE_Flying);
		MOwner->GetCharacterMovement()->StopMovementImmediately();
		
		MOwner->GetChiRef()->LastParticle->SetVisibility(true);

		//MOwner->GetChiRef()->ToggleAimPivot(true);
		MOwner->ShowCatapultSpline = true;

		//fraction = 0;
		
		CurrentRotation = MOwner->GetActorRotation();
	}
}

// launch player with appropriate direction and catapult force 
void UCatapultComponent::LaunchPlayer() {
	bIsMenuThrow = false;
	
	MOwner->GetCharacterMovement()->SetMovementMode(MOVE_Walking);
	MOwner->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, false));
	FVector direction = MOwner->GetChiRef()->GetActorForwardVector();
	MOwner->SetActorRotation(FRotator(0, direction.Rotation().Yaw, 0));
	direction.Z = ZChange;
	// LAUNCH character
	MOwner->LaunchCharacter(direction * CatForce, true, true);

	MOwner->UpdateState(EPlayerStates::CatLaunched);
	// switches the player controller back to mokosh

	ToggleChi();
	MOwner->GetChiRef()->LastParticle->SetVisibility(false);

	// reset chi-mokosh collisions to block
	MOwner->ResetCollisions();
}


// Shows the catapult landing position
void UCatapultComponent::ShowCatapultPath() {
	// cat force is too large! remap value between 1000 and 2000? 
	//float LaunchDistance = Remap(CamManager->GetCameraRotation().Pitch, FVector2D(-30,15), FVector2D(800,1600));
	float LaunchDistance = Remap(MOwner->GetChiRef()->CatapultPitchValue, FVector2D(0,100), FVector2D(800,1300));
	FVector StartLocation = MOwner->GetActorLocation() + (MOwner->GetActorForwardVector() * LaunchDistance) - (MOwner->GetActorRightVector() * 100.0f);
	StartLocation.Z += 300;

	
	FVector EndLocation = StartLocation;
	EndLocation.Z -= 4000;

	FHitResult hit;
	FCollisionQueryParams params(TEXT("Path Trace"), true);

	if (MOwner->GetChiRef()->SplineMeshes[0]->IsVisible() == false) MOwner->GetChiRef()->ToggleAimPivot(true);

	// if hit surface, move sap aim particle 
	if (GetWorld()->LineTraceSingleByChannel(hit, StartLocation, EndLocation, ECC_GameTraceChannel4, params)) {
		FVector NewLoc = hit.Location;
		DrawDebugSphere(GetWorld(), NewLoc, 15.0f, 1, FColor::Red, false, 0.001f);
		if (LastAimPos == NewLoc) return;
		LastAimPos = NewLoc;
		MOwner->GetChiRef()->SetCatapultSpline(NewLoc);
		MOwner->GetChiRef()->DrawCatapultSpline();
	}

	// TODO: See if we need to readd this 
	/*
	 * 
	else
	{
		MOwner->GetChiRef()->SetCatapultSpline(StartLocation);
		MOwner->GetChiRef()->DrawCatapultSpline();
	}
	 */

}

// calculates force and zchange based on current camera pitch 
void UCatapultComponent::CalculateCatValues() {
	//float currPitch = CamManager->GetCameraRotation().Pitch;
	//FVector2D ViewPitch = FVector2D(CamManager->ViewPitchMin, CamManager->ViewPitchMax);
	if (bIsMenuThrow) {
		CatForce = 700;
		return;
	}
	CatForce = Remap(MOwner->GetChiRef()->CatapultPitchValue, FVector2D(0, 100), FVector2D(MinForce, MaxForce));
	ZChange = Remap(MOwner->GetChiRef()->CatapultPitchValue, FVector2D(0, 100), FVector2D(ZChangeMin, ZChangeMax));

	//CatForce = (currPitch - ViewPitch.X) / (ViewPitch.Y - ViewPitch.X) * (MaxForce - MinForce) + MinForce;
	//ZChange = (currPitch - ViewPitch.X) / (ViewPitch.Y - ViewPitch.X) * (ZChangeMax - ZChangeMin) + ZChangeMin;
}

// switches player controls between chi and player
void UCatapultComponent::ToggleChi() {
	if (MOwner->GetState() == EPlayerStates::CatLaunched) {
		Controller->Possess(MOwner);
		MOwner->TriggerStaff(true);
		SwitchCamera(false);
	}

	else if (MOwner->GetState() == EPlayerStates::CatLoaded) {
		MOwner->AttachToComponent(MOwner->GetChiRef()->GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true), FName("LoadSocket"));
		Controller->UnPossess();
		Controller->Possess(MOwner->GetChiRef());
		SwitchCamera(true);
	}
}

// checks if player has landed on ground to respawn chi
void UCatapultComponent::CheckForGround() {
	// set floor save to activate
	if (MOwner->CheckFloorDist() > 800) {
		MOwner->bWillFallSave = true;
		MOwner->prevFallDist = MOwner->CheckFloorDist();
	}
	
	if (MOwner->GetCharacterMovement()->IsMovingOnGround() == true) {
		MOwner->CheckForChiSpawn = true;

		// need when leaf save is implemented
		MOwner->GetCharacterMovement()->GravityScale = 1;
		if (MOwner->GetState() != EPlayerStates::SapAiming) {
			MOwner->UpdateState(EPlayerStates::Neutral); 
		}

		MOwner->doneClimbing = false;
		//MOwner->staffMode = false;

		MOwner->WingedStaffMesh->SetVisibility(false);

		MOwner->bWillFallSave = false;
	}
}

// change camera min and max pitch values for aiming
void UCatapultComponent::SwitchCamera(bool ToChi) {
	if (ToChi) {
		CamManager->ViewPitchMax = 15;
		CamManager->ViewPitchMin = -30;

		return;
	}
	CamManager->ViewPitchMax = 43;
	CamManager->ViewPitchMin = -75;
}

// resets appropriate values if player cancels out of catapult
void UCatapultComponent::CancelCatapult() {
	if (bIsMenuThrow) return;
	
	// CHECKTHIS: SETTING CHI COLLISION TO BLOCK
	MOwner->ResetCollisions();
	MOwner->GetChiRef()->LastParticle->SetVisibility(false);

	MOwner->ShowCatapultSpline = false;
	MOwner->GetChiRef()->ToggleAimPivot(false);
	MOwner->MokoshPlayerController->mChiIndicator->ResetIndicator();

	if (MOwner->GetState() == EPlayerStates::CatInit) {
		// reset chi catapult variables
		MOwner->GetChiRef()->CancelCatapult();
		MOwner->UpdateState(EPlayerStates::Neutral);
	}

	if (MOwner->GetState() == EPlayerStates::CatLoaded || MOwner->GetState() == EPlayerStates::CatLaunched) {
		//GEngine->AddOnScreenDebugMessage(-1, 0.001f, FColor::Yellow, TEXT("drawing"));
		MOwner->GetChiRef()->CancelCatapult();
		MOwner->GetChiRef()->RePossess();
		MOwner->GetCharacterMovement()->SetMovementMode(MOVE_Walking);
		
		MOwner->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, false));
		FRotator CurrentRotation = MOwner->GetActorRotation();
		MOwner->SetActorRotation(FRotator(0, CurrentRotation.Yaw, 0));
		Controller->Possess(MOwner);
		
		MOwner->UpdateState(EPlayerStates::Neutral);
		MOwner->ChiCam = false;
		SwitchCamera(false);

		
		MOwner->GetMesh()->SetPhysicsLinearVelocity(FVector(0, 0, 0));

		CatForce = 0;
	}
}

void UCatapultComponent::LerpCamera()
{
	float fraction = (UGameplayStatics::GetRealTimeSeconds(GetWorld()) - CamStartTime) / CamLoadTime;
	FRotator lerpLoc = FMath::Lerp(CamStartPos, CamEndPos, fraction);
	Controller->SetControlRotation(lerpLoc);

	if (fraction >= 1.0f)
	{
		bChangeCam = false;
		Controller->SetControlRotation(CamEndPos);
	}
}
