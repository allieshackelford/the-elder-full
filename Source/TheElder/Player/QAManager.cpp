// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "QAManager.h"
#include "Player/HttpService.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Player/Mokosh.h"
#include "AI/ChiAI/Chi.h"

// Sets default values
AQAManager::AQAManager()
{
	// whole level 
	registeredAreas.Add(FQAArea("Whole Level", FVector(7742.4418945, -11238.0820312, 1392.0001221), FVector(20095.3398438, -3175.6184082, 4552.0)));
	// forest
	registeredAreas.Add(FQAArea("Forest", FVector(20095.3398438, -3175.6184082, 4552.0), FVector(16859.6035156, 421.5181885, 2152.1555176)));
	// mushroom
	registeredAreas.Add(FQAArea("Mushroom", FVector(16859.6035156, 421.5181885, 2152.1555176), FVector(21032.5410156, 4057.7756348, 1772.0)));

	// TODO: add final boss area later

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AQAManager::BeginPlay()
{
	Super::BeginPlay();
	
	checkForStart = false;
	startTimer = false;
	PlayerRef = Cast<AMokosh>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AChi::StaticClass(), FoundActors);
	// error checker
	if (FoundActors.Num() > 0) {
		ChiRef = Cast<AChi>(FoundActors[0]);
	}
}

// Called every frame
void AQAManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (checkForStart) {
		StartSession();
	}
	else {
		// EndSession
		EndSession();
	}

	// start timer set in startsession
	if (startTimer) {
		// add the delta time to the session timer
		sessionTimer += DeltaTime;
	}

}

void AQAManager::CreateSession(bool firstTime, FString option) {
	HttpActor = GetWorld()->SpawnActor<AHttpService>(FVector(0, 0, 0), FRotator(0, 0, 0));

	if (HttpActor != nullptr) {
		FRequest_QASession session;
		session.cmd = "savesession";
		session.sessionid = FDateTime::Now().ToString();
	//  ==== save sessionid for sending data ======
		currSession = FString(session.sessionid);

		for (int i = 0; i < registeredAreas.Num(); i++) {
			if (registeredAreas[i].name == option) {
				currentArea = i;
			}
		}
	//  ===========================================
		session.area = option;
		session.firstTime = firstTime;

		HttpActor->SendQASession(session);
	}

	// set timer to 0
	sessionTimer = 0;
	checkForStart = true;
}

// ==================== SESSION START =====================
void AQAManager::StartSession() {
	FVector location = PlayerRef->GetActorLocation();

	float distance = FVector::Distance(location, registeredAreas[currentArea].startPos);

	if (distance < 20) {
		startTimer = true;
		checkForStart = false;

		// call send data functions 
		SendPlayerPos();
	}
}

// ==================== SESSION PROGRESS ( called with timer ) =====================

void AQAManager::SendPlayerPos() {
	FRequest_PlayerPos position;
	FVector playerpos = PlayerRef->GetActorLocation();
	position.cmd = "saveplayerpos";
	position.playSession = currSession;
	position.posX = playerpos.X;
	position.posY = playerpos.Y;

	HttpActor->SendPosition(position);


	GetWorld()->GetTimerManager().SetTimer(FT_PlayerPos, this, &AQAManager::SendPlayerPos, 1.0f);
}

void AQAManager::SendChiPos() {
	
}

void AQAManager::SendChiState() {
	FRequest_ChiState chiState;
	chiState.cmd = "savechistate";
	chiState.playSession = currSession;
	//chiState.state = ChiRef->
	// get chi state from controller somehow
}


// ==================== SESSION END =====================
void AQAManager::EndSession() {
	FVector location = PlayerRef->GetActorLocation();

	float distance = FVector::Distance(location, registeredAreas[currentArea].endPos);

	if (distance < 20) {
		startTimer = false;
		// TODO: Send ending time
	}
}

