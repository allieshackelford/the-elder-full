// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "ElderAudioComponent.h"


// Sets default values
AElderAudioComponent::AElderAudioComponent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AElderAudioComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AElderAudioComponent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

