// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Camera/ElderSpringArmComponent.h"
#include "CatapultComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEELDER_API UCatapultComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCatapultComponent();


	// ======== Catapult Force Helpers =====================
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Catapult")
		int MinForce;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Catapult")
		int MaxForce;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Catapult")
		float ZChangeMin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Catapult")
		float ZChangeMax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsMenuThrow;
	
protected:
	class AMokosh* MOwner;
	class APlayerCameraManager* CamManager;
	class APlayerController* Controller;


	// ========== CATAPULT VARIABLES ===========
	FVector StartPos;
	FVector CurvePos;
	FVector EndPos;
	float StartTime;
	float LoadTime = .35f;
	FVector PrevDirection;
	FRotator CurrentRotation;

	// ========= FORCE VARIABLES ===============
	float CatForce;
	float ZChange;
	float SplineDistance;

	// ======== CALCULATE PATH ================
	FVector StartPath;
	FVector CurvePath;
	FVector EndPath;

	class UParticleSystem* CatapultParticle;

	// ======= CATAPULT VARIABLES ===========
	FVector LastAimPos;
	class UParticleSystemComponent* LastParticle;

	// ====== CAMERA LERP ======
	FRotator CamStartPos;
	FRotator CamEndPos;
	float CamStartTime;
	float CamLoadTime = .8f;
	
	bool bChangeCam;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	float Remap(float val, FVector2D xy1, FVector2D xy2);
	
public:	
	// ======= CHECK SYSTEM ==========
	bool CheckIfCanActivate();

	UFUNCTION(BlueprintCallable)
	void InitCatapult();
	void CheckForCatReady();
	void SetCatapultLoad();
	void LoadingIntoChi();
	void LaunchPlayer();
	void ShowCatapultPath();
	void CalculateCatValues();
	void ToggleChi();
	void CheckForGround();
	void SwitchCamera(bool ToChi);
	void CancelCatapult();


	void LerpCamera();
};
