// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "SapThrowComponent.h"
#include "AI/ChiAI/Chi.h"
#include "Mokosh.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Mechanics/Sap.h"
#include "Camera/ElderSpringArmComponent.h"
#include "Engine/Engine.h"
#include "Components/StaticMeshComponent.h"
#include "Animations/ChiAnimInstance.h"
#include "Player/ElderPlayerController.h"
#include "UI/ChiIndicator.h"
#include "Camera/CameraComponent.h"
#include "AI/ChiAI/ChiAIController.h"

USapThrowComponent::USapThrowComponent()
{
	// register aim particle 
	InputHoldTime = 0;

	PrimaryComponentTick.bCanEverTick = true;
}


void USapThrowComponent::BeginPlay()
{
	Super::BeginPlay();

	// cache references to player, camManager, playercontroller
	MOwner = Cast<AMokosh>(GetOwner());
	PlayerControllerRef = Cast<AElderPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(),0));
	if(PlayerControllerRef == nullptr)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("it's null :("));
	}
}


// Called every frame
void USapThrowComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// show raycast of aim direction on screen
	if (MOwner->GetCameraType() == ECameraTypes::SapAim) {
		Aiming();

		// timer for rapid firing sap when shoot input is held down
		if (StartInputTimer) InputHoldTime++;
		if(InputHoldTime > 20.0f && InputHoldTime % RapidFireCoolDown == 0) SapThrow();

	}
}

// rules to check if sap aiming can be activated
bool USapThrowComponent::CheckIfCanActivate() {
	// what is mokosh's state? 
	if (MOwner->GetState() != EPlayerStates::Neutral && 
		MOwner->GetState() != EPlayerStates::CatLaunched &&
		MOwner->GetState() != EPlayerStates::Bouncing) {
		//GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Yellow, TEXT("Mokosh we can't sap right now!"));
		return false;
	}

	// where is chi? is chi in staffmode?
	if (MOwner->staffMode == false) {
		if(MOwner->CheckChiDistance() > 450)
		{
			// teleport chi to be behind the player at a smaller distance
			if (MOwner->TeleportChi() == false) {
				//GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Yellow, TEXT("There's no space for me there"));
			}
			
		}
	}

	// should mokosh face the camera?
	if (MOwner->GetState() == EPlayerStates::Neutral) {
		// rotate mokosh to face camera direction
		MOwner->RotateToFaceCamera();
	}
	
	return true;
}

void USapThrowComponent::EnterSapAim() {
	// STATE CHANGE CHECKS
	if (CheckIfCanActivate() == false) {
		if (MOwner->IsStaffMode() == false) MOwner->MokoshPlayerController->mChiIndicator->ToggleChiIndicator(false);
		return;
	}

	// show reticle
	if (PlayerControllerRef != nullptr) {
		PlayerControllerRef->ToggleReticleOn();
	}

	// AUDIO WHISTLE_SHOOT
	MOwner->PlayWhistle(WhistleType::Shoot);
	
	// set camera and animation for aiming
	MOwner->IsAiming = true;
	MOwner->bUseControllerRotationYaw = true;
	MOwner->GetChiRef()->ChangeState(ChiStates::Sap);

	MOwner->MokoshPlayerController->mChiIndicator->ResetIndicator();
	
	// null checks for chi and chi anim instance
	if(MOwner->GetChiRef() != nullptr && MOwner->GetChiRef()->AnimInstance != nullptr) MOwner->GetChiRef()->AnimInstance->bIsSapping = true;
	
	MOwner->UpdateState(EPlayerStates::SapAiming);
	// change to sap camera view
	MOwner->CameraBoom->ChangeCamera(ECameraTypes::SapAim);
	GetWorld()->GetTimerManager().SetTimer(SapTimer, this, &USapThrowComponent::ToggleSap, 0.2f);
}

// continually called while in aim mode to reposition aim particle
void USapThrowComponent::Aiming() {
	FVector Start = MOwner->CamManager->GetCameraLocation();
	// end is position far out from the camera
	FVector End = MOwner->CamManager->GetCameraLocation() + (UKismetMathLibrary::GetForwardVector(MOwner->CamManager->GetCameraRotation()) * 10000.0f);

	FHitResult hit;
	FCollisionQueryParams params(TEXT("SapAimTrace"), true);

	FVector LookPoint;


	// if hit surface, move sap aim particle 
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params)) {
		AChi* isChi = Cast<AChi>(hit.Actor);
		if (isChi != nullptr) return;
		if (hit.Actor != nullptr) {
			LookPoint = hit.Location;
			// rotate chi to face aim direction
			MOwner->GetChiRef()->AimRotation(LookPoint);
			// if the redicle hits sap, don't change
			ASap* IsSap = Cast<ASap>(hit.Actor);
			if (IsSap != nullptr) return;
			// change reticle if hitting / not hitting
			ASappableSurface* CheckHit = Cast<ASappableSurface>(hit.Actor);
			if(CheckHit != nullptr)
			{
				PlayerControllerRef->ToggleReticleHittable(true);
				return;
			}
		}
	}

	PlayerControllerRef->ToggleReticleHittable(false);
}

// change back to original cam
void USapThrowComponent::ExitSapAim() {
	if (MOwner->GetCameraType() != ECameraTypes::SapAim) return;

	MOwner->GetChiRef()->AnimInstance->bIsSapping = false;

	// hide reticle
	if (PlayerControllerRef != nullptr) {
		PlayerControllerRef->ToggleReticleOff();
	}

	// reset animation and camera
	MOwner->IsAiming = false;
	MOwner->bUseControllerRotationYaw = false;
	MOwner->GetChiRef()->ChangeState(ChiStates::Neutral);

	if (MOwner->IsStaffMode() == false) MOwner->MokoshPlayerController->mChiIndicator->ResetIndicator();
	
	MOwner->UpdateState(EPlayerStates::Neutral);
	// set camera back to neutral position 
	MOwner->CameraBoom->ChangeCamera(ECameraTypes::Normal);
	GetWorld()->GetTimerManager().SetTimer(SapTimer, this, &USapThrowComponent::ToggleSap, 0.3f);
	
}

// called when shoot input is pressed
void USapThrowComponent::SapThrow() {
	if (MOwner->GetCameraType() != ECameraTypes::SapAim) return;
	
	// input hold time to trigger rapid fire
	StartInputTimer = true;

	// start is camera location
	FVector Start = MOwner->CamManager->GetCameraLocation();
	// get point straight out from camera
	FVector End = MOwner->CamManager->GetCameraLocation() + (UKismetMathLibrary::GetForwardVector(MOwner->CamManager->GetCameraRotation()) * 10000.0f);

	FHitResult hit;
	FCollisionQueryParams params(TEXT("SapThrowTrace"), true, MOwner);
	params.AddIgnoredActor(MOwner->GetChiRef());

	FVector LookPoint;
	FVector direction;

	// if hit, find direction from chi to look point and set sap rotation 
	if (GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECC_WorldStatic, params)) {
		if (hit.Actor != nullptr) {
			LookPoint = hit.Location;
			// if chi in staff mode, fire sap from staff position
			MOwner->GetChiRef()->AnimInstance->bThrowSap = true;
			GetWorld()->GetTimerManager().SetTimer(AnimationTimer, this, &USapThrowComponent::ResetThrowAnimation, 0.2f);

			
			if (MOwner->staffMode) {
				direction = LookPoint - MOwner->StaffCrystalSpawnPoint->GetComponentLocation();
				GetWorld()->SpawnActor<ASap>(BPSap, MOwner->StaffCrystalSpawnPoint->GetComponentLocation(), direction.Rotation());
			}
			else {
				
				direction = LookPoint - MOwner->GetChiRef()->GetMesh()->GetSocketLocation("LoadSocket");
				GetWorld()->SpawnActor<ASap>(BPSap, MOwner->GetChiRef()->GetMesh()->GetSocketLocation("LoadSocket"), direction.Rotation());
			}
		}
	}
}

// resets rapid fire timer
void USapThrowComponent::SapThrowInputReleased()
{
	InputHoldTime = 0;
	StartInputTimer = false;
}

// toggle aim particle visibility and set camera pitch values
void USapThrowComponent::ToggleSap() {
	
	if (MOwner->GetState() == EPlayerStates::SapAiming) {
		MOwner->CamManager->ViewPitchMax = 70;
		MOwner->CamManager->ViewPitchMin = -89;
	}

	if (MOwner->GetState() == EPlayerStates::Neutral) {
		// original pitch values
		MOwner->CamManager->ViewPitchMax = 43;
		MOwner->CamManager->ViewPitchMin = -75;
	}
}

void USapThrowComponent::ResetThrowAnimation()
{
	MOwner->GetChiRef()->AnimInstance->bThrowSap = false;
}

// check if aim particle gets triggered on position behind player
bool USapThrowComponent::TargetBehindPlayer(FVector HitLocation)
{
	FVector PlayerForward = MOwner->GetActorForwardVector();
	float DotProd = FVector::DotProduct(PlayerForward, HitLocation);
	float Angle = FMath::RadiansToDegrees(acosf(DotProd));

	// if behind player, don't move particle
	if(Angle > 180)
	{
		return true;
	}
	
	return false;
}
