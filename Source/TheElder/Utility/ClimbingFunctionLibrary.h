// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ClimbingFunctionLibrary.generated.h"

UCLASS()
class THEELDER_API UClimbingFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UClimbingFunctionLibrary();

public:
	// We have to use this crappy way instead of getting it directly from the blueprint because of when the Saps check for collision
	static float GetPlayerCapsuleHalfHeight()
	{
		return 83.8684845f;
	}
	static float GetPlayerCapsuleRadius()
	{
		return 19.7544079f;
	}
	
	static float GetClimbingDetectionRadius()
	{
		return 200.0f;
	}

	static bool IsPlayerCapsuleObstructed(AActor* Climbable, UObject* WorldContextObject);
	static FTransform GetHangingTransform(const AActor* NewClimbable, UObject* WorldContextObject);
private:

	static FVector GetDistanceFromClimbTarget()
	{
		return FVector(-80, 100, 30);
	}


	static bool IsDebugging()
	{
		return false;
	}

};
