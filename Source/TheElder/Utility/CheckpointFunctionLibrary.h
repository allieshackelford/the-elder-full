// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "World/ElderCheckpoint.h"
#include "CheckpointFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UCheckpointFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "Checkpoints")
	static int GetNumberOfCheckpoints(AActor* Instigator);
};
