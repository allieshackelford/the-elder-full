// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#include "ClimbingFunctionLibrary.h"
#include "MasterIncludes.h"
#include "World/SplineLedge.h"
#include "SapBase.h"

UClimbingFunctionLibrary::UClimbingFunctionLibrary()
{
}

bool UClimbingFunctionLibrary::IsPlayerCapsuleObstructed(AActor* Climbable, UObject* WorldContextObject)
{
	// TODO: Gotta detect ledges as well
	auto World = GEngine->GetWorldFromContextObjectChecked(WorldContextObject);

	TArray<FOverlapResult> Overlaps;
	TArray<TEnumAsByte<EObjectTypeQuery>> Params;
	Params.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));
	Params.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic));

	FCollisionObjectQueryParams ObjectParams = { Params };
	auto HangingTransform = GetHangingTransform(Climbable, WorldContextObject);
	World->OverlapMultiByObjectType(Overlaps, HangingTransform.GetLocation(), HangingTransform.GetRotation(), ObjectParams,
		FCollisionShape::MakeCapsule(GetPlayerCapsuleRadius(), GetPlayerCapsuleHalfHeight()));

	for (const auto Result : Overlaps)
	{
		if (Cast<ASapBase>(Result.Component->GetOwner()) != nullptr)
			continue;
		// Let's see if the actor if set to block Mokosh
		if (Result.Component->GetCollisionResponseToChannel(ECC_MokoshChannel) == ECollisionResponse::ECR_Block)
		{
			if (IsDebugging())
			{
				LOG(FString::Printf(TEXT("%s is in the way."), *Result.Component->GetName()), 20);
			}

			return true;
		}
	}
	return false;
}

FTransform UClimbingFunctionLibrary::GetHangingTransform(const AActor* NewClimbable, UObject* WorldContextObject)
{
	auto World = GEngine->GetWorldFromContextObjectChecked(WorldContextObject);

	if (NewClimbable == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Tried to GetHangingPosition while NewClimbable was nullptr"))
			return FTransform{};
	}
	auto BaseLoc = NewClimbable->GetActorLocation();
	auto Ledge = Cast<ASplineLedge>(NewClimbable);
	if (Ledge != nullptr)
	{
		LOG_ERR("Tried to GetHangingTransform from Ledge in ClimbingFunctionLibrary, currently unsupported", 50)
		return FTransform();
	}
	auto HangingVerticalLocalPosition = NewClimbable->GetActorUpVector() * (GetDistanceFromClimbTarget().Z + GetPlayerCapsuleHalfHeight());
	auto HorizontalDirection = NewClimbable->GetActorForwardVector() * GetDistanceFromClimbTarget().X;
	/*if (Ledge != nullptr)
	{
		HorizontalDirection = Ledge->GetClimbUpTransform(GetOwner()).Rotator().Vector() * -DistanceFromClimbTarget.X;
	}*/
	FTransform Result;
	Result.SetLocation((BaseLoc - HangingVerticalLocalPosition) + HorizontalDirection);
	if (Ledge == nullptr)
	{
		Result.SetRotation(NewClimbable->GetActorForwardVector().Rotation().Quaternion());
	}
	/*else
	{
		if (bIsDebugging)
		{
			UKismetSystemLibrary::DrawDebugArrow(World, BaseLoc,
				BaseLoc + ((Ledge->GetClimbUpTransform(GetOwner()).Rotator().Vector() *
					-1.0f).Rotation().Quaternion().Vector() * 200),
				10, FLinearColor::Green, 10.0f);
		}
		Result.SetRotation((Ledge->GetClimbUpTransform(GetOwner()).Rotator().Vector() * -1.0f).Rotation().Quaternion());
	}*/
	return Result;
}