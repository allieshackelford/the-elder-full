// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ObjectMacros.h"

#define ECC_ChiChannel ECollisionChannel::ECC_GameTraceChannel1
#define ECC_MokoshChannel ECollisionChannel::ECC_GameTraceChannel2

class THEELDER_API Utility
{
public:
	Utility();
	~Utility();
};

template<typename TEnum>
static FORCEINLINE FString GetEnumValueAsString(const FString& Name, TEnum Value)
{
	const UEnum* enumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true);
	if (!enumPtr)
	{
		return FString("Invalid");
	}
	return enumPtr->GetNameByValue(static_cast<int64>(Value)).ToString();
}

