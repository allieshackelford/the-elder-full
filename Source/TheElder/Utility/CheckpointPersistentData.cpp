// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "CheckpointPersistentData.h"
#include "World/ElderCheckpoint.h"

bool operator==(const FCheckpointInformation& Left, const FCheckpointInformation& Right) 
{
	return Left.CheckpointName == Right.CheckpointName;
	
}

void UCheckpointPersistentData::MoveCheckpoint(FString LevelName, int CheckpointIndex,
                                               ECheckpointMovementDirectionEnum MovementDirection,
                                               UObject* WorldContextObject)
{
	auto LevelCheckpoints = Checkpoints.Find(LevelName);

	if (MovementDirection == ECheckpointMovementDirectionEnum::CMD_MoveDown)
	{
		if (CheckpointIndex == 0)
			return;

		LevelCheckpoints->Array.Swap(CheckpointIndex, CheckpointIndex - 1);

		ArrayChanged.Broadcast(LevelCheckpoints->Array[CheckpointIndex].CheckpointName, CheckpointIndex);
		// At some point we should update the GUI seperately, but for now we don't
		//ArrayChanged.Broadcast(LevelCheckpoints->Array[CheckpointIndex-1].CheckpointName, CheckpointIndex);
	}
		// Moving Up
	else
	{
		if (LevelCheckpoints->Array.Num() - 1 <= CheckpointIndex)
			return;
		LevelCheckpoints->Array.Swap(CheckpointIndex, CheckpointIndex + 1);

		ArrayChanged.Broadcast(LevelCheckpoints->Array[CheckpointIndex].CheckpointName, CheckpointIndex);
		// At some point we should update the GUI seperately, but for now we don't
		//ArrayChanged.Broadcast(LevelCheckpoints->Array[CheckpointIndex+1].CheckpointName, CheckpointIndex);
	}
	
	// Need to update the TextRender for all the other checkpoints here.
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, AElderCheckpoint::StaticClass(), OutActors);
	for (auto Actor : OutActors)
	{
		auto Checkpoint = Cast<AElderCheckpoint>(Actor);
		int NewIndex = FindCheckpoint(LevelName, Checkpoint->GetName());
		Checkpoint->OrderChanged(NewIndex);
	}
	
	SaveAsset();
}
