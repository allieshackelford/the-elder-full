// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TheElderGameMode.h"
#include "ElderFunctionLibrary.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FElderSpecialTimerDelegate, UPrimitiveComponent*, TheComp);

UCLASS()
class THEELDER_API UElderFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	/** Returns the current TheElderGameMode or nullptr if it can't be retrieved */
	UFUNCTION(BlueprintPure, Category = "Game", meta = (WorldContext = "WorldContextObject"))
	static ATheElderGameMode* GetElderGameMode(const UObject* WorldContextObject);
	/* Returns if this was built in the editor, or is a package*/
	UFUNCTION(BlueprintPure, Category = "Game")
	bool IsEditorBuild();


	UFUNCTION(BlueprintPure, Category = "UI")
	static FSlateColor GetRegularTextColor();
	UFUNCTION(BlueprintPure, Category = "UI")
	static FSlateColor GetPressedTextColor();
	UFUNCTION(BlueprintPure, Category = "UI")
	static FSlateColor GetHoveredTextColor();
	UFUNCTION(BlueprintPure, Category = "UI")
	static FSlateColor GetFocusedTextColor();

	UFUNCTION(BlueprintCallable, Category = "Game")
	static FTimerHandle FallingRocksTimerDelegate(const UObject* Base, FTimerDynamicDelegate Delegate, float Time);
};
