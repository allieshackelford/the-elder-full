// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "PackageName.h"
#include "Package.h"
//#include "MasterIncludes.h"
#include "Kismet/GameplayStatics.h"
#include "CheckpointPersistentData.generated.h"

/*Function signature: */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnArrayChangedDelegate, FString, CheckpointName, int, CheckpointIndex);

UENUM(BlueprintType)
enum class ECheckpointMovementDirectionEnum : uint8
{
	CMD_MoveUp		UMETA(DisplayName = "Move Up"),
	CMD_MoveDown	UMETA(DisplayName = "Move Down")
};

USTRUCT(BlueprintType)
struct FCheckpointInformation
{
	GENERATED_BODY()
	/* The object's name*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Checkpoint Name"))
	FString CheckpointName;
	/* The array of sublevels to load at this checkpoint, in no particular order*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "Sublevels to load"))
	TArray<FString> SublevelsToLoad;
	
	THEELDER_API friend bool operator==(const FCheckpointInformation& Left, const FCheckpointInformation& Right);
};

USTRUCT(BlueprintType)
struct FCheckpoints
{
	GENERATED_BODY()
		
public:
	/* Just a list of checkpoints*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (DisplayName = "Checkpoint Information"))
	TArray<FCheckpointInformation> Array;

	FCheckpoints()
	{
		Array = TArray<FCheckpointInformation>();
	}
	
};

/**
 * 
 */
UCLASS()
class THEELDER_API UCheckpointPersistentData : public UPrimaryDataAsset
{
	GENERATED_BODY()
public:
	
	/* The TMap values are NameOfTheMap, checkpointsWithinThatMap*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FString, FCheckpoints> Checkpoints;
	/* The SubLevels that always get loaded, used more for collaboration than streaming optimization*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> PersistentSublevels;

	FCheckpoints NewCheckpointCategory(FString MapName)
	{
		return Checkpoints.Add(MapName, FCheckpoints());
	}

	FCheckpoints* FindCheckpoint(FString LevelName)
	{
		return Checkpoints.Find(LevelName);
	}

	/* The current checkpoint that we're going to load when we load the map, this is overriden if #WITH_EDITOR is not true*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrentEditorCheckpoint = 0;
	
	/* If we should show the main menu, this is overriden if #WITH_EDITOR is not true*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bShowMainMenu = false;

	// Access to the lower levels of the checkpoints

	UFUNCTION(BlueprintCallable)
	TArray<FCheckpointInformation> GetArray(FString LevelName) const
	{
		auto Result = Checkpoints.Find(LevelName);
		if (Result == nullptr)
			return TArray<FCheckpointInformation>();
		return Result->Array;
	}
	UFUNCTION(BlueprintCallable)
	FCheckpointInformation GetCheckpointInformation(FString LevelName, int Index)
	{
		auto LevelCheckpoints = Checkpoints.Find(LevelName);
		if (LevelCheckpoints == nullptr)
			return FCheckpointInformation();
		if (LevelCheckpoints->Array.Num() < Index)
			return FCheckpointInformation();
		return LevelCheckpoints->Array[Index];
	}
	UFUNCTION(BlueprintCallable)
	void SetCheckpointInformation(FString LevelName, int Index, FCheckpointInformation CheckpointInformation)
	{
		auto Checkpoint = Checkpoints.Find(LevelName);
		Checkpoint->Array[Index] = CheckpointInformation;
		ArrayChanged.Broadcast(Checkpoint->Array[Index].CheckpointName, Index);

		SaveAsset();
	}
	UFUNCTION(BlueprintCallable)
	int AddCheckpointInformation(FString LevelName, FCheckpointInformation CheckpointInformation)
	{
		auto Checkpoint = Checkpoints.Find(LevelName);
		auto Index = Checkpoint->Array.Add(CheckpointInformation);
		ArrayChanged.Broadcast(CheckpointInformation.CheckpointName, Index);

		SaveAsset();
		
		return Index;
	}
	UFUNCTION(BlueprintCallable)
	int RemoveCheckpoint(FString LevelName, FString CheckpointName)
	{
		auto Checkpoint = Checkpoints.Find(LevelName);
		
		auto DummyCheckpoint = FCheckpointInformation();
		DummyCheckpoint.CheckpointName = CheckpointName;

		auto Index = Checkpoint->Array.Find(DummyCheckpoint);
		if (Index == -1)
		{
			return -1;
		}
		Checkpoint->Array.RemoveAt(Index);
		ArrayChanged.Broadcast(CheckpointName, Index);
		
		SaveAsset();

		return Index;
	}
	UFUNCTION(BlueprintCallable)
	int FindCheckpoint(FString LevelName, FString CheckpointName)
	{
		auto LevelCheckpoints = Checkpoints.Find(LevelName);
		
		auto DummyCheckpoint = FCheckpointInformation();
		DummyCheckpoint.CheckpointName = CheckpointName;

		return LevelCheckpoints->Array.Find(DummyCheckpoint);
	}
	UFUNCTION(BlueprintCallable)
	void AddSubLevel(FString LevelName, FString CheckpointName, FString SubLevelName)
	{
		auto LevelCheckpoints = Checkpoints.Find(LevelName);
		
		auto DummyCheckpoint = FCheckpointInformation();
		DummyCheckpoint.CheckpointName = CheckpointName;

		auto Index = LevelCheckpoints->Array.Find(DummyCheckpoint);
		if (Index == -1)
			return;
		LevelCheckpoints->Array[Index].SublevelsToLoad.Add(SubLevelName);
		SaveAsset();
	}
	UFUNCTION(BlueprintCallable)
	void RemoveSubLevel(FString LevelName, FString CheckpointName)
	{
		auto LevelCheckpoints = Checkpoints.Find(LevelName);
		
		auto DummyCheckpoint = FCheckpointInformation();
		DummyCheckpoint.CheckpointName = CheckpointName;

		auto Index = LevelCheckpoints->Array.Find(DummyCheckpoint);
		LevelCheckpoints->Array[Index].SublevelsToLoad.RemoveAt(LevelCheckpoints->Array[Index].SublevelsToLoad.Num() - 1);
		SaveAsset();
	}
	UFUNCTION(BlueprintCallable)
	void UpdateSublevel(FString LevelName, FString CheckpointName, int SubLevelIndex, FString NewName)
	{
		auto LevelCheckpoints = Checkpoints.Find(LevelName);
		
		auto DummyCheckpoint = FCheckpointInformation();
		DummyCheckpoint.CheckpointName = CheckpointName;

		auto Index = LevelCheckpoints->Array.Find(DummyCheckpoint);
		if (SubLevelIndex >= LevelCheckpoints->Array[Index].SublevelsToLoad.Num())
			return;

		LevelCheckpoints->Array[Index].SublevelsToLoad[SubLevelIndex] = NewName;
		SaveAsset();
	}
	
	UFUNCTION(BlueprintCallable)
	void MoveCheckpoint(FString LevelName, int CheckpointIndex, ECheckpointMovementDirectionEnum MovementDirection,
	                    UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable)
	void SaveAsset()
	{
		MarkPackageDirty();
	}
	/* Also need to check if CheckpointName is exists any more*/
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Checkpoints")
	FOnArrayChangedDelegate ArrayChanged;
};
