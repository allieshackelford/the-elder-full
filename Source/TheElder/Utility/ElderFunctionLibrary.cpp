// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "ElderFunctionLibrary.h"
#include "MasterIncludes.h"
#include "TheElderGameMode.h"

ATheElderGameMode* UElderFunctionLibrary::GetElderGameMode(const UObject* WorldContextObject)
{
	UWorld* const World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	if (World == nullptr)
		return nullptr;
	return Cast<ATheElderGameMode>(World->GetAuthGameMode());
}

bool UElderFunctionLibrary::IsEditorBuild()
{
#if WITH_EDITOR
	return true;
#else
	return false;
#endif
}


FSlateColor UElderFunctionLibrary::GetRegularTextColor()
{
	return FSlateColor(FLinearColor(1.0f, 1.0f, 1.0f));
}

FSlateColor UElderFunctionLibrary::GetPressedTextColor()
{
	return FSlateColor(FLinearColor(0.005208f, 0.005208f, 0.005208f));
}

FSlateColor UElderFunctionLibrary::GetHoveredTextColor()
{
	return FSlateColor(FLinearColor(0.244792f, 0.244792f, 0.244792f));
}

FSlateColor UElderFunctionLibrary::GetFocusedTextColor()
{
	return FSlateColor(FLinearColor(0.00388f, 0.052083f, 0.0f));
}

FTimerHandle UElderFunctionLibrary::FallingRocksTimerDelegate(const UObject* Base, FTimerDynamicDelegate Delegate, float Time)
{
	FTimerHandle Handle;
	const UWorld* World = GEngine->GetWorldFromContextObject(Base, EGetWorldErrorMode::LogAndReturnNull);

	FTimerManager& TimerManager = World->GetTimerManager();
	
	TimerManager.SetTimer(Handle, Delegate, Time, false);

	return Handle;
}