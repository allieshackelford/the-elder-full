// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "CheckpointFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "Systems/ElderSaveGame.h"
#include "Systems/TheElderGameInstance.h"

int UCheckpointFunctionLibrary::GetNumberOfCheckpoints(AActor* Instigator)
{
	auto Instance = Cast<UTheElderGameInstance>(Instigator->GetWorld()->GetGameInstance());
	if (Instance == nullptr || Instance->CheckpointData == nullptr)
		return 0;
	auto World = Instigator->GetWorld();
	if (World == nullptr)
		return 0;
	auto LevelName = UGameplayStatics::GetCurrentLevelName(Instigator);
	
	auto  Checkpoints = Instance->CheckpointData->Checkpoints.Find(LevelName);
	if (Checkpoints == nullptr)
		return 0;
	return Checkpoints->Array.Num();
}
