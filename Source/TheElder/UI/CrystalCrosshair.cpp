// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "CrystalCrosshair.h"
#include "Components/Image.h"

UCrystalCrosshair::UCrystalCrosshair(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

void UCrystalCrosshair::NativeConstruct()
{
}

void UCrystalCrosshair::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	// raycast check for crystalable wall and change reticle texture
}

void UCrystalCrosshair::ToggleReticle(bool IsHittable)
{
	if (IsHittable)
	{
		CrosshairImage->SetBrushFromTexture(CrosshairTexPositive);
		LeftImage->SetBrushFromTexture(LeftTexPositive);
		RightImage->SetBrushFromTexture(RightTexPositive);
	}

	else
	{
		CrosshairImage->SetBrushFromTexture(CrosshairTexNegative);
		LeftImage->SetBrushFromTexture(LeftTexNegative);
		RightImage->SetBrushFromTexture(RightTexNegative);
	}
}
