// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "ChiIndicator.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UChiIndicator : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
		class UImage* RingImage;

	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
		class UImage* StaffImage;

	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
		class UImage* NegativeChiImage;


	UPROPERTY()
		FTimerHandle ResetIndicatorTimer;

private:
	bool IsStaff;

public:
	virtual bool Initialize() override;
	
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable)
	void ToggleChiIndicator(bool IsPositive);

	void ToggleStaffIndicator();

	void ResetIndicator();

};
