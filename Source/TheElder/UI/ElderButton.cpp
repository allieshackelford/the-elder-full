// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "ElderButton.h"
#include "Engine/Engine.h"
#include "TimerManager.h"

void UElderButton::OnWidgetRebuilt()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
}

void UElderButton::Tick(float DeltaTime)
{
	if(HasKeyboardFocus() != CachedHasKeyboardFocus)
	{
		CachedHasKeyboardFocus = HasKeyboardFocus();
		if (HasKeyboardFocus())
		{
			OnHasFocus.Broadcast();
		}
		else
		{
			OnLostFocus.Broadcast();
		}
	}
}

bool UElderButton::IsTickable() const
{
	return true;
}

bool UElderButton::IsTickableInEditor() const
{
	return false;
}

bool UElderButton::IsTickableWhenPaused() const
{
	return false;
}

TStatId UElderButton::GetStatId() const
{
	return TStatId();
}

UWorld* UElderButton::GetWorld() const
{
	return GetOuter()->GetWorld();
}
