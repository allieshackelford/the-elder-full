// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Systems/ElderSaveGame.h"
#include "OptionsMenu.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UOptionsMenu : public UUserWidget
{
	GENERATED_BODY()
public:
	UOptionsMenu(const FObjectInitializer& ObjectInitializer);

	/*UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class USlider* GammaSlider;
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UComboBoxString* QualityComboBox;*/
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UElderButton* ReturnButton;

	UPROPERTY(BlueprintReadOnly)
	TArray<FString> GraphicsOptions = {"Low", "Medium", "High", "Epic", "Cinematic"};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UUserWidget* Creator;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float DefaultSliderValue = 0.36f;

	virtual void NativeConstruct() override;
	virtual void NativePreConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	UFUNCTION(BlueprintNativeEvent)
	void ReturnPressed();
	virtual void ReturnPressed_Implementation();

	UFUNCTION(BlueprintCallable)
	void GammaChanged(float Value);
	UFUNCTION(BlueprintCallable)
	void QualityChanged(FString NewSetting);

	UFUNCTION(BlueprintImplementableEvent)
	void LoadDataFromSave(float Gamma, const FString& GraphicsSetting, float MusicVolume, float SFXVolume);
	
	UFUNCTION(BlueprintCallable)
	void SaveData(float Gamma, const FString& GraphicsSetting, float MusicVolume, float SFXVolume);

	UFUNCTION(BlueprintImplementableEvent)
	float GetGammaValue();

	UFUNCTION(BlueprintImplementableEvent)
	FString GetGraphicsValue();
	
	UFUNCTION(BlueprintImplementableEvent)
	float GetMusicVolumeValue();
	
	UFUNCTION(BlueprintImplementableEvent)
	float GetSFXVolumeValue();
};
