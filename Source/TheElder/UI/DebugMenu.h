// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DebugMenu.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UDebugMenu : public UUserWidget
{
	GENERATED_BODY()
	
};
