// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "OptionsMenu.h"
#include "Components/Slider.h"
#include "Engine/Engine.h"
#include "Kismet/KismetMathLibrary.h"
#include "ComboBox.h"
#include "ComboBoxString.h"
#include "Public/Scalability.h"
#include "ElderButton.h"
#include "MainMenu.h"
#include "PauseMenu.h"
#include "Kismet/GameplayStatics.h"
#include "Systems/ElderSaveGame.h"
#include "Systems/TheElderGameInstance.h"
#include "MasterIncludes.h"

UOptionsMenu::UOptionsMenu(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UOptionsMenu::NativeConstruct()
{
	auto GameInstance = Cast<UTheElderGameInstance>(GetGameInstance());
	if (GameInstance == nullptr)
		return;
	
	ReturnButton->OnPressed.AddUniqueDynamic(this, &UOptionsMenu::ReturnPressed);
	//QualityComboBox->OnSelectionChanged.AddUniqueDynamic(this, &UOptionsMenu::QualityChanged);

	UElderSaveGame* LoadedGame = Cast<UElderSaveGame>(UGameplayStatics::LoadGameFromSlot(GameInstance->SettingsSaveSlot, 0));

	if (LoadedGame != nullptr)
	{
		LoadDataFromSave(LoadedGame->GammaValue, LoadedGame->GraphicsSetting, LoadedGame->MusicVolume, LoadedGame->SFXVolume);
	}
	else
	{
		// Yay magic numbers
		const float DefaultGammaValue = 0.36f;
		const float DefaultSoundVolume = 1.0f;
		LoadDataFromSave(0.36f, "Epic", DefaultSoundVolume, DefaultSoundVolume);
	}
	
	Super::NativeConstruct();
}

void UOptionsMenu::NativePreConstruct()
{
	//GammaSlider->OnValueChanged.AddUniqueDynamic(this, &UOptionsMenu::GammaChanged);
	//QualityComboBox->ClearOptions();
	for (FString Option : GraphicsOptions)
	{
		//QualityComboBox->AddOption(Option);
	}
	//QualityComboBox->SetSelectedOption(GraphicsOptions[3]);
	Super::NativePreConstruct();
}

void UOptionsMenu::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UOptionsMenu::ReturnPressed_Implementation()
{
	auto GameInstance = Cast<UTheElderGameInstance>(GetGameInstance());
	if (GameInstance == nullptr)
		return;
	
	UElderSaveGame* LoadedGame = Cast<UElderSaveGame>(UGameplayStatics::LoadGameFromSlot(GameInstance->SettingsSaveSlot, 0));

	// We need to create a new save
	if (LoadedGame == nullptr)
	{
		LoadedGame = Cast<UElderSaveGame>(UGameplayStatics::CreateSaveGameObject(UElderSaveGame::StaticClass()));
	}

	if (LoadedGame != nullptr)
	{
		// We have to save the slider value rather than the internal engine value because we're using a curve
		LoadedGame->GammaValue = GetGammaValue();
		LoadedGame->GraphicsSetting = GetGraphicsValue();
		LoadedGame->MusicVolume = GetMusicVolumeValue();
		LoadedGame->SFXVolume = GetSFXVolumeValue();
		//LoadedGame->GraphicsSetting = QualityComboBox->GetSelectedOption();
		GetGammaValue();
		UGameplayStatics::SaveGameToSlot(LoadedGame, GameInstance->SettingsSaveSlot, 0);
	}

	// Setting up return stuff
	if (Creator == nullptr)
	{
		LOG_ERR(TEXT("Somehow creator was nullptr"), 20)
		return;
	}
	
	auto MainMenu = Cast<UMainMenu>(Creator);
	auto PauseMenu = Cast<UPauseMenu>(Creator);
	if (MainMenu != nullptr)
	{
		MainMenu->ReturnFromOptions();
	}
	else if (PauseMenu != nullptr)
	{
		PauseMenu->ReturnFromOptions();
	}

}

void UOptionsMenu::GammaChanged(float Value)
{
	auto GameInstance = Cast<UTheElderGameInstance>(GetGameInstance());
	if (GameInstance == nullptr)
		return;
	
	if (GEngine == nullptr)
		return;
	// Default is 2.2
	// Unreal's built-in dialogue has a range from 1-3

	float NewGammaValue = Value;
	if (GameInstance->GammaCurve != nullptr)
	{
		NewGammaValue = GameInstance->GammaCurve->GetFloatValue(Value);
	}
	auto GammaValue = UKismetMathLibrary::MapRangeClamped(NewGammaValue, 0.0f, 1.0f, UTheElderGameInstance::GetGammaMin(), UTheElderGameInstance::GetGammaMax());
	GEngine->DisplayGamma = GammaValue;
}

//0:Low, 1 : Medium, 2 : High, 3 : Epic, 4 : Cinematic
void UOptionsMenu::QualityChanged(FString NewSetting)
{
	if (GEngine == nullptr)
		return;
	Scalability::FQualityLevels QualityLevel = Scalability::GetQualityLevels();
	
	auto OptionIndex = GraphicsOptions.Find(NewSetting);
	if (OptionIndex == INDEX_NONE)
	{
		GEngine->AddOnScreenDebugMessage(-1, 20, FColor::Red, 
			FString::Printf(TEXT("Unknown option %s, cannot find in GraphicsOption array."), *NewSetting));
	}
	QualityLevel.SetFromSingleQualityLevel(OptionIndex);
	Scalability::SetQualityLevels(QualityLevel, true);
}

void UOptionsMenu::SaveData(float Gamma, const FString& GraphicsSetting, float MusicVolume, float SFXVolume)
{
	
}
