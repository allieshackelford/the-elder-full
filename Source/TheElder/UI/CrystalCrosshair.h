// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CrystalCrosshair.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UCrystalCrosshair : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UImage* CrosshairImage;

	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UImage* LeftImage;
	
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UImage* RightImage;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reticle")
		class UTexture2D* LeftTexPositive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reticle")
		class UTexture2D* LeftTexNegative;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reticle")
		class UTexture2D* RightTexPositive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reticle")
		class UTexture2D* RightTexNegative;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reticle")
		class UTexture2D* CrosshairTexPositive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Reticle")
		class UTexture2D* CrosshairTexNegative;

public:
	UCrystalCrosshair(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;
	
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	void ToggleReticle(bool IsHittable);
};
