// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenu.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UMainMenu : public UUserWidget
{
	GENERATED_BODY()
public:
	UMainMenu(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UElderButton* PlayButton;
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UElderButton* QAButton;
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UElderButton* CreditsButton;
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UElderButton* OptionsButton;
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UElderButton* QuitButton;
	
	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	UFUNCTION(BlueprintNativeEvent)
	void ReturnFromOptions();

	virtual void ReturnFromOptions_Implementation();
};
