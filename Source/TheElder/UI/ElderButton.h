// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "Runtime/Engine/Public/Tickable.h"
#include "ElderButton.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UElderButton : public UButton, public FTickableGameObject
{
	GENERATED_BODY()
public:
	virtual void OnWidgetRebuilt() override;

	void Tick(float DeltaTime) override;
	bool IsTickable() const override;
	bool IsTickableInEditor() const override;
	bool IsTickableWhenPaused() const override;
	TStatId GetStatId() const override;

	UWorld* GetWorld() const override;

	/** Called when the button is pressed */
	UPROPERTY(BlueprintAssignable, Category = "Button|Event")
	FOnButtonPressedEvent OnHasFocus;
	UPROPERTY(BlueprintAssignable, Category = "Button|Event")
	FOnButtonPressedEvent OnLostFocus;
private:
	bool CachedHasKeyboardFocus = false;
};
