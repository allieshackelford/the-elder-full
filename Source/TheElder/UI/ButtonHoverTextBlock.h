// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/TextBlock.h"
#include "ButtonHoverTextBlock.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UButtonHoverTextBlock : public UTextBlock
{
	GENERATED_BODY()
public:
	virtual void OnWidgetRebuilt() override;
	UFUNCTION()
	void OnParentPressed();
	UFUNCTION()
	void OnParentHasFocus();
	UFUNCTION()
	void OnParentLostFocus();
	UFUNCTION()
	void OnParentUnhovered();
	UFUNCTION()
	void OnParentHovered();
	UFUNCTION()
	void OnParentReleased();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Custom Zion Stuff")
	FSlateColor PressedColour = FLinearColor::Blue;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Custom Zion Stuff")
	FSlateColor HoveredColour = FLinearColor::Red;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Custom Zion Stuff")
	FSlateColor RegularColour = FLinearColor::Green;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Custom Zion Stuff")
	FSlateColor FocusedColour = FLinearColor::Green;
};
