// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LoadingScreen.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API ULoadingScreen : public UUserWidget
{
	GENERATED_BODY()
public:
	/* This is called by the game mode when all the sublevels are loaded*/
	UFUNCTION(BlueprintImplementableEvent)
	void FadeOut();

	/* This is called by the game mode when the player dies and the screen fades to black*/
	UFUNCTION(BlueprintImplementableEvent)
	void DeathFadeIn(float SecondsToFade);

	/* This is called by the game mode when the death timer is up and it's time to play the game again*/
	UFUNCTION(BlueprintImplementableEvent)
	void DeathFadeOut(float SecondsToFade);
};
