// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "TutorialPrompt.h"
#include "Components/BoxComponent.h"
#include "UserWidget.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATutorialPrompt::ATutorialPrompt()
{
	// initialize input presses
	TimesInputPressed = 0;
	TimesToPressBeforeNotShowing = 0;
	bShouldShowPrompt = true;
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATutorialPrompt::BeginPlay()
{
	Super::BeginPlay();

	/*
	 * 
	if (wPromptToShow) {
		mPromptToShow = CreateWidget<UUserWidget>(UGameplayStatics::GetPlayerController(GetWorld(),0), wPromptToShow);
		if (mPromptToShow) {
			mPromptToShow->AddToViewport();
			mPromptToShow->SetVisibility(ESlateVisibility::Hidden);
		}
	}
	 */
}

// Called every frame
void ATutorialPrompt::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (TimesInputPressed >= TimesToPressBeforeNotShowing) {
		bShouldShowPrompt = false;
		//ShowPrompt(false);
	}
}

void ATutorialPrompt::IncrementInputCount()
{
	//ShowPrompt(false);
	TimesInputPressed++;
}

void ATutorialPrompt::ShowPrompt(bool ToShow)
{
	//if(ToShow) mPromptToShow->SetVisibility(ESlateVisibility::Visible);
	//else mPromptToShow->SetVisibility(ESlateVisibility::Hidden);
}

