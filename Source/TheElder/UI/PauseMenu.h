// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PauseMenu.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UPauseMenu : public UUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly, Category = "UI", meta = (BindWidget))
	class UElderButton* ReturnToGameButton;
	UPROPERTY(BlueprintReadOnly, Category = "UI", meta = (BindWidget))
	class UElderButton* DebugMenuButton;
	UPROPERTY(BlueprintReadOnly, Category = "UI", meta = (BindWidget))
	class UElderButton* OptionsButton;
	UPROPERTY(BlueprintReadOnly, Category = "UI", meta = (BindWidget))
	class UElderButton* MainMenuButton;

	UFUNCTION(BlueprintNativeEvent)
		void ReturnFromOptions();

	virtual void ReturnFromOptions_Implementation();
};
