// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "ButtonHoverTextBlock.h"
#include "Engine/Engine.h"
#include "Components/Widget.h"
#include "PanelWidget.h"
#include "Button.h"
#include "ElderButton.h"
#include "MasterIncludes.h"

void UButtonHoverTextBlock::OnWidgetRebuilt()
{
	auto Button = Cast<UElderButton>(GetParent());
	if (Button == nullptr)
	{
		Button = Cast<UElderButton>(GetParent()->GetParent());
	}
	if (Button != nullptr)
	{
		Button->OnPressed.AddUniqueDynamic(this, &UButtonHoverTextBlock::OnParentPressed);
		Button->OnUnhovered.AddUniqueDynamic(this, &UButtonHoverTextBlock::OnParentUnhovered);
		Button->OnHovered.AddUniqueDynamic(this, &UButtonHoverTextBlock::OnParentHovered);
		Button->OnReleased.AddUniqueDynamic(this, &UButtonHoverTextBlock::OnParentReleased);
		Button->OnHasFocus.AddUniqueDynamic(this, &UButtonHoverTextBlock::OnParentHasFocus);
		Button->OnLostFocus.AddUniqueDynamic(this, &UButtonHoverTextBlock::OnParentLostFocus);
	}
}

void UButtonHoverTextBlock::OnParentPressed()
{
	SetColorAndOpacity(PressedColour);
}

void UButtonHoverTextBlock::OnParentHasFocus()
{
	SetColorAndOpacity(FocusedColour);
}

void UButtonHoverTextBlock::OnParentLostFocus()
{
	SetColorAndOpacity(RegularColour);
}

void UButtonHoverTextBlock::OnParentUnhovered()
{
	SetColorAndOpacity(RegularColour);
}

void UButtonHoverTextBlock::OnParentHovered()
{
	SetColorAndOpacity(HoveredColour);
}

void UButtonHoverTextBlock::OnParentReleased()
{
	if (IsHovered())
	{
		SetColorAndOpacity(HoveredColour);
		return;
	}
	SetColorAndOpacity(RegularColour);
}