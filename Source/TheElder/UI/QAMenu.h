// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "QAMenu.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UQAMenu : public UUserWidget
{
	GENERATED_BODY()
public:
	UQAMenu(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UComboBoxString* AreasComboBox;
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UCheckBox* FirstTimeCheck;
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UCheckBox* StartFromCheck;
	UPROPERTY(BlueprintReadWrite, Category = "UI", meta = (BindWidget))
	class UElderButton* EnterButton;


	UPROPERTY(BlueprintReadOnly)
	TArray<FString> AreaOptions = { "Whole Level", "Forest", "Mushroom", "Boss"};
	
	class AQAManager* QAManager;

public: 
	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable)
	void SendQAData();
};
