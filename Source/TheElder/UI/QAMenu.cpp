// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "QAMenu.h"
#include "ComboBox.h"
#include "ComboBoxString.h"
#include "Public/Scalability.h"
#include "ElderButton.h"
#include "Engine/World.h"
#include "Components/CheckBox.h"
#include "Player/QAManager.h"

UQAMenu::UQAMenu(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

void UQAMenu::NativePreConstruct() {
	Super::NativePreConstruct();
	AreasComboBox->ClearOptions();
	for (FString Option : AreaOptions)
	{
		AreasComboBox->AddOption(Option);
	}
	AreasComboBox->SetSelectedOption(AreaOptions[0]);
}

void UQAMenu::NativeConstruct() {
	QAManager = GetWorld()->SpawnActor<AQAManager>(FVector(0, 0, 0), FRotator(0, 0, 0));
}

void UQAMenu::SendQAData() {
	QAManager->CreateSession(FirstTimeCheck->IsChecked(), AreasComboBox->GetSelectedOption());
}