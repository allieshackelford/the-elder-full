// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Utility/CheckpointPersistentData.h"
#include "World/ElderCheckpoint.h"
#include "UI/MainMenu.h"
#include "Utility/CheckpointFunctionLibrary.h"
#include "TheElderGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UTheElderGameInstance : public UGameInstance
{
	GENERATED_BODY()
		UTheElderGameInstance(const FObjectInitializer & ObjectInitializer);
public:
	virtual void Init() override;
	virtual void Shutdown() override;
	AElderCheckpoint* GetCheckpoint(AActor* Instigator) const;

	int GetCurrentCheckpointNum() const;
	UPROPERTY(BlueprintReadWrite)
	int CurrentCheckpointNum = 0;

	/* Used to check if the player is just starting the game, used mostly in-editor*/
	UPROPERTY(BlueprintReadWrite)
	bool bFirstRun = true;
	
	UPROPERTY()
	UCheckpointPersistentData* CheckpointData;
	UPROPERTY(BlueprintReadWrite)
	TSubclassOf<class UUserWidget> MainMenuClass;

	UPROPERTY()
	UCurveFloat* GammaCurve;


	const FString SettingsSaveSlot = TEXT("Settings");

	static float GetGammaMax()
	{
		return 50.0f;
	}
	
	static float GetGammaMin()
	{
		return 0.0001f;
	}
};