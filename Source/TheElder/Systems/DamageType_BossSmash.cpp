// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "DamageType_BossSmash.h"

UDamageType_BossSmash::UDamageType_BossSmash(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	bCausedByWorld = false;
}
