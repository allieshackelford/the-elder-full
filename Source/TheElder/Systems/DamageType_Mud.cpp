// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "DamageType_Mud.h"

UDamageType_Mud::UDamageType_Mud(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	bCausedByWorld = true;
}
