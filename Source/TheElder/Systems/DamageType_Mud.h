// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "DamageType_Mud.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UDamageType_Mud : public UDamageType
{
	GENERATED_BODY()

	UDamageType_Mud(const FObjectInitializer & ObjectInitializer);
};
