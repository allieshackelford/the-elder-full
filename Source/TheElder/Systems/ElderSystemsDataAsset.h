// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ElderSystemsDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UElderSystemsDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Loading/Death")
	float TimeToFadeIn = 0.1f;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Loading/Death")
	float TimeStayBlack = 0.5f;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Loading/Death")
	float TimeToFadeOut = 0.1f;
	/* The amount of time to give the engine to buffer streaming textures*/
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Loading")
	float TexturingStreamingBufferTime = 3.0f;

	/* Is this the build that goes up on the projector*/
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "PitchAndPlay")
	bool bIsPresentationBuild = false;
};
