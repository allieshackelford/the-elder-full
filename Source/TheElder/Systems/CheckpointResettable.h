// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Interface.h"
#include "CheckpointResettable.generated.h"

/**
 * 
 */
UINTERFACE(BlueprintType, Blueprintable)
class THEELDER_API UCheckpointResettable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class ICheckpointResettable
{
	GENERATED_IINTERFACE_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, Category = "Checkpoints")
	void ResetActor(uint8 StateToResetTo);
};
