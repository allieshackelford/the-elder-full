// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "DamageType_BossSmash.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UDamageType_BossSmash : public UDamageType
{
	GENERATED_BODY()
	
	UDamageType_BossSmash(const FObjectInitializer & ObjectInitializer);
};
