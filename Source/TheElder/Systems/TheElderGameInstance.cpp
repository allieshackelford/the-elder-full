// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "TheElderGameInstance.h"
#include "ConstructorHelpers.h"
#include "World/ElderCheckpoint.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include <string>
#include "UI/MainMenu.h"
#include "Engine/PlayerStartPIE.h"
#include "ElderSaveGame.h"
#include "Kismet/KismetMathLibrary.h"

UTheElderGameInstance::UTheElderGameInstance(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<UCheckpointPersistentData> TheCheckpointData(TEXT("/Game/Blueprints/Utility/DA_CheckpointPersistentData"));
	CheckpointData = TheCheckpointData.Object;

	static ConstructorHelpers::FClassFinder<UUserWidget> TheMainMenuWidget(TEXT("/Game/Blueprints/UI/BPW_MainMenu"));
	MainMenuClass = TheMainMenuWidget.Class;

	static ConstructorHelpers::FObjectFinder<UCurveFloat> GammaCurveAsset(TEXT("/Game/Data/Curves/GammaCurve"));
	GammaCurve = GammaCurveAsset.Object;

}

void UTheElderGameInstance::Init()
{
	CurrentCheckpointNum = 0;
#if WITH_EDITOR
	if (CheckpointData != nullptr)
	{
		CurrentCheckpointNum = CheckpointData->CurrentEditorCheckpoint;
	}
#endif

	// Time to load data from checkpoints
	UElderSaveGame* LoadedGame = Cast<UElderSaveGame>(UGameplayStatics::LoadGameFromSlot(UTheElderGameInstance::SettingsSaveSlot, 0));

	if (LoadedGame != nullptr)
	{
		float NewGammaValue = LoadedGame->GammaValue;
		if (GammaCurve != nullptr)
		{
			NewGammaValue = GammaCurve->GetFloatValue(LoadedGame->GammaValue);
		}
		auto GammaValue = UKismetMathLibrary::MapRangeClamped(NewGammaValue, 0.0f, 1.0f, GetGammaMin(), GetGammaMax());
		GEngine->DisplayGamma = GammaValue;
	}
	
	Super::Init();
}

void UTheElderGameInstance::Shutdown()
{
	Super::Shutdown();
}

AElderCheckpoint* UTheElderGameInstance::GetCheckpoint(AActor* Instigator) const
{	
	auto LevelName = UGameplayStatics::GetCurrentLevelName(Instigator);
	if (CheckpointData == nullptr)
		return nullptr;
	auto Checkpoints = CheckpointData->GetArray(LevelName);
	if (Checkpoints.Num() == 0)
		return nullptr;
	
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsOfClass(Instigator, AElderCheckpoint::StaticClass(), OutActors);

	if (CurrentCheckpointNum >= Checkpoints.Num())
	{
		return nullptr;
	}
	auto TheCheckpointName = CheckpointData->GetCheckpointInformation(LevelName, CurrentCheckpointNum).CheckpointName;
	for (auto Actor : OutActors)
	{
		auto Checkpoint = Cast<AElderCheckpoint>(Actor);
		if (Checkpoint->GetName() == TheCheckpointName)
		{
			return Checkpoint;
		}
	}
	return nullptr;
}

int UTheElderGameInstance::GetCurrentCheckpointNum() const
{
	return CurrentCheckpointNum;
}