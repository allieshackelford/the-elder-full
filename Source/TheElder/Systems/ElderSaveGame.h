// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "World/ElderCheckpoint.h"
#include "ElderSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API UElderSaveGame : public USaveGame
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
	float GammaValue;

	UPROPERTY(BlueprintReadWrite)
	float MusicVolume = 1.0f;
	UPROPERTY(BlueprintReadWrite)
	float SFXVolume = 1.0f;

	UPROPERTY(BlueprintReadWrite)
	FString GraphicsSetting;
	
	UPROPERTY(BlueprintReadOnly)
	FString CurrentCheckpointName;
};
