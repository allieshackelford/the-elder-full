// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "SapBase.h"
#include "ExplodingSap.generated.h"

UCLASS()
class THEELDER_API AExplodingSap : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExplodingSap();
	UPROPERTY(EditAnywhere, Category = "Tunables")
	int NumOfExplosions = 6;
	UPROPERTY(EditAnywhere, Category = "Tunables")
	float DistanceFromSource = 100;
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	float SapExplosionSpeed = 3;
	UPROPERTY(VisibleAnywhere, Meta = (MakeEditWidget = true))
	TArray<FVector> ExplosionLocations;
	UPROPERTY()
	TArray<ASapBase*> ExplodedSaps;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASapBase> SapBlueprint;

	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UArrowComponent* ForwardArrow;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	float MovingLerpValue = 0.0f;
	bool bHasExploded = false;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Explode();
	void OnConstruction(const FTransform& Transform) override;
};
