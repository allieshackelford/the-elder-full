// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SappableSurface.generated.h"

class ASapBase;
UCLASS()
class THEELDER_API ASappableSurface : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASappableSurface();
	
	UPROPERTY(EditAnywhere)
	bool bIsMoving = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh;

	UFUNCTION(BlueprintNativeEvent, Category = "Sap Events")
	void SapHitSurface(ASapBase* Sap);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Used by SapHitSurface*/
	void SapHitSurface_Implementation(ASapBase* Sap);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
