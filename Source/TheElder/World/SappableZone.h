// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Utility/ClimbingFunctionLibrary.h"
#include "SappableZone.generated.h"

USTRUCT()
struct FClimbableNode
{
	GENERATED_BODY()

	FClimbableNode() : Self(nullptr)
	{
	}

	explicit FClimbableNode(class ASapBase* RootSap) : Self(RootSap)
	{
		
	}

	FClimbableNode(class ASapBase* RootSap, TArray<class ASapBase*> PreConnectedSaps) : Self(RootSap), ConnectedSaps(PreConnectedSaps)
	{
	}

	UPROPERTY()
	class ASapBase* Self;

	UPROPERTY()
	TArray<ASapBase*> ConnectedSaps;

	bool bHasTraversed = false;

	THEELDER_API friend bool operator==(const FClimbableNode& Left, const FClimbableNode& Right);
};

USTRUCT()
struct FNodeBranch
{
	GENERATED_BODY()
	
	UPROPERTY()
	TArray<FClimbableNode> Nodes;

	// Do we want to use initial string index or pointer?
	int BranchPointIndex;
	bool bDoesConnectToLedge = false;
};

USTRUCT()
struct FNodeWeb
{
	GENERATED_BODY()

	/* Index 0 of the Branches arrray should always be the inital string*/
	TArray<FNodeBranch> Branches;
	bool bDoesConnectToLedge = false;
	// The maximum distance in the target direction that this web reaches
	float Distance;
};

UCLASS()
class THEELDER_API ASappableZone : public AActor
{
	GENERATED_BODY()
	
public:	
	ASappableZone();

protected:
	virtual void BeginPlay() override;

	/* This is used to delay Begin Play one frame to allow each SapBase to do their things*/
	UFUNCTION()
	void DelayedBeginPlay();

	virtual void OnConstruction(const FTransform& Transform) override;
	void ProcessNodes();

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, meta = (AllowPrivateAccess = true))
	class UArrowComponent* DirectionalArrow;
	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, meta = (AllowPrivateAccess = true))
	class UBillboardComponent* RootIcon;
	
public:
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (AllowPrivateAccess = true))
	TArray<class ASappableSurface*> TrackedSurfaces;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (DisplayName = "Static Crystals"))
	TArray<class ASapBase*> StaticSaps;

private:
	UPROPERTY()
	TArray<FClimbableNode> Nodes;
};
