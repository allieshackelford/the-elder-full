// Fill out your copyright notice in the Description page of Project Settings.


#include "SplineLedge.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "Engine/StaticMesh.h"

ASplineLedge::ASplineLedge()
{
	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = SceneComp;
	Spline = CreateDefaultSubobject<USplineComponent>(TEXT("Spline"));
	Spline->SetupAttachment(RootComponent);

	// Setup for all the arrow
	ForwardArrowStart = CreateDefaultSubobject<UArrowComponent>("ForwardArrowStart");
	ForwardArrowStart->SetWorldScale3D(FVector::OneVector * 2.0f);
	ForwardArrowStart->SetupAttachment(RootComponent);

	ForwardArrowEnd = CreateDefaultSubobject<UArrowComponent>("ForwardArrowEnd");
	ForwardArrowEnd->SetWorldScale3D(FVector::OneVector * 2.0f);
	ForwardArrowEnd->SetupAttachment(RootComponent);

	ForwardArrowMiddle = CreateDefaultSubobject<UArrowComponent>("ForwardArrowMiddle");
	ForwardArrowMiddle->SetWorldScale3D(FVector::OneVector * 2.0f);
	ForwardArrowMiddle->SetupAttachment(RootComponent);

	UpArrowMiddle = CreateDefaultSubobject<UArrowComponent>("UpArrowMiddle");
	UpArrowMiddle->SetWorldScale3D(FVector::OneVector * 2.0f);
	UpArrowMiddle->SetupAttachment(RootComponent);
}

FTransform ASplineLedge::GetClimbUpTransform(AActor* Player) const
{
	if (Player == nullptr)
		return FTransform();
	FVector WorldLocation = Spline->FindLocationClosestToWorldLocation(Player->GetActorLocation(), ESplineCoordinateSpace::World);
	FVector RightDirection = Spline->FindRightVectorClosestToWorldLocation(Player->GetActorLocation(), ESplineCoordinateSpace::World);
	FTransform Result;
	Result.SetLocation(WorldLocation);
	Result.SetRotation(RightDirection.Rotation().Quaternion());
	return Result;
}

void ASplineLedge::OnConstruction(const FTransform& Transform)
{
	auto World = GetWorld();
	if (World == nullptr)
	{
		return;
	}
	SplineMeshes.Empty();
	for (int i = 0; i <= Spline->GetNumberOfSplinePoints() - 2; i++)
	{
		
		auto SplineMesh = NewObject<USplineMeshComponent>(this, USplineMeshComponent::StaticClass());
		if (MeshForSpline != nullptr)
		{
			SplineMesh->SetStaticMesh(MeshForSpline);
		}
		SplineMesh->RegisterComponent();
		SplineMesh->CreationMethod = EComponentCreationMethod::SimpleConstructionScript;
		SplineMesh->SetMobility(EComponentMobility::Movable);
		SplineMesh->ForwardAxis = ESplineMeshAxis::Z;
		FVector StartLocation;
		FVector StartTangent;
		FVector EndLocation;
		FVector EndTangent;
		Spline->GetLocationAndTangentAtSplinePoint(i, StartLocation, StartTangent, ESplineCoordinateSpace::Local);
		Spline->GetLocationAndTangentAtSplinePoint(i + 1, EndLocation, EndTangent, ESplineCoordinateSpace::Local);
		SplineMesh->SetStartAndEnd(StartLocation, StartTangent, EndLocation, EndTangent);
		SplineMesh->AttachToComponent(Spline, FAttachmentTransformRules::FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
		SplineMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		SplineMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
		SplineMesh->SetHiddenInGame(true);
		// Generating overlap events causes a lot of performance issues on moving objects
		SplineMesh->SetGenerateOverlapEvents(false);
		if (SplineMeshMaterial != nullptr)
		{
			SplineMesh->SetMaterial(0, SplineMeshMaterial);
		}
		SplineMeshes.Add(SplineMesh);
	}

	// Now let's just align the arrows
	ForwardArrowStart->SetWorldRotation(Spline->GetRightVectorAtTime(0, ESplineCoordinateSpace::World).Rotation());
	ForwardArrowStart->SetWorldLocation(Spline->GetLocationAtTime(0, ESplineCoordinateSpace::World));

	ForwardArrowEnd->SetWorldRotation(Spline->GetRightVectorAtTime(1, ESplineCoordinateSpace::World).Rotation());
	ForwardArrowEnd->SetWorldLocation(Spline->GetLocationAtTime(1, ESplineCoordinateSpace::World));

	ForwardArrowMiddle->SetWorldRotation(Spline->GetRightVectorAtTime(0.5f, ESplineCoordinateSpace::World).Rotation());
	ForwardArrowMiddle->SetWorldLocation(Spline->GetLocationAtTime(0.5f, ESplineCoordinateSpace::World));

	UpArrowMiddle->SetWorldRotation(Spline->GetUpVectorAtTime(0.5f, ESplineCoordinateSpace::World).Rotation());
	UpArrowMiddle->SetWorldLocation(Spline->GetLocationAtTime(0.5f, ESplineCoordinateSpace::World));
}

void ASplineLedge::BeginPlay()
{
	Super::BeginPlay();
//#if !WITH_EDITOR
	ExecuteConstruction(GetActorTransform(), nullptr, nullptr);
//#endif
}