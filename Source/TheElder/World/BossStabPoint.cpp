// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "BossStabPoint.h"
#include "Components/BoxComponent.h"
#include "MasterIncludes.h"

// Sets default values
ABossStabPoint::ABossStabPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PlayerDetector = CreateDefaultSubobject<UBoxComponent>(TEXT("PlayerDetector"));
	PlayerDetector->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PlayerDetector->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	PlayerDetector->SetCollisionResponseToChannel(ECC_MokoshChannel, ECollisionResponse::ECR_Overlap);

	PlayerDetector->SetCanEverAffectNavigation(false);
	PlayerDetector->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABossStabPoint::BeginPlay()
{
	Super::BeginPlay();

	auto World = GetWorld();
	if (World == nullptr)
		return;

	PlayerDetector->OnComponentBeginOverlap.AddDynamic(this, &ABossStabPoint::OnPlayerDetectorOverlapBegin);
	PlayerDetector->OnComponentEndOverlap.AddDynamic(this, &ABossStabPoint::OnPlayerDetectorOverlapEnd);

	auto PlayerController = UGameplayStatics::GetPlayerController(World, 0);
	if (PlayerController != nullptr)
	{
		EnableInput(PlayerController);
		FInputActionBinding& Binding = InputComponent->BindAction("Interact", IE_Pressed, this, &ABossStabPoint::InteractPressed);
		Binding.bConsumeInput = false;
	}
}

void ABossStabPoint::InteractPressed()
{
	if (bPlayerIsOverlapped)
	{
		StabEventStarted();
	}
}

void ABossStabPoint::StabEventStarted()
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
	
	// TODO: We need to activate a big cinematic moment here.
	PlayerDetector->OnComponentBeginOverlap.RemoveAll(this);
	PlayerDetector->OnComponentEndOverlap.RemoveAll(this);
	bPlayerIsOverlapped = false;

	// TODO: This should only get broadcasted after the successful quicktime event
	OnStabbed.Broadcast();
}

// Called every frame
void ABossStabPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABossStabPoint::OnPlayerDetectorOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto Mokosh = Cast<AMokosh>(OtherActor);
	if (Mokosh != nullptr && Mokosh->GetController() != nullptr)
	{
		// TODO: Should pop up some kind of fancy UI
		bPlayerIsOverlapped = true;
	}
}

void ABossStabPoint::OnPlayerDetectorOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (Cast<AMokosh>(OtherActor) != nullptr)
	{
		// TODO: Should pop up some kind of fancy UI
		bPlayerIsOverlapped = false;
	}
}