// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "ElderCheckpoint.h"
#include "Kismet/GameplayStatics.h"
#include "TheElderGameMode.h"
#include "Engine/TextRenderActor.h"
#include "Components/TextRenderComponent.h"
#include "Systems/TheElderGameInstance.h"
#include "ConstructorHelpers.h"
#include "Engine/Engine.h"
#include "Player/Mokosh.h"

AElderCheckpoint::AElderCheckpoint(const FObjectInitializer& ObjectInitializer): Super(ObjectInitializer)
{
	TextNumber = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRender"));
	TextNumber->SetupAttachment(RootComponent);
	TextNumber->SetHiddenInGame(true);
	TextNumber->SetWorldScale3D(FVector(4.0f));
	TextNumber->SetRelativeLocation(FVector(0, 0, 100));
	TextNumber->SetTextRenderColor(FColor::Red);
	TextNumber->Text = NSLOCTEXT("CheckpointNamespace", "BlankCheckpointText", "Text");

	static ConstructorHelpers::FObjectFinder<UCheckpointPersistentData> TheCheckpointData(TEXT("/Game/Blueprints/Utility/DA_CheckpointPersistentData"));
	CheckpointData = TheCheckpointData.Object;
	CachedCheckpointName = GetName();

	Collision = CreateDefaultSubobject<UBoxComponent>("Trigger");
	Collision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Collision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	Collision->ShapeColor = FColor::Purple;
	// Only overlap with Mokosh
	Collision->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Overlap);
	Collision->SetCanEverAffectNavigation(false);
	Collision->SetupAttachment(RootComponent);
}

void AElderCheckpoint::BeginPlay()
{
	Super::BeginPlay();
	Collision->OnComponentBeginOverlap.AddDynamic(this, &AElderCheckpoint::OnTriggerOverlapBegin);
}

void AElderCheckpoint::OnTriggerOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, 
											 int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Cast<AMokosh>(OtherActor) == nullptr)
	{
		return;
	}
	auto GameInstance = Cast<UTheElderGameInstance>(GetGameInstance());
	if (GameInstance != nullptr)
	{
		if (GameInstance->CurrentCheckpointNum < CachedCheckpointNumber)
		{
			GameInstance->CurrentCheckpointNum = CachedCheckpointNumber;
		}
	}
}

void AElderCheckpoint::OnConstruction(const FTransform& Transform)
{
#if WITH_EDITOR
	// Let's check if we have a brand new thing
	if (CachedCheckpointName.IsEmpty())
	{
		// This shouldn't ever get called now
		
		checkNoEntry();
		/*
		CachedCheckpointName = GetName();
		int Index = RegisterCheckpoint();
		UpdateText(Index);
		*/
		
	}
	// If TextNumber's value is equal to Text, then we just created this checkpoint
	else if (TextNumber->Text.EqualToCaseIgnored(NSLOCTEXT("CheckpointNamespace", "BlankCheckpointText", "Text")))
	{
		auto Index = RegisterCheckpoint();
		CheckpointNameWasChanged();
		CachedCheckpointName = GetName();
		CachedCheckpointNumber = Index;
		UpdateText(Index);
	}
	else if (CachedCheckpointName != GetName())
	{
		CheckpointNameWasChanged();
		CachedCheckpointName = GetName();
	}

	auto NewTransform = TriggerPosition;
	NewTransform.SetScale3D(TriggerPosition.GetScale3D() * (FVector::OneVector * 5));
	Collision->SetRelativeTransform(NewTransform);
#endif
}

void AElderCheckpoint::Destroyed()
{
#if WITH_EDITOR
	RemoveCheckpoint();
#endif
}

void AElderCheckpoint::OrderChanged(int NewIndex)
{
	CachedCheckpointNumber = NewIndex;
	UpdateText(NewIndex);
}

void AElderCheckpoint::UpdateText(int Value)
{
	TextNumber->SetText(FText::Format(NSLOCTEXT("CheckpointNamespace", "CheckpointText", "{0}"), Value));
}

/*
 * We need to check if there is still an actor with the previous name, and if there is then we want to add a new checkpoint, because this checkpoint was duplicated.
 */
void AElderCheckpoint::CheckpointNameWasChanged()
{
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsOfClass(this, AElderCheckpoint::StaticClass(), OutActors);
	for (auto Actor : OutActors)
	{
		auto Checkpoint = Cast<AElderCheckpoint>(Actor);
		if (Actor == this)
			continue;
		// If there is an actor with the previous name, then this checkpoint was created from a duplicate, and we need to register this as a new checkpoint
		if (Actor->GetName() == CachedCheckpointName)
		{
			int Index = RegisterCheckpoint();
			CachedCheckpointNumber = Index;
			UpdateText(Index);
			return;
		}
	}

	// Looks like we just renamed the checkpoint, that's ok, we just need to update the checkpointData
	auto LevelName = UGameplayStatics::GetCurrentLevelName(this);
	if (CheckpointData == nullptr)
		return;
	FCheckpoints* Checkpoints = CheckpointData->FindCheckpoint(LevelName);
	if (Checkpoints == nullptr)
	{
		return;
	}
	int Index = CheckpointData->FindCheckpoint(LevelName, CachedCheckpointName);
	UpdateText(Index);

	// If we didn't find this checkpoint in the data
	// TODO: Probably should register this as a new checkpoint.
	if (Index == -1)
	{
		return;
	}
	auto CheckpointInformation = CheckpointData->GetCheckpointInformation(LevelName, Index);
	CheckpointInformation.CheckpointName = GetName();
	CheckpointData->SetCheckpointInformation(LevelName, Index, CheckpointInformation);
}

int AElderCheckpoint::RegisterCheckpoint() const
{
	auto LevelName = UGameplayStatics::GetCurrentLevelName(this);
	if (CheckpointData == nullptr)
		return -1;
	FCheckpoints* Checkpoints = CheckpointData->FindCheckpoint(LevelName);

	// If we don't have any checkpoints for this level yet.
	if (Checkpoints == nullptr)
	{
		auto NewCheckpoints = CheckpointData->NewCheckpointCategory(LevelName);
		auto CheckpointInfo = FCheckpointInformation();
		CheckpointInfo.CheckpointName = GetName();
		CheckpointInfo.SublevelsToLoad = TArray<FString>();

		return CheckpointData->AddCheckpointInformation(LevelName, CheckpointInfo);
	}
	
	//Not one already in there, let's add a new one
	auto CheckpointInfo = FCheckpointInformation();
	CheckpointInfo.CheckpointName = GetName();
	CheckpointInfo.SublevelsToLoad = TArray<FString>();
	GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString::Printf(TEXT("%s, %s"), *GetName(), *CachedCheckpointName));
	return CheckpointData->AddCheckpointInformation(LevelName, CheckpointInfo);
}

void AElderCheckpoint::RemoveCheckpoint() const
{
	auto LevelName = UGameplayStatics::GetCurrentLevelName(this);
	if (CheckpointData == nullptr)
		return;
	
	FCheckpoints* Checkpoints = CheckpointData->FindCheckpoint(LevelName);
	if (Checkpoints == nullptr)
	{
		return;
	}
	
	CheckpointData->RemoveCheckpoint(LevelName, GetName());

	// Need to update the TextRender for all the other checkpoints here.
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsOfClass(this, AElderCheckpoint::StaticClass(), OutActors);
	for (auto Actor : OutActors)
	{
		auto Checkpoint = Cast<AElderCheckpoint>(Actor);
		int NewIndex = CheckpointData->FindCheckpoint(LevelName, Checkpoint->GetName());
		Checkpoint->OrderChanged(NewIndex);
	}
}