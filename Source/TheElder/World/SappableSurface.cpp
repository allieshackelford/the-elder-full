// Fill out your copyright notice in the Description page of Project Settings.

#include "SappableSurface.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ASappableSurface::ASappableSurface()
{
	PrimaryActorTick.bCanEverTick = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	RootComponent = StaticMesh;
	
}

// Called when the game starts or when spawned
void ASappableSurface::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASappableSurface::SapHitSurface_Implementation(ASapBase* Sap)
{
	
}

// Called every frame
void ASappableSurface::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}