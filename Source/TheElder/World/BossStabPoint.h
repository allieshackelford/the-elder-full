// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "BossStabPoint.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);

UCLASS()
class THEELDER_API ABossStabPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABossStabPoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void InteractPressed();
	void StabEventStarted();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnPlayerDetectorOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	                                  const FHitResult& SweepResult);
	
	UFUNCTION()
	void OnPlayerDetectorOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	                                UPrimitiveComponent* OtherComp,
	                                int32 OtherBodyIndex);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	UBoxComponent* PlayerDetector;

	UPROPERTY(BlueprintAssignable)
	FNoParamDelegate OnStabbed;
	
	bool bPlayerIsOverlapped;
};
