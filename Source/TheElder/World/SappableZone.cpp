// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#include "SappableZone.h"
#include "Components/BoxComponent.h"
#include "Components/BillboardComponent.h"
#include "Components/ArrowComponent.h"
#include "SapBase.h"
#include "TimerManager.h"

ASappableZone::ASappableZone()
{
	PrimaryActorTick.bCanEverTick = false;
	
	RootIcon = CreateDefaultSubobject<UBillboardComponent>(TEXT("Icon"));
	RootComponent = RootIcon;
	RootIcon->SetHiddenInGame(true);

	DirectionalArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("DirectionalArrow"));
	DirectionalArrow->SetupAttachment(RootIcon);
	DirectionalArrow->SetHiddenInGame(true);
}

void ASappableZone::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle handle;
	GetWorld()->GetTimerManager().SetTimer(handle, this, &ASappableZone::DelayedBeginPlay, 0.0f, false);
}

void ASappableZone::DelayedBeginPlay()
{
	for (auto Sap : StaticSaps)
	{
		if (Sap->bApproved)
		{
			auto Index = Nodes.AddUnique(FClimbableNode(Sap, Sap->GetConnectedSaps()));
		}
	}
}

void ASappableZone::OnConstruction(const FTransform& Transform)
{
	DirectionalArrow->SetRelativeLocation(FVector::ZeroVector);
	auto PreviousRotation = DirectionalArrow->GetRelativeRotationCache();
	DirectionalArrow->SetRelativeRotation(FRotator(PreviousRotation.GetCachedRotator().Pitch, 90, 90));
}

void ASappableZone::ProcessNodes()
{
	// Just reset all the nodes
	for (auto Node : Nodes)
	{
		Node.bHasTraversed = false;
	}

	TArray<FNodeWeb> Web;
	Web.AddDefaulted();
	
	// Initial search
	Web[0].Branches.AddDefaulted();
	Web[0].Branches[0].Nodes.Add(Nodes[0]);
	Nodes[0].bHasTraversed = true;
	
	bool bHasTraversedAll = false;

	int CurWebIndex = 0;
	int CurBranchIndex = 0;
	int CurNodeIndex = 0;
	while (bHasTraversedAll == false)
	{
		int UnusedIndex = -1;
		for (int i = 0; i < Web[CurWebIndex].Branches[CurBranchIndex].Nodes[CurNodeIndex].ConnectedSaps.Num(); ++i)
		{
			if (Web[CurWebIndex].Branches[CurBranchIndex].Nodes[CurNodeIndex].ConnectedSaps[i])
			{
				
			}
		}
	}
}

bool operator==(const FClimbableNode& Left, const FClimbableNode& Right)
{
	return Left.Self == Right.Self;
}