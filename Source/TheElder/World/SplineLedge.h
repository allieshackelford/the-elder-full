// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "World/Climbable.h"
#include "Components/SplineComponent.h"
#include "Components/ArrowComponent.h"
#include "SplineLedge.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API ASplineLedge : public AClimbable
{
	GENERATED_BODY()
public:
	ASplineLedge();
	FTransform GetClimbUpTransform(AActor* Player) const;
	void OnConstruction(const FTransform& Transform) override;
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* SceneComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USplineComponent* Spline;
	UPROPERTY(EditDefaultsOnly)
	UStaticMesh* MeshForSpline;
	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* SplineMeshMaterial;

	// LOL, Arrows
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UArrowComponent* ForwardArrowStart;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UArrowComponent* ForwardArrowEnd;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UArrowComponent* ForwardArrowMiddle;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UArrowComponent* UpArrowMiddle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<class USplineMeshComponent*> SplineMeshes;
};
