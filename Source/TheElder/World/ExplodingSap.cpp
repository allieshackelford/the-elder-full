// Fill out your copyright notice in the Description page of Project Settings.

#include "ExplodingSap.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/Engine.h"

// Sets default values
AExplodingSap::AExplodingSap()
{
	PrimaryActorTick.bCanEverTick = true;
	// We don't need to tick until we're moving the sap post-explosion
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	RootComponent = StaticMesh;
	ForwardArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Forward Arrow"));
	ForwardArrow->SetupAttachment(RootComponent);
	ForwardArrow->SetRelativeRotation((-1*GetActorRightVector()).Rotation());
	SetActorTickEnabled(false);
}

void AExplodingSap::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickEnabled(false);
}

void AExplodingSap::Tick(float DeltaTime)
{
	if (ExplosionLocations.Num() <= 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, TEXT("Explosion Locations is empty in Tick for Exploding Sap"));
		return;
	}
	
	MovingLerpValue += 0.1 * SapExplosionSpeed * DeltaTime;
	for (int i = 0; i < ExplodedSaps.Num(); i++)
	{
		ExplodedSaps[i]->SetActorLocation(
			UKismetMathLibrary::VLerp(	GetActorLocation(), 
										UKismetMathLibrary::TransformLocation(GetActorTransform(), ExplosionLocations[i]), 
										MovingLerpValue));
	}
	if (MovingLerpValue >= 1.0f)
	{
		for (int i = 0; i < ExplodedSaps.Num(); i++)
		{
			ExplodedSaps[i]->bIsClimbable = true;
			// Aaaand, we don't want to tick any more
			SetActorTickEnabled(false);
			ExplodedSaps[i]->SetActorRotation(GetActorRotation() + FRotator(0, 90, 0));
		}
	}
	Super::Tick(DeltaTime);
}

void AExplodingSap::Explode()
{
	auto World = GetWorld();
	if (World == nullptr)
	{
		return;
	}
	if (SapBlueprint == nullptr)
	{
		return;
	}
	if (bHasExploded)
	{
		return;
	}
	
	bHasExploded = true;
	
	for (int i = 0; i < ExplosionLocations.Num(); i++)
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.Owner = this;
		SpawnInfo.Instigator = Instigator;
		
		auto item = ExplodedSaps.Add(World->SpawnActor<ASapBase>(	SapBlueprint, GetActorLocation(), GetActorRotation() + FRotator(0, 90, 0), SpawnInfo));
		
		// We want to make sure it's not climbable while it's exploding.
		ExplodedSaps[item]->bIsClimbable = false;
	}

	SetActorTickEnabled(true);
	StaticMesh->SetVisibility(false);
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AExplodingSap::OnConstruction(const FTransform& Transform)
{
	TArray<FVector> NewLocations;
	NewLocations.Init(FVector::ZeroVector, NumOfExplosions);

	ExplosionLocations.SetNum(NumOfExplosions);
	
	const float RotDistance = 360.0f / NumOfExplosions;
	for (int i = 0; i < NewLocations.Num(); i++)
	{
		auto Direction = FRotator(i * RotDistance, 0, 0).Vector();
		
		
		NewLocations[i] = Direction * DistanceFromSource;
		if (FMath::IsNearlyEqual(ExplosionLocations[i].X, 0))
		{
			ExplosionLocations[i] = NewLocations[i];
		}
		else
		{
			NewLocations[i].Y = ExplosionLocations[i].Y;
			ExplosionLocations[i] = NewLocations[i];
		}
	}
}