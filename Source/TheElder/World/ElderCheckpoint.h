// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerStart.h"
#include "Engine/TextRenderActor.h"
#include "ConstructorHelpers.h"
#include "Utility/CheckpointPersistentData.h"
#include "Components/BoxComponent.h"
#include "ElderCheckpoint.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API AElderCheckpoint : public APlayerStart
{
	GENERATED_BODY()
	
	AElderCheckpoint(const FObjectInitializer& ObjectInitializer);
public:
	virtual void BeginPlay() override;
	void OnConstruction(const FTransform& Transform) override;
	void Destroyed() override;
	/* This is called by the checkpoint GUI system when the developer changes the order of an checkpoint*/
	UFUNCTION(BlueprintCallable)
	void OrderChanged(int NewIndex);
	void UpdateText(int Value);
	void CheckpointNameWasChanged();
	int RegisterCheckpoint() const;
	void RemoveCheckpoint() const;

	UPROPERTY()
	UTextRenderComponent* TextNumber;
	
	UPROPERTY()
	UCheckpointPersistentData* CheckpointData;

	/* This is used to check if the name of the checkpoint has changed*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FString CachedCheckpointName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int CachedCheckpointNumber;

	UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
	FTransform TriggerPosition;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UShapeComponent* Collision;
private:
	UFUNCTION()
	void OnTriggerOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
