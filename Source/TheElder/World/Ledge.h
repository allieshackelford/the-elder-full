// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "World/Climbable.h"
#include "Ledge.generated.h"

/**
 * 
 */
UCLASS()
class THEELDER_API ALedge : public AClimbable
{
	GENERATED_BODY()
	
};
