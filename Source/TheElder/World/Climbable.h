// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Climbable.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FClimbableGrabbedDelegate);

UCLASS()
class THEELDER_API AClimbable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AClimbable();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bIsMoving = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bIsClimbable = true;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(BlueprintAssignable, Category = "Sap Delegates")
	FClimbableGrabbedDelegate OnClimbableGrabbed;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void PlayerHasGrabbed();
};
