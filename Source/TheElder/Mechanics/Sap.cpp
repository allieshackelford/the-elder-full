// Fill out your copyright notice in the Description page of Project Settings.


#include "Sap.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "World/ExplodingSap.h"
#include "World/SappableSurface.h"
#include "World/BouncyMushroom.h"

ASap::ASap()
{
	PrimaryActorTick.bCanEverTick = true;

	// This object's position is generated dynamically at runtime
	bIsStaticallyPlaced = false;
	//Sphere->SetSimulatePhysics(false);
	//Sphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	//Sphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);

	// The event for overlap is bound in BeginPlay

	
	ProjectileComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	
	ProjectileComponent->SetUpdatedComponent(RootComponent);
	ProjectileComponent->bShouldBounce = false;
	// We enable gravity later as specified in the metrics
	ProjectileComponent->ProjectileGravityScale = 0.0f;

	// We're not climbable until we've hit a target
	bIsClimbable = false;
}

void ASap::BeginPlay()
{
	Super::BeginPlay();

	InitialPosition = GetActorLocation();
	
	StaticMesh->OnComponentHit.AddDynamic(this, &ASap::OnOverlapBegin);
}

void ASap::OnHitExplodingSap(AExplodingSap* HitSap, const FHitResult& SweepResult)
{
	HitSap->Explode();
	OnHitDelegate.Broadcast(SweepResult.Location, (-1 * SweepResult.ImpactNormal).Rotation(), ESapHitType::SHT_ExplodingSap);
	Destroy();
}

void ASap::HitOtherSap(ASap* OtherSap, const FHitResult& SweepResult)
{
	OnHitDelegate.Broadcast(SweepResult.Location, (-1 * SweepResult.ImpactNormal).Rotation(), ESapHitType::SHT_OtherSap);
	Destroy();
}

void ASap::HitSappableSurface(ASappableSurface* Surface, const FHitResult& SweepResult)
{
	bIsMoving = Surface->bIsMoving;
	bIsClimbable = true;
	OnHitDelegate.Broadcast(SweepResult.Location, (-1 * SweepResult.ImpactNormal).Rotation(), ESapHitType::SHT_SapWall);

	Surface->SapHitSurface(this);
	
	// We don't want to still receive overlap events
	StaticMesh->OnComponentHit.RemoveDynamic(this, &ASap::OnOverlapBegin);
	SetActorRotation((-1 * SweepResult.ImpactNormal).Rotation());

	auto Offset = GetActorForwardVector() * WallOffset;
	SetActorLocation(SweepResult.ImpactPoint + Offset);

	GenerateLasers();
}

void ASap::HitRegularThing(AActor* Thing, const FHitResult& SweepResult)
{
	SetActorRotation((-1 * SweepResult.ImpactNormal).Rotation());
	OnHitDelegate.Broadcast(SweepResult.Location, (-1 * SweepResult.ImpactNormal).Rotation(), ESapHitType::SHT_NormalWall);
	// We don't want to still receive overlap events
	StaticMesh->OnComponentHit.RemoveDynamic(this, &ASap::OnOverlapBegin);
}

void ASap::HitBouncyMushroom(ABouncyMushroom* Shroom, const FHitResult& SweepResult)
{
	// We just waitin' for the mushroom to trigger a bounce back.
	OnHitDelegate.Broadcast(SweepResult.Location, (-1 * SweepResult.ImpactNormal).Rotation(), ESapHitType::SHT_BouncyObject);
}

void ASap::OnOverlapBegin(	class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, FVector NormalImpulse, 
							const FHitResult& SweepResult)
{
	auto ExplodingSap = Cast<AExplodingSap>(OtherActor);
	auto HitSap = Cast<ASap>(OtherActor);
	auto SappableSurface = Cast<ASappableSurface>(OtherActor);
	auto Shroom = Cast<ABouncyMushroom>(OtherActor);
	
	if (ExplodingSap != nullptr)
	{
		OnHitExplodingSap(ExplodingSap, SweepResult);
	}
	else if (SappableSurface != nullptr)
	{
		CheckSapAttachStatus(OtherActor, OtherComp, SweepResult);
		HitSappableSurface(SappableSurface, SweepResult);
	}
	else if (HitSap != nullptr)
	{
		HitOtherSap(HitSap, SweepResult);
	}
	else if (Shroom != nullptr)
	{
		HitBouncyMushroom(Shroom, SweepResult);
	}
	// This should never happen
	else if (SweepResult.bBlockingHit == false)
	{
		check(!"Exploding Sap hit an OnComponentHit with a non blocking hit! This should never happen!")
	}
	else
	{
		CheckSapAttachStatus(OtherActor, OtherComp, SweepResult);
		HitRegularThing(OtherActor, SweepResult);
	}
}

void ASap::CheckSapAttachStatus(AActor* OtherActor, UPrimitiveComponent* OtherComp, const FHitResult& SweepResult)
{
	FAttachmentTransformRules AttachmentRules = { EAttachmentRule::KeepWorld, true };
	if (OtherComp != nullptr)
	{
		if (OtherComp->Mobility == EComponentMobility::Movable)
		{
			bIsMoving = true;
			AttachToComponent(OtherComp, AttachmentRules, SweepResult.BoneName);
		}
	}
	else if (OtherActor != nullptr && OtherActor->IsRootComponentMovable())
	{
		bIsMoving = true;
		AttachToActor(OtherActor, AttachmentRules, SweepResult.BoneName);
	}
}

void ASap::OnConstruction(const FTransform& Transform)
{
	if (ProjectileComponent != nullptr)
	{
		ProjectileComponent->InitialSpeed = SapSpeed;
	}
}

void ASap::OnBounce()
{
	ProjectileComponent->ProjectileGravityScale = 0.0f;
	InitialPosition = GetActorLocation();
	SetActorTickEnabled(true);
}

void ASap::ReachedMaxStraightDistance()
{
	ProjectileComponent->ProjectileGravityScale = ProjectileGravityScale;
	SetActorTickEnabled(false);
}

void ASap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if ((InitialPosition - GetActorLocation()).Size2D() >= MaxStraightHorizontalDistance)
	{
		ReachedMaxStraightDistance();
	}
}

void ASap::GenerateLasers()
{
	auto bPlayerObstructed = UClimbingFunctionLibrary::IsPlayerCapsuleObstructed(this, this);
	bApproved = !bPlayerObstructed;
	if (bApproved)
	{
		GenerateConnectedSaps();
		CheckForConnectedLedge();

		for (auto Sap : GetConnectedSaps())
		{
			if (LaserBeamMesh == nullptr || Sap->bApproved == false)
				continue;
			
			auto Laser = GenerateLaser(Sap);

			// If a sap doesn't have any connected sap, then we need to tell it, it has a buddy now!
			if (Sap->GetConnectedSaps().Num() <= 0)
			{
				Sap->ConnectedSaps.Add(this);
				Sap->UpdateStatus(/*isApproved: */true, /*HasConnectedSap: */true);
			}
		}
		
	}
	UpdateStatus(bApproved, ConnectedSaps.Num() > 0);
}