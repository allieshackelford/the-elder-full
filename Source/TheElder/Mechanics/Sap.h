// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "World/Climbable.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "SapBase.h"
#include "World/ExplodingSap.h"
#include "World/SappableSurface.h"
#include "World/BouncyMushroom.h"
#include "Sap.generated.h"

UENUM(BlueprintType)
enum class ESapHitType: uint8
{
	SHT_NormalWall UMETA(DisplayName = "Hit Normal Wall"),
	SHT_SapWall UMETA(DisplayName = "Hit Sappable Wall"),
	SHT_OtherSap UMETA(DisplayName = "Hit Another Sap"),
	SHT_BouncyObject UMETA(DisplayName = "Hit Bouncy Object"),
	SHT_ExplodingSap UMETA(DisplayName = "Hit Exploding Sap")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FSapOnHitDelegate, FVector, ExplosionSpawnLocation, FRotator, ExplosionSpawnRotation, ESapHitType, SapHitType);

/**
 * 
 */
UCLASS()
class THEELDER_API ASap : public ASapBase
{
	GENERATED_BODY()
public:
	ASap();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileComponent;

	/// Tunables section
protected:
	/* This is the distance that the sap goes vertically before it isn't affected by gravity*/
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	float MaxStraightHorizontalDistance = 2000.0f;
	
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	float ProjectileGravityScale = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tunables")
	float SapSpeed = 300;
	
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	float WallOffset = 0.1f;
	
	void BeginPlay() override;
	void OnConstruction(const FTransform& Transform) override;
	void Tick(float DeltaTime) override;
	void GenerateLasers();

	// All the overrideable functions
	virtual void OnHitExplodingSap(AExplodingSap* HitSap, const FHitResult& SweepResult);
	virtual void HitOtherSap(ASap* OtherSap, const FHitResult& SweepResult);
	virtual void HitSappableSurface(ASappableSurface* Surface, const FHitResult& SweepResult);
	virtual void HitRegularThing(AActor* Thing, const FHitResult& SweepResult);
	void HitBouncyMushroom(ABouncyMushroom* Shroom, const FHitResult& SweepResult);
public:
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                    FVector NormalImpulse, const FHitResult& SweepResult);
	void CheckSapAttachStatus(AActor* OtherActor, UPrimitiveComponent* OtherComp, const FHitResult& SweepResult);
	void ReachedMaxStraightDistance();

	
	
	UPROPERTY(BlueprintAssignable, Category = "Sap")
	FSapOnHitDelegate OnHitDelegate;
	
private:
	FVector InitialPosition;

	UFUNCTION(BlueprintCallable)
	void OnBounce();
};