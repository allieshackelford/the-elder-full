// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Mechanics/Sap.h"
#include "ThrowableExlodingSap.generated.h"

USTRUCT()
struct FLerpingSapData
{
	GENERATED_BODY()
	
	UPROPERTY()
	ASapBase* Sap;
	UPROPERTY()
	FTransform TargetTransform;
	UPROPERTY()
	bool bIsRelativeTransform;
	UPROPERTY()
	FHitResult HitResult;
	UPROPERTY()
	FTransform BeforeMoveTransform;

	float CurrentLerpTime = 0.0f;

	bool IsAttachedToBone() const
	{
		return HitResult.BoneName.IsEqual(FName("None")) == false;
	}
};

/**
 * 
 */
UCLASS()
class THEELDER_API AThrowableExlodingSap : public ASap
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	float StartRaycastDistance = 100.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	float BabySapDirection = 100.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	int NumOfExplosions = 6;
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	float BabySapRaycastDistance;
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	FFloatRange RandomExplosionDelayRange = FFloatRange{0.0f, 0.1f};
	UPROPERTY(EditDefaultsOnly, Category = "Tunables")
	UCurveFloat* ExplosionCurve;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASapBase> BabySapBlueprint;

	UPROPERTY()
	TArray<FLerpingSapData> LerpingSap;
	
protected:
	void HitSappableSurface(ASappableSurface* Surface, const FHitResult& SweepResult) override;
	void Tick(float DeltaSeconds) override;
};
