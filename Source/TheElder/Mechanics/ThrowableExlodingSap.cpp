// Copyright 2019 Team Elder, Alexandrea Shackelford, Zion Nimchuk, All Rights Reserved.


#include "ThrowableExlodingSap.h"
#include "MasterIncludes.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"

void AThrowableExlodingSap::HitSappableSurface(ASappableSurface* Surface, const FHitResult& SweepResult)
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
	bIsMoving = Surface->bIsMoving;

	StaticMesh->SetVisibility(false);

	SetActorRotation((-1 * SweepResult.ImpactNormal).Rotation());
	
	bIsClimbable = true;
	OnHitDelegate.Broadcast(SweepResult.Location, (-1 * SweepResult.ImpactNormal).Rotation(), ESapHitType::SHT_SapWall);

	FVector InitialPosition = (GetActorForwardVector() * -1) * StartRaycastDistance + GetActorLocation();
	UKismetSystemLibrary::DrawDebugLine(World, GetActorLocation(), InitialPosition, FLinearColor::Red, 50.0f);
	
	const float RotDistance = 360.0f / NumOfExplosions;
	for(int i = 0; i < NumOfExplosions; i++)
	{
		auto Direction = FRotator(i * RotDistance, 0, 0).Vector();
		auto RaycastDirection = Direction + (GetActorForwardVector() * BabySapDirection);
		RaycastDirection.Normalize();
		UKismetSystemLibrary::DrawDebugLine(World, InitialPosition, InitialPosition + RaycastDirection * BabySapRaycastDistance, FLinearColor::Blue, 50.0f);

		const TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes = {
			UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic),
			UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic)};
		const TArray<AActor*> IgnoreList = {this};
		FHitResult OutResult;
		auto bDidHit = UKismetSystemLibrary::LineTraceSingleForObjects(World, InitialPosition, InitialPosition + RaycastDirection * BabySapRaycastDistance, 
			ObjectTypes, /*bTraceComplex: */true, IgnoreList, EDrawDebugTrace::ForDuration, OutResult, /*bIgnoreSelf: */true);
		if (bDidHit)
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.Owner = this;
			SpawnInfo.Instigator = Instigator;
			
			FAttachmentTransformRules AttachRules = { EAttachmentRule::KeepWorld, true };

			auto Index = LerpingSap.Add(FLerpingSapData());
			LerpingSap[Index].HitResult = OutResult;
			LerpingSap[Index].Sap = World->SpawnActor<ASapBase>(BabySapBlueprint, GetActorLocation(), GetActorRotation(), SpawnInfo);

			// Randomly subtract start time to give the sap a bit of a delay, make it seem a little more random
			//LerpingSap[Index].CurrentLerpTime -= FMath::RandRange(RandomExplosionDelayRange.GetLowerBoundValue(), RandomExplosionDelayRange.GetUpperBoundValue());
			
			if (OutResult.Component->Mobility == EComponentMobility::Movable)
			{
				auto SkeletalMesh = Cast<USkeletalMeshComponent>(OutResult.Component);
				// Let's check if we need to attach to a bone
				if (SkeletalMesh != nullptr && LerpingSap[Index].IsAttachedToBone())
				{
					auto BoneTransform = SkeletalMesh->GetBoneTransform(SkeletalMesh->GetBoneIndex(OutResult.BoneName));
					LerpingSap[Index].bIsRelativeTransform = true;
					FTransform NewTransform;
					NewTransform.SetLocation(UKismetMathLibrary::InverseTransformLocation(BoneTransform, OutResult.ImpactPoint));
					auto TargetDirection = OutResult.ImpactNormal * -1;
					// This should just convert the world Quaternion to a local one.
					NewTransform.SetRotation(TargetDirection.ToOrientationQuat().Inverse() * BoneTransform.GetRotation());
					LerpingSap[Index].TargetTransform = NewTransform;
					
					AttachToComponent(OutResult.GetComponent(), AttachRules, OutResult.BoneName);
					LerpingSap[Index].BeforeMoveTransform.SetLocation(UKismetMathLibrary::InverseTransformLocation(BoneTransform, GetActorLocation()));
					LerpingSap[Index].BeforeMoveTransform.SetRotation(GetActorQuat().Inverse() * BoneTransform.GetRotation());
				}
				else
				{
					LerpingSap[Index].bIsRelativeTransform = true;
					FTransform NewTransform;
					NewTransform.SetLocation(UKismetMathLibrary::InverseTransformLocation(OutResult.Component->GetComponentTransform(), OutResult.ImpactPoint));
					auto TargetDirection = OutResult.ImpactNormal * -1;
					// This should just convert the world Quaternion to a local one.
					NewTransform.SetRotation(TargetDirection.ToOrientationQuat().Inverse() * OutResult.Component->GetComponentQuat());
					LerpingSap[Index].TargetTransform = NewTransform;

					LerpingSap[Index].BeforeMoveTransform.SetLocation(UKismetMathLibrary::InverseTransformLocation(OutResult.Component->GetComponentTransform(), GetActorLocation()));
					LerpingSap[Index].BeforeMoveTransform.SetRotation(GetActorQuat().Inverse() * OutResult.Component->GetComponentQuat());

					AttachToComponent(OutResult.GetComponent(), AttachRules);
				}
			}
			else
			{
				LerpingSap[Index].bIsRelativeTransform = false;
				FTransform NewTransform;
				NewTransform.SetLocation(OutResult.ImpactPoint);
				NewTransform.SetRotation((OutResult.ImpactNormal * -1).ToOrientationQuat());
				LerpingSap[Index].TargetTransform = NewTransform;
				LerpingSap[Index].BeforeMoveTransform.SetLocation(GetActorLocation());
				LerpingSap[Index].BeforeMoveTransform.SetRotation(GetActorQuat());
			}
		}
		else
		{
			//
			// TODO:
			// Need to spawn a regular sap that just goes in that direction.
			// 
			// 
		}
	}
	
	// We don't want to still receive overlap events
	StaticMesh->OnComponentHit.RemoveDynamic(this, &ASap::OnOverlapBegin);

}

void AThrowableExlodingSap::Tick(float DeltaSeconds)
{
	auto World = GetWorld();
	if (World == nullptr)
		return;
	
	if (ExplosionCurve == nullptr)
		return;

	float MinTime, MaxTime;
	ExplosionCurve->GetTimeRange(MinTime, MaxTime);
	
	for (auto &SapData : LerpingSap)
	{
		SapData.CurrentLerpTime += DeltaSeconds;
		if (SapData.CurrentLerpTime < 0)
			continue;
		if (SapData.CurrentLerpTime > MaxTime)
		{
			
		}
		auto Val = ExplosionCurve->GetFloatValue(SapData.CurrentLerpTime);

		if (SapData.bIsRelativeTransform)
		{
			SapData.Sap->SetActorRelativeLocation(
				UKismetMathLibrary::VLerp(SapData.BeforeMoveTransform.GetLocation(), SapData.TargetTransform.GetLocation(), Val));
			SapData.Sap->SetActorRelativeRotation(
				FQuat::Slerp(SapData.BeforeMoveTransform.GetRotation(), SapData.TargetTransform.GetRotation(), Val));
		}
		else
		{
			SapData.Sap->SetActorLocation(
				UKismetMathLibrary::VLerp(SapData.BeforeMoveTransform.GetLocation(), SapData.TargetTransform.GetLocation(), Val));
			SapData.Sap->SetActorRotation(
				FQuat::Slerp(SapData.BeforeMoveTransform.GetRotation(), SapData.TargetTransform.GetRotation(), Val));
		}
	}
}

