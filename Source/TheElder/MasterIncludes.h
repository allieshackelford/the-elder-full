#pragma once

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Components/InputComponent.h"
#include "Player/Mokosh.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "AI/ChiAI/Chi.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Engine/Engine.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Utility/Utility.h"
#include "Utility/ElderFunctionLibrary.h"

#define LOG_ERR(OutputString, Time)		{GEngine->AddOnScreenDebugMessage(-1, Time, FColor::Red,	(FString("ERROR: ")		+	OutputString) );}
#define LOG_WARN(OutputString, Time)	{GEngine->AddOnScreenDebugMessage(-1, Time, FColor::Yellow,	(FString("WARNING: ")	+	OutputString) );}
#define LOG(OutputString, Time)			{GEngine->AddOnScreenDebugMessage(-1, Time, FColor::White,								OutputString)  ;}