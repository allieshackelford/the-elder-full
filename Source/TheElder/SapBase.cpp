// Fill out your copyright notice in the Description page of Project Settings.


#include "SapBase.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "MasterIncludes.h"
#include "World/SplineLedge.h"

// Sets default values
ASapBase::ASapBase()
{
	PrimaryActorTick.bCanEverTick = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));

	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->bTraceComplexOnMove = true;
	StaticMesh->SetNotifyRigidBodyCollision(true);
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	RootComponent = StaticMesh;
}


void ASapBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	auto World = GetWorld();

	auto bPlayerObstructed = UClimbingFunctionLibrary::IsPlayerCapsuleObstructed(this, this);
	bApproved = !bPlayerObstructed;
}


void ASapBase::BeginPlay()
{
	Super::BeginPlay();
	auto World = GetWorld();
	// If we're not approved, then the player can't climb onto this sap anyways
	if (bApproved)
	{
		GenerateConnectedSaps();
		CheckForConnectedLedge();

		
		
		for (auto Sap : ConnectedSaps)
		{
			if (LaserBeamMesh == nullptr)
				continue;
			if (Sap->IsAlreadyConnected() == false)
			{
				auto Laser = GenerateLaser(Sap);
			}
		}
		bAlreadyConnected = true;
	}
	UpdateStatus(bApproved, ConnectedSaps.Num() > 0);
}

void ASapBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

TArray<ASapBase*> ASapBase::GetConnectedSaps() const
{
	return ConnectedSaps;
}

bool ASapBase::IsCloseToLedge() const
{
	return bCloseToLedge;
}

UStaticMeshComponent* ASapBase::GenerateLaser(const ASapBase* SapToConnectTo)
{
	auto Laser = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass());
	Laser->SetRelativeLocation(LaserOffset);
	Laser->RegisterComponent();
	Laser->AttachToComponent(RootComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
	this->AddInstanceComponent(Laser);
	Laser->SetStaticMesh(LaserBeamMesh);
	Laser->SetCanEverAffectNavigation(false);
	Laser->SetCastShadow(false);
	Laser->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	auto OtherSapOffset = UKismetMathLibrary::TransformDirection(SapToConnectTo->GetActorTransform(), LaserOffset);
	auto Direction = (SapToConnectTo->GetActorLocation() + OtherSapOffset) - Laser->GetComponentLocation();
	Laser->SetWorldRotation(Direction.GetSafeNormal().Rotation());
	Laser->SetWorldScale3D(FVector((Direction.Size() * 0.2f)*0.5f, LaserScaleXZ, LaserScaleXZ));
	return Laser;
}

void ASapBase::GenerateConnectedSaps()
{
	auto World = GetWorld();
	
	const TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes =
	{
		UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic),
		UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic),
	};
	
	const TArray<AActor*> IgnoreList = {this};
	TArray<AActor*> OutActors = {};
	
	bool bIsOverlapped = UKismetSystemLibrary::SphereOverlapActors(World, GetActorLocation(), UClimbingFunctionLibrary::GetClimbingDetectionRadius(), ObjectTypes,
		ASapBase::StaticClass(), IgnoreList, OutActors);
	for (auto Actor : OutActors)
	{
		auto Sap = Cast<ASapBase>(Actor);
		if (Sap == nullptr)
			continue;
		if (Sap->bApproved)
		{
			ConnectedSaps.Add(Sap);
		}
	}
}

void ASapBase::CheckForConnectedLedge()
{
	auto World = GetWorld();
	const TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes =
	{
		UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic),
		UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic),
	};
	const TArray<AActor*> IgnoreList = {this};
	TArray<AActor*> OutActors = {};
	bool bHasLedge = UKismetSystemLibrary::SphereOverlapActors(World, GetActorLocation(), UClimbingFunctionLibrary::GetClimbingDetectionRadius(), ObjectTypes,
	                                                           ASplineLedge::StaticClass(), IgnoreList, OutActors);
	bCloseToLedge = bHasLedge;
}